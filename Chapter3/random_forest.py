import numpy as np

from sklearn import datasets
iris = datasets.load_iris()
X = iris.data[:,[2,3]]
y = iris.target

# Stratify=y means try to shuffle contents such that there are fractionally equal numbers of y=0,1,2 etc in the test and train sets
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=1, stratify=y)

# Note that for decision trees we don't need to standardize the features

from sklearn.ensemble import RandomForestClassifier

# n_estimators is the number of random trees
# n_jobs is a parrallel processing option (redundant here but ok)
# criterion sets the information gain maximization strategy (impurity measure)
forest = RandomForestClassifier(criterion="gini", n_estimators=25, random_state=1, n_jobs=2)
forest.fit(X_train, y_train)

import matplotlib.pyplot as plt
from plot_decision_regions import plot_decision_regions

X_combined = np.vstack(( X_train, X_test ))
y_combined = np.hstack((y_train, y_test))

plot_decision_regions(X_combined, y_combined, classifier=forest, test_idx = range(105, 150))

plt.xlabel("petal length [cm]")
plt.ylabel("petal width [cm]")

plt.legend(loc="upper left")
plt.tight_layout()
plt.show()
