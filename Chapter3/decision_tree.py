import numpy as np

from sklearn import datasets
iris = datasets.load_iris()
X = iris.data[:,[2,3]]
y = iris.target

# Stratify=y means try to shuffle contents such that there are fractionally equal numbers of y=0,1,2 etc in the test and train sets
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=1, stratify=y)

# Note that for decision tree I don't believe that we need to standardize the features

from sklearn.tree import DecisionTreeClassifier, plot_tree
tree_model = DecisionTreeClassifier(criterion="gini", max_depth=4, random_state=1)
tree_model.fit(X_train, y_train)

import matplotlib.pyplot as plt
from plot_decision_regions import plot_decision_regions

X_combined = np.vstack(( X_train, X_test ))
y_combined = np.hstack((y_train, y_test))

plot_decision_regions(X_combined, y_combined, classifier=tree_model, test_idx = range(105, 150))

plt.xlabel("petal length [cm]")
plt.ylabel("petal width [cm]")

plt.legend(loc="upper left")
plt.tight_layout()
plt.show()

plot_tree(tree_model)
plt.show()
