import numpy as np

from sklearn import datasets
iris = datasets.load_iris()
X = iris.data[:,[2,3]]
y = iris.target

# Stratify=y means try to shuffle contents such that there are fractionally equal numbers of y=0,1,2 etc in the test and train sets
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=1, stratify=y)

# Standardize the features
from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
sc.fit(X_train)
X_train_std = sc.transform(X_train)
X_test_std = sc.transform(X_test)

from sklearn.svm import SVC

# As with logistic regression, the C parameter is again related to under/over-fitting
# With C too small > underfitting, C too large > overfitting
# C in this case controls the penalty for misclassification (multiplies by sum of slack variables in the cost function)

# kernel = "linear" means we are effectively not applying a kernel
svm = SVC(kernel="linear", C=1.0, random_state=1)
svm.fit(X_train_std, y_train)

import matplotlib.pyplot as plt
from plot_decision_regions import plot_decision_regions

X_combined_std = np.vstack(( X_train_std, X_test_std ))
y_combined = np.hstack((y_train, y_test))

plot_decision_regions(X_combined_std, y_combined, classifier=svm, test_idx = range(105, 150))

plt.xlabel("petal length [standardized]")
plt.ylabel("petal width [standardized]")

plt.legend(loc="upper left")
plt.tight_layout()
plt.show()
