import numpy as np

from sklearn import datasets
iris = datasets.load_iris()
X = iris.data[:,[2,3]]
y = iris.target

# Stratify=y means try to shuffle contents such that there are fractionally equal numbers of y=0,1,2 etc in the test and train sets
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=1, stratify=y)

# Standardize the features
from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
sc.fit(X_train)
X_train_std = sc.transform(X_train)
X_test_std = sc.transform(X_test)

# OVR I believe stands for One versus Rest
# C is inverse of lambda, lambda sets how much we try to minimise the weights
# If lambda is too large (C is too small), we underfit. Conversely if lambda is too small (C is too large) we overfit
from sklearn.linear_model import LogisticRegression

weights, params = [], []

for c in np.arange(-5,5):
    lr = LogisticRegression(C = 10.**c, random_state=1, solver="lbfgs", multi_class="ovr")

    lr.fit(X_train_std, y_train)
    weights.append(lr.coef_[1])
    params.append(10.**c)

weights = np.array(weights)

# As C gets larger and we start overfitting, the 2 weights get very large
# Conversely when C is tiny the weights also become tiny, reflecting underfitting

import matplotlib.pyplot as plt
plt.plot(params, weights[:,0], label="petal length")
plt.plot(params, weights[:,1], label="petal width", linestyle='--')
plt.ylabel("weight coefficients")
plt.xlabel("C")
plt.legend(loc="upper left")
plt.xscale("log")
plt.show()
