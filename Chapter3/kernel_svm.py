import numpy as np
np.random.seed(1)

# Create a linearly inseparable dataset (4 quadrants)
X_xor = np.random.randn(200,2)

y_xor = np.logical_xor(X_xor[:,0] > 0, X_xor[:,1] > 0)
y_xor = np.where(y_xor, 1, -1)

from sklearn.svm import SVC

# kernel = rbf "Radial Basis Function" - aka a Guassian kernel

# gamma is sets the width of the gaussian (width \propto 1/gamma)
# High gamma means sharp transition from 1 to 0, means we punish misclassificaitons more, tending towards overfitting

# C as before goes with the sum of slack variables, increasing C punishes misclassifications more, tending towards overfitting
svm = SVC(kernel="rbf", C=10.0, random_state=1, gamma=0.1)

svm.fit(X_xor, y_xor)

import matplotlib.pyplot as plt
from plot_decision_regions import plot_decision_regions

plot_decision_regions(X_xor, y_xor, classifier=svm)

plt.legend(loc="upper left")
plt.tight_layout()
plt.show()
