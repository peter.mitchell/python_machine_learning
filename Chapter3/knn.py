# "Lazy learning", K-nearest-neighbours method
import numpy as np

from sklearn import datasets
iris = datasets.load_iris()
X = iris.data[:,[2,3]]
y = iris.target

# Stratify=y means try to shuffle contents such that there are fractionally equal numbers of y=0,1,2 etc in the test and train sets
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=1, stratify=y)

# Standardize the features
from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
sc.fit(X_train)
X_train_std = sc.transform(X_train)
X_test_std = sc.transform(X_test)

from sklearn.neighbors import KNeighborsClassifier

# minkowski metric refers to how we define distance between two points in the feature space
# Minkowski is the generalisation of Euclidean distance (sqrt(sum(np.square(feature^i)))) to be sqrt(), cuberoot, no root, quad-root, etc
# p is then the root-factor (square root, p=2 in this case)
# n_neighbors is then the number of nearest training-set neighbours used to decide which class (via democracy) to a given test-set member 
knn = KNeighborsClassifier(p=2, metric="minkowski", n_neighbors = 5)

knn.fit(X_train_std, y_train)

import matplotlib.pyplot as plt
from plot_decision_regions import plot_decision_regions

X_combined_std = np.vstack(( X_train_std, X_test_std ))
y_combined = np.hstack((y_train, y_test))

plot_decision_regions(X_combined_std, y_combined, classifier=knn, test_idx = range(105, 150))

plt.xlabel("petal length [standardized]")
plt.ylabel("petal width [standardized]")

plt.legend(loc="upper left")
plt.tight_layout()
plt.show()
