import numpy as np

from sklearn import datasets
iris = datasets.load_iris()
X = iris.data[:,[2,3]]
y = iris.target

# Stratify=y means try to shuffle contents such that there are fractionally equal numbers of y=0,1,2 etc in the test and train sets
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=1, stratify=y)

# Standardize the features
from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
sc.fit(X_train)
X_train_std = sc.transform(X_train)
X_test_std = sc.transform(X_test)

# OVR I believe stands for One versus Rest
# C is inverse of lambda, lambda sets how much we try to minimise the weights
# If lambda is too large (C is too small), we underfit. Conversely if lambda is too small (C is too large) we overfit
from sklearn.linear_model import LogisticRegression
lr = LogisticRegression(C=100., random_state=1, solver='lbfgs', multi_class='ovr')

lr.fit(X_train_std, y_train)

import matplotlib.pyplot as plt
from plot_decision_regions import plot_decision_regions

X_combined_std = np.vstack(( X_train_std, X_test_std ))
y_combined = np.hstack((y_train, y_test))

plot_decision_regions(X_combined_std, y_combined, classifier=lr, test_idx = range(105, 150))

plt.xlabel("petal length [standardized]")
plt.ylabel("petal width [standardized]")

plt.legend(loc="upper left")
plt.tight_layout()
plt.show()
