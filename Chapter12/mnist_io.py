import os
import struct
import numpy as np

def load_mnist(path, kind="train"):
    """Load MNIST data from 'path'"""

    labels_path = os.path.join(path, '%s-labels-idx1-ubyte' % kind)
    images_path = os.path.join(path, '%s-images-idx3-ubyte' % kind)

    with open(labels_path, "rb") as lbpath:
        magic, n = struct.unpack('>II', lbpath.read(8))
        labels = np.fromfile(lbpath, dtype=np.uint8)

    with open(images_path, "rb") as imgpath:
        magic, num, rows, cols = struct.unpack(">IIII", imgpath.read(16))
        
        images = np.fromfile(imgpath, dtype=np.uint8).reshape(len(labels), 784)

        # Normalize pixel values to +/- 1 range (helps classifiers later)
        images = ((images/255.) - .5) * 2

        # Images is a n x m 2-d array. n is the image index. m is the pixel index (stored flat).
        # There are 28x28 pixels in each image (hence 784 in above line)
        # The data for one pixel is just a greyscale intensity

    return images, labels
