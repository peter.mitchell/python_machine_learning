import mnist_io

X_train, y_train = mnist_io.load_mnist('', kind="train")

print("Rows: %d, columns: %d" % (X_train.shape[0], X_train.shape[1]))

X_test, y_test = mnist_io.load_mnist('', kind="t10k")

print("Rows: %d, columns: %d" % (X_test.shape[0], X_test.shape[1]))


# Visualize 10 training examples where the labels are 0,1,2,...,9
import matplotlib.pyplot as plt
fig, ax = plt.subplots(nrows=2, ncols=5, sharex = True, sharey = True)

ax = ax.flatten()

for i in range(10):
    img = X_train[y_train==i][0].reshape(28, 28)
    ax[i].imshow(img, cmap="Greys")

ax[0].set_xticks([])
ax[0].set_yticks([])

plt.tight_layout()
plt.show()


# Visualize 25 examples with the same label (7)

fig, ax = plt.subplots(nrows=5, ncols=5, sharex=True, sharey=True)

ax = ax.flatten()

for i in range(25):
    img = X_train[y_train==7][i].reshape(28,28)

    ax[i].imshow(img, cmap="Greys")

ax[0].set_xticks([])
ax[0].set_yticks([])

plt.tight_layout()
plt.show()
