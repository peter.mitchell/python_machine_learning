import numpy as np
import mnist_io

X_train, y_train = mnist_io.load_mnist('', kind="train")
X_test, y_test = mnist_io.load_mnist('', kind="t10k")

import neuralnet

# n_hidden - number of hidden layer units (rows)
# l2 = 0.01 - lambda parameter for L2 regularization
# epochs = 200 - number of weight-update iterations
# minibatch_size = number of (randomly sampled, with replacement if shuffle true) examples to use on each iteration epoch
# shuffle - shuffle the examples at the start of each iteration epoch
# this shouldn't make a difference unless epochs x minibatch_size > number of examples, which is not the case here

nn = neuralnet.NeuralNetMLP(n_hidden=100,
                             l2 = 0.01,
                             epochs=200,
                             eta = 0.0005,
                             minibatch_size=100,
                             shuffle=True,
                             seed=1)

print("Training the neural net")
nn.fit(X_train = X_train[:55000],
       y_train = y_train[:55000],
       X_valid = X_train[55000:],
       y_valid = y_train[55000:])
print("Done, now showing some diagnostics")

# Look at convergence
import matplotlib.pyplot as plt

plt.plot(range(nn.epochs), nn.eval_["cost"])
plt.ylabel("Cost")
plt.xlabel("Epochs")
plt.show()

# Then look at training/validation accuracy

plt.figure()
plt.plot(range(nn.epochs), nn.eval_["train_acc"], label="training")

plt.plot(range(nn.epochs), nn.eval_["valid_acc"], label="validation", linestyle='--')

plt.ylabel("Accuracy")
plt.xlabel("Epochs")
plt.legend(loc="lower right")
plt.show()

# Evaluate generalization performance using the test dataset
y_test_pred = nn.predict(X_test)

acc = (np.sum(y_test==y_test_pred).astype(np.float) / X_test.shape[0])

print("Test accuracy: %.2f%%" % (acc*100))

# See page 411 for more stuff on plotting the examples where the classifier fails
