import os
import pandas as pd
import numpy as np

s = os.path.join('https://archive.ics.uci.edu', 'ml', 'machine-learning-databases','iris','iris.data')
# Pandas has a nice ascii reader
df = pd.read_csv(s, header=None, encoding="utf-8")

# Now get lost pandas :)
y = df.iloc[0:100, 4].values
y = np.where(y=="Iris-setosa",-1,1)

# Note upper case indicates an (at least) 2d array
X = df.iloc[0:100, [0,2]].values

# Adaline parameters (same as perceptron)
eta1 = 0.0001 # Try two values of eta (now we use classes)
eta2 = 0.01
n_iter = 10
random_state = 1

class adaline:
    def __init__(self, eta=0.1, n_iter=50,random_state=1):
        self.eta = eta
        self.n_iter = n_iter
        self.random_state = random_state

    # Compute the net input function (i.e. "z")
    def net_input(self,X):
        # Here the net input (z) is defined as
        # weights[0] + weights[1]*X[0] + weights[2]*X[1] + ..

        return self._weights[0] + np.dot(X, self._weights[1:])

    # Just for illustration at this point
    def activation(self,X):
        z = self.net_input(X)
        return z

    # Model prediction for y given input values X and weights
    # Get z from net_input()
    # Phi(z) from activation function (Phi(z) just equals z in this case)
    # Transform Phi into binary +/- 1
    def predict(self,X):
        prediction = np.where( self.activation(X, self._weights) >= 0.0, 1, -1)
        return prediction

    def fit(self,X,y):
        rgen = np.random.RandomState(random_state)

        self.cost_list = []

        n_column = X.shape[1]
        self._weights = rgen.normal(loc=0.0, scale=0.01, size= 1+n_column )

        # Time to learn
        for _ in range(self.n_iter):

            output = self.activation(X)
            errors = (y-output)
    
            # For each column in X, multiply elements by corresponding element in errors, then sum over elements in the column
            self._weights[1:] += self.eta * X.T.dot(errors)
            self._weights[0] += self.eta * errors.sum() # Recall x_0 is 1 by definition
    
            cost = np.square(errors).sum() / 2.0
            self.cost_list.append(cost)
        
        return self


v1 = adaline(eta1,n_iter,random_state)
v1 = v1.fit(X,y)
v2 = adaline(eta2,n_iter,random_state)
v2 = v2.fit(X,y)

###### Visualize convergence (or not!) #############
import matplotlib.pyplot as plt

# By the way, I now see why they implement this with objects
# Makes it much easier to compare to fits with different values of eta
# Could I just do it with a fit function though, passing eta as input? (maybe, would have to check!)

plt.subplot(211)
plt.plot(np.arange(n_iter), v1.cost_list, label="eta="+str(eta1))
plt.ylabel(r"$\mathrm{R.M.S.}$")
plt.legend()

plt.subplot(212)
plt.plot(np.arange(n_iter), np.log10(v2.cost_list), label="eta="+str(eta2))
plt.ylabel(r"$\log_{10}(\mathrm{R.M.S.})$")
plt.legend()

plt.show()
quit()

   
# Note if we want we can check it worked by comparing
#print(y)
# against
#print(predict(X, weights))
#quit()

# We can visualize the dividing line as a line..
# Note this has to be a straight line! - The algorithm would fail if the true dividing line was not straight (e.g. quadratic)
x0 = np.sort(X[:100,0]) # 0 here is referring to the first column of the matrix
dividing_line = -(weights[1]*x0 + weights[0]) / weights[2]

import matplotlib.pyplot as plt
plt.scatter(X[:50,0], X[:50,1], color="red", marker="o", label="setosa")
plt.scatter(X[50:100,0], X[50:100,1], color="blue", marker="x", label="versicolor")

plt.plot(x0, dividing_line)

plt.xlabel("sepal length [cm]")
plt.ylabel("sepal length [cm]")
plt.legend(loc="upper left", frameon=False)
plt.show()
