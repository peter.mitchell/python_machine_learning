# Note to self
# I should probably change this into an object orientated version
# So I can plot easily side by side the impact of different choices of eta
# Question is, do we need objects here?
# Or could I do the same with just functions?

import os
import pandas as pd
import numpy as np

s = os.path.join('https://archive.ics.uci.edu', 'ml', 'machine-learning-databases','iris','iris.data')
# Pandas has a nice ascii reader
df = pd.read_csv(s, header=None, encoding="utf-8")

# Now get lost pandas :)
y = df.iloc[0:100, 4].values
y = np.where(y=="Iris-setosa",-1,1)

# Note upper case indicates an (at least) 2d array
X = df.iloc[0:100, [0,2]].values
n_column = X.shape[1]

# Adaline parameters (same as perceptron)
eta = 0.0001
n_iter = 10
random_state = 1

# Init
rgen = np.random.RandomState(random_state)

weights = rgen.normal(loc=0.0, scale=0.01, size= 1+n_column )
cost_list = []

# Compute the net input function (i.e. "z")
def net_input(X, weights):
    # Here the net input (z) is defined as
    # weights[0] + weights[1]*X[0] + weights[2]*X[1] + ..

    return weights[0] + np.dot(X, weights[1:])

# Just for illustration at this point
def activation(X,weights):
    z = net_input(X,weights)
    return z

# Model prediction for y given input values X and weights
# Get z from net_input()
# Phi(z) from activation function (Phi(z) just equals z in this case)
# Transform Phi into binary +/- 1
def predict(X, weights):
    prediction = np.where( activation(X, weights) >= 0.0, 1, -1)
    return prediction

# Time to learn
for _ in range(n_iter):

    output = activation(X,weights)
    errors = (y-output)
    
    # For each column in X, multiply elements by corresponding element in errors, then sum over elements in the column
    weights[1:] += eta * X.T.dot(errors)
    weights[0] += eta * errors.sum() # Recall x_0 is 1 by definition
    
    cost = np.square(errors).sum() / 2.0
    cost_list.append(cost)

###### Visualize convergence (or not!) #############
import matplotlib.pyplot as plt

# By the way, I now see why they implement this with objects
# Makes it much easier to compare to fits with different values of eta
# Could I just do it with a fit function though, passing eta as input? (maybe, would have to check!)

plt.plot(np.arange(n_iter), cost_list)
plt.show()
quit()

   
# Note if we want we can check it worked by comparing
#print(y)
# against
#print(predict(X, weights))
#quit()

# We can visualize the dividing line as a line..
# Note this has to be a straight line! - The algorithm would fail if the true dividing line was not straight (e.g. quadratic)
x0 = np.sort(X[:100,0]) # 0 here is referring to the first column of the matrix
dividing_line = -(weights[1]*x0 + weights[0]) / weights[2]

import matplotlib.pyplot as plt
plt.scatter(X[:50,0], X[:50,1], color="red", marker="o", label="setosa")
plt.scatter(X[50:100,0], X[50:100,1], color="blue", marker="x", label="versicolor")

plt.plot(x0, dividing_line)

plt.xlabel("sepal length [cm]")
plt.ylabel("sepal length [cm]")
plt.legend(loc="upper left", frameon=False)
plt.show()
