import os
import pandas as pd
import numpy as np

s = os.path.join('https://archive.ics.uci.edu', 'ml', 'machine-learning-databases','iris','iris.data')
# Pandas has a nice ascii reader
df = pd.read_csv(s, header=None, encoding="utf-8")

# Now get lost pandas :)
y = df.iloc[0:100, 4].values
y = np.where(y=="Iris-setosa",-1,1)

# Note upper case indicates an (at least) 2d array
X = df.iloc[0:100, [0,2]].values
n_column = X.shape[1]

# Perceptron parameters
eta = 0.1
n_iter = 10
random_state = 1

# Init
rgen = np.random.RandomState(random_state)

weights = rgen.normal(loc=0.0, scale=0.01, size= 1+n_column )
errors_list = []

# Compute the net input function (i.e. "z")
def net_input(X, weights):
    # Here the net input (z) is defined as
    # weights[0] + weights[1]*X[0] + weights[2]*X[1] + ..

    return weights[0] + np.dot(X, weights[1:])

# Model prediction for y given input values X and weights
# Get z from net_input()
# Transforms z into binary +/- 1
# Note this function works both if we feed in the entire X matrix (which we can do to evaluate the model at the end),
# or if we only feed one row at a time (i.e. in the algorithm)
def predict(X, weights):
    output = np.where( net_input(X, weights) >= 0.0, 1, -1)
    return output

# Time to learn
for _ in range(n_iter):
    errors = 0

    # Loop over each row in the training set
    # For a given measurement, xi are the inputs (X[n]), target is the output value (y[n])
    # Note we have to do this since the weights are being updated as we go row by row through the loop
    # (this feels weird given the linear algebra definition at the start, but ok)
    # Edit: went back and checked - yes this is what the text describes before the code example
    # This also means that the ordering of the entries in the training set makes a difference
    for xi, target in zip(X,y):

        # If the prediction overshoots the target (i.e. +1 instead of -1), update is negative (scaled by eta)
        # If the prediction undershoots the target (i.e. -1 instead of +1), update is positive (scaled by eta)
        update = eta * (target - predict(xi, weights))

        # For the weights that are linear coefficients with X, we need to scale them appropriately
        # I.e. a value in weights needs to be larger if a value xi is itself large (and if we made an error)
        weights[1:] += update * xi
        # Conversely, weights[0] (bias parameter) is where we set the global threshold (for all columns)
        # for whether we return -1 or +1, and so should be independent of the values in the different columns
        weights[0] += update
        
        errors += int(update!= 0.0)
    errors_list.append(errors)

# Note if we want we can check it worked by comparing
#print(y)
# against
#print(predict(X, weights))

# We can visualize the dividing line as a line..
# Note this has to be a straight line! - The algorithm would fail if the true dividing line was not straight (e.g. quadratic)
x0 = np.sort(X[:100,0]) # 0 here is referring to the first column of the matrix
dividing_line = -(weights[1]*x0 + weights[0]) / weights[2]

import matplotlib.pyplot as plt
plt.scatter(X[:50,0], X[:50,1], color="red", marker="o", label="setosa")
plt.scatter(X[50:100,0], X[50:100,1], color="blue", marker="x", label="versicolor")

plt.plot(x0, dividing_line)

plt.xlabel("sepal length [cm]")
plt.ylabel("sepal length [cm]")
plt.legend(loc="upper left", frameon=False)
plt.show()
