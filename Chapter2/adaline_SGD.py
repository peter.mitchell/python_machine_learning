# SGD = Stochastic gradient descent, where we cycle (at random) through individual rows (similar to first perceptron example)
# This is supposed to lead to faster convergence (python not the right language for this though!)

import os
import pandas as pd
import numpy as np

s = os.path.join('https://archive.ics.uci.edu', 'ml', 'machine-learning-databases','iris','iris.data')
# Pandas has a nice ascii reader
df = pd.read_csv(s, header=None, encoding="utf-8")

# Now get lost pandas :)
y = df.iloc[0:100, 4].values
y = np.where(y=="Iris-setosa",-1,1)

# Note upper case indicates an (at least) 2d array
X = df.iloc[0:100, [0,2]].values

# Standardize the training data
X_std = X - np.mean(X,axis=0)
X_std /= np.std(X,axis=0)

# Adaline parameters (same as perceptron)
eta1 = 0.0001 # Try two values of eta (now we use classes)
eta2 = 0.01
n_iter = 15
random_state = 1

class adaline_sgd:
    def __init__(self, eta=0.1, n_iter=50,random_state=1,shuffle=True):
        self.eta = eta
        self.n_iter = n_iter
        self.random_state = random_state
        self.shuffle = shuffle
        self.weights_initialised =False

    # Compute the net input function (i.e. "z")
    def net_input(self,X):
        # Here the net input (z) is defined as
        # weights[0] + weights[1]*X[0] + weights[2]*X[1] + ..

        return self.weights_[0] + np.dot(X, self.weights_[1:])

    # Just for illustration at this point
    def activation(self,X):
        z = self.net_input(X)
        return z

    # Model prediction for y given input values X and weights
    # Get z from net_input()
    # Phi(z) from activation function (Phi(z) just equals z in this case)
    # Transform Phi into binary +/- 1
    def predict(self,X):
        prediction = np.where( self.activation(X, self.weights_) >= 0.0, 1, -1)
        return prediction

    def _init_weights(self,n_column):
        # I believe the trailing underscore is to indicate this is an internal variable
        # Not intended to be extricated from outside this class instance
        self.weights_ = self.rgen.normal(loc=0.0, scale=0.01, size= 1+n_column )
        self.weights_initialised =True

    def _shuffle(self,X,y):
        r = self.rgen.permutation(len(y))
        return X[r], y[r]

    def update_weights(self,x_i,y_i):
        output = self.activation(x_i)
        error = (y_i-output)
    
        # For each column in X, multiply by corresponding errors
        self.weights_[1:] += self.eta * x_i.dot(error)
        self.weights_[0] += self.eta * error # Recall x_0 is 1 by definition

        cost = np.square(error) / 2.0
        return cost

    def fit(self,X,y):
        self.rgen = np.random.RandomState(random_state)

        self.cost_list = []

        # I think the preceeding underscore indicates something in self is being modified by the method
        self._init_weights(X.shape[1])

        # Time to learn
        for _ in range(self.n_iter):
            if self.shuffle:
                X,y = self._shuffle(X,y)

            cost = []
            for x_i, y_i in zip(X,y): 
                cost.append(self.update_weights(x_i, y_i))
            
            average_cost = np.mean(cost)
            self.cost_list.append(average_cost)
        
        return self
    
    # For if you want to add a few extra training examples, without restarting the fit
    def partial_fit(self,X,y):
        if not self.weights_initialised:
            self._init_weights(X.shape[1])

        if y.ravel().shape[0] > 1:
            for xi, yi in zip(X,y):
                self._update_weights(xi,yi)
        else:
            self._update_weights(X,y)
        return self

v1 = adaline_sgd(eta1,n_iter,random_state)
v1 = v1.fit(X_std,y)
v2 = adaline_sgd(eta2,n_iter,random_state)
v2 = v2.fit(X_std,y)

###### Visualize convergence (or not!) #############
import matplotlib.pyplot as plt

# By the way, I now see why they implement this with objects
# Makes it much easier to compare to fits with different values of eta
# Could I just do it with a fit function though, passing eta as input? (maybe, would have to check!)

plt.subplot(211)
plt.plot(np.arange(n_iter), v1.cost_list, label="eta="+str(eta1))
plt.ylabel(r"$\mathrm{R.M.S.}$")
plt.legend()

plt.subplot(212)
plt.plot(np.arange(n_iter), v2.cost_list, label="eta="+str(eta2))
plt.ylabel(r"$\mathrm{R.M.S.}$")
plt.legend()

plt.show()
quit()

   
# Note if we want we can check it worked by comparing
#print(y)
# against
#print(predict(X, weights))
#quit()

# We can visualize the dividing line as a line..
# Note this has to be a straight line! - The algorithm would fail if the true dividing line was not straight (e.g. quadratic)
x0 = np.sort(X[:100,0]) # 0 here is referring to the first column of the matrix
dividing_line = -(weights[1]*x0 + weights[0]) / weights[2]

import matplotlib.pyplot as plt
plt.scatter(X[:50,0], X[:50,1], color="red", marker="o", label="setosa")
plt.scatter(X[50:100,0], X[50:100,1], color="blue", marker="x", label="versicolor")

plt.plot(x0, dividing_line)

plt.xlabel("sepal length [cm]")
plt.ylabel("sepal length [cm]")
plt.legend(loc="upper left", frameon=False)
plt.show()
