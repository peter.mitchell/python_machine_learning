import numpy as np

def conv1d(x, w, p=0, s=1):

    # Convolution is defined as y[i] = int x(i-k) w(k)
    # Which we can mimic by reversing the kernel function
    w_rot = np.array(w[::-1])

    x_padded = np.array(x)

    # Padding
    if p > 0:
        zero_pad = np.zeros(shape=p)
        x_padded = np.concatenate([zero_pad, x_padded, zero_pad])

    res = []

    # s is stride, which effectively downsamples the data if it is not set to one
    for i in range(0, int(len(x)/s), s):
        res.append(np.sum(x_padded[i:i+w_rot.shape[0]] * w_rot))

    return np.array(res)

x = [1, 3, 2, 4, 5, 6, 1, 3]
w = [1, 0, 3, 1, 2]

print('Conv1d Implementation', conv1d(x, w, p=2, s=1))

# "same" means you pad x such that the returned y has the same shape as x (p=2 in this example)
print('Numpy implementation', np.convolve(x, w, mode='same'))
