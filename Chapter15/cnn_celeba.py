# CNN applied to Celebrity Image dataset
# The main new idea here is "data augmentation", where we randomly apply operations like crop/contrast adjust/brightness adjust
# during training - this is to make the training make robust
import tensorflow as tf
import tensorflow_datasets as tfds
import numpy as np

# Note that as before with celeba, my computer crashes in the prepation phase
# "too many open files"
# In any case, I can still write/read/copy the source code

celeba_bldr = tfds.builder('celeb_a')
celeba_bldr.download_and_prepare()
celeba = celeba_bldr.as_dataset(shuffle_files=False)

# In this case (unlike MNIST) a validation set was pre-defined
celeba_train = celeba["train"]
celeba_valid = celeba["validation"]
celeba_test = celeba["test"]

def count_items(ds):
    n = 0
    for _ in ds:
        n += 1
    return n

#print("Train set: {}".format(count_items(celeba_train)))
#print("Validation set: {}".format(count_items(celeba_valid)))
#print("Test set: {}".format(count_items(celeba_test)))

# Assuming my computer could handle the data prep stages, we would then take mercy by reducing the set sizes from their defaults
celeba_train = celeba_train.take(16000)
celeba_valid = celaba_valid.take(1000)


# The textbook now gives some anecdotal examples of "data agumentation" - see page 552, 555

def preprocess(example, size=(64, 64), mode="train"):
    image = example["image"]
    label = example["attributes"]["Male"]

    if mode == "train":

        image_cropped = tf.image.random_crop(
            image, 
            size=(178, 178, 3))

        # Standardize after random crap
        image_resized = tf.image.resize(
            image_cropped,
            size=size)

        image_flip = tf.image.random_flip_left_right(image_resized)

        return image_flip / 255., tf.cast(label, tf.int32)
    
    else:
        # Use center instead of random crop for non-training data
        image_cropped = tf.image.crop_to_bounding_box(
            image, offset_height=20, offset_width=0,
            target_height=178, target_width=178)
        image_resized = tf.image.resize(
            image_cropped, size=size)
        return image_resized/255., tf.cast(label, tf.int32)

BUFFER_SIZE = 1000
BATCH_SIZE = 32
IMAGE_SIZE = (64, 64)
steps_per_epoch = np.ceil(16000 / BATCH_SIZE)

tf.random.set_seed(1)

ds_train = celeba_train.map(
    lambda x: preprocess(x, size=IMAGE_SIZE, mode="train"))

ds_train = ds_train.shuffle(buffer_size = BUFFER_SIZE).repeat()
ds_train = ds_train.batch(BATCH_SIZE)

ds_valid = celeba_valid.map(lambda x: preprocess(x, size=IMAGE_SIZE, mode="eval"))
ds_valid = ds_valid.batch(BATCH_SIZE)



model = tf.keras.Sequential([
    # First number in number of filters, second tuple is the size of each smoothing filter
    tf.keras.layers.Conv2D(
        32, (3, 3), padding='same', activation='relu'),
    tf.keras.layers.MaxPooling2d((2,2))
    tf.keras.layers.Dropout(rate=0.5),

    tf.keras.layers.Conv2D(
        64, (3, 3), padding='same', activation='relu'),
    tf.keras.layers.MaxPooling2d((2,2))
    tf.keras.layers.Dropout(rate=0.5),

    tf.keras.layers.Conv2D(
        128, (3, 3), padding='same', activation='relu'),
    tf.keras.layers.MaxPooling2d((2,2))
    
    tf.keras.layers.Conv2D(
        256, (3, 3), padding='same', activation='relu'),
    ])

print(model.compute_output_shape(input_shape=(None, 64, 64, 3)))

# New # Add a gloabl average pooling layer
# This is an average pool, for the special "global" case where the pooling tile size is just set to the size of the image
# So in practice we take the mean (presumably) of each filtered image (of which there are 256, set by the previous conv2d layer)
model.add(tf.keras.layers.GlobalAveragePooling2D())
print(model.compute_output_shape(input_shape=(None, 64, 64, 3)))

# Setting activation=None for this layer means that the model apparently outputs logits (log probabilities, normed)
# I'm not quite sure why this is the case, would have to back to chapter 3 and remind myself when probabilities are first introduced
model.add(tf.keras.layers.Dense(1, activation=None))

tf.random.set_seed(1)
model.build(input_shape=(None, 64, 64, 3))
print(model.summary())


# Gender classification so this is a binary (0) or (1) label problem
model.compile(optimizer=tf.keras.optimizers.Adam(),
              loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
              metrics=["accuracy"])

history = model.fit(ds_train, validation_data=ds_valid, epochs=20,
                    steps_per_epoch = steps_per_epoch)

hist = history.history
x_arr = np.arange(len(hist['loss'])) + 1

import matplotlib.pyplot as plt

fig = plt.figure(figsize=(12,4))

ax = fig.add_subplot(1, 2, 1)
ax.plot(x_arr, hist['loss'], '-o', label='Train loss')
ax.plot(x_arr, hist['val_loss'], '--<', label='Validation loss')
ax.legend(fontsize=15)

ax = fig.add_subplot(1, 2, 2)
ax.plot(x_arr, hist['accuracy'], '-o', label="Train acc")
ax.plot(x_arr, hist['val_accuracy'], '--<', label="Validation acc")
ax.legend(fontsize=15)

plt.show()

# From this we would have concluded that we have not yet reached a plateau in the accuracy/loss
# So we could continue training as follows
# using "initial_epoch" argument

history = model.fit(ds_train, validation_data=ds_valid,
                    epochs=30, initial_epoch = 20,
                    steps_per_epoch = steps_per_epoch)

ds_test = celeba_test.map(
    lambda x: preprocess(x, size=IMAGE_SIZE, mode="eval")).batch(32)

test_results = model.evaluate(ds_test)

print("Test accuracy: {:.2f}%".format(test_results[1]*100))

# To see some anecdotal image examples, and also how to convert model output (logit) into class probabilities, see page 563
