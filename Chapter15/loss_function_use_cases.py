import tensorflow as tf
# This script is supposed to show when different loss functions are appropriate, given the type of output data from the classifier
# Note all these loss functions are for discrete class labels (either binary, or multi-class)
# You would use a different loss function (stddev type things) if the y values were continuous (i.e. fitting a line to some points)

#### Binary Crossentropy #####
# This is the standard loss function used for binary classification (i.e. true value is either 0 or 1)

# In general, the loss function will assume you are passing in probabilities (as with a sigmoid function)
# But you can also choose to pass in "logits" (page 61), which is just log (p / (1-p)) - where log is natural log, and p is probability

# You can use either, but apparently tensorflow is slightly more efficient and stable if you use logits

bce_probas = tf.keras.losses.BinaryCrossentropy(from_logits=False)
bce_logits = tf.keras.losses.BinaryCrossentropy(from_logits=True)

logits = tf.constant([0.8])
probas = tf.keras.activations.sigmoid(logits) # Recall that sigmoid function is the inverse of the logit function (I think?)

# Evaluate loss function, given input verses truth
tf.print('BCE (w Probas): {:.4f}'.format(bce_probas(y_true=[1], y_pred=probas)),
         '(w Logits): {:.4f}'.format(bce_logits(y_true=[1], y_pred=logits)))
# Note they are supposed to give the same result!

#### Categorical Crossentropy ############
# This is the loss function you use when you have one-hot style encoding, 
#(i.e. there are more than 2 labels that are e.g. (0,0,1), or (1,0,0), etc)
# Note in this case the probabilities/logits would have been obtained from the "softmax" activation, rather than sigmoid

cce_probas = tf.keras.losses.CategoricalCrossentropy(from_logits=False)
cce_logits = tf.keras.losses.CategoricalCrossentropy(from_logits=True)

logits = tf.constant([[1.5, 0.8, 2.1]])
probas = tf.keras.activations.softmax(logits)

tf.print(
    'CCE (w probas): {:.4f}'.format(
        cce_probas(y_true=[[0,0,1]], y_pred=probas)),
    '(w logits): {:.4f}'.format(
        cce_logits(y_true=[[0,0,1]], y_pred=logits)))

##### Sparse Categorical Crossentropy #####
# This is the loss function you use when you have muti-class classification (more than two labels)
# But in this case where one-hot encoding is not used, and instead labels are (0), or (1), or (2), etc

sp_cce_probas = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=False)
sp_cce_logits = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)

tf.print(
    'Sparse CCE (w probas): {:.4f}'.format(
        sp_cce_probas(y_true=[2], y_pred=probas)),
    '(w logits): {:.4f}'.format(
        sp_cce_logits(y_true=[2], y_pred=logits)))
