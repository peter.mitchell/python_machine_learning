# Build a convolutional neural network (CNN) to classify handwritten numbers from the MNIST dataset
import tensorflow_datasets as tfds
import tensorflow as tf
import numpy as np

# There are two ways to load datasets from tfds - this is the more verbose way
mnist_bldr = tfds.builder('mnist')
mnist_bldr.download_and_prepare()

# The shuffle_files=False is apparently quite important, since we want to split the training set into train/valid and not cross them over
# It is not clear to me however where the shuffles would occur if they were allowed to occur
datasets = mnist_bldr.as_dataset(shuffle_files=False)
mnist_train_orig = datasets["train"]
mnist_test_orig = datasets["test"]

BUFFER_SIZE = 10000
BATCH_SIZE = 64
NUM_EPOCHS = 20
print("TEMP HACK")
NUM_EPOCHS = 1
BATCH_SIZE = 16

# Convert dict into tuple
mnist_train = mnist_train_orig.map(
    lambda item: (tf.cast(item['image'], tf.float32) / 255.,
                  tf.cast(item['label'], tf.int32)))

mnist_test = mnist_test_orig.map(
    lambda item: (tf.cast(item['image'], tf.float32) / 255.,
                  tf.cast(item['label'], tf.int32)))

# Split training/validation sets
tf.random.set_seed(1)
mnist_train = mnist_train.shuffle(buffer_size=BUFFER_SIZE, reshuffle_each_iteration=False)
mnist_valid = mnist_train.take(10000).batch(BATCH_SIZE) # Note this bins into batches, doesn't return just one batch
mnist_train = mnist_train.skip(10000).batch(BATCH_SIZE)


# Build the CNN
model = tf.keras.Sequential()

# 2d convolution layer. Filters is number of convolution filters - Each has a 2d weight array given by kernel_size
# Stride is downsampling option (does nothing here)
# padding="same" means pad zeros to ensure that output images have same shape as input images
# channels last - means the (e.g.) RGB dimension is at the end of the array (NHWC format - N images in batch, Height, Width, Channel)
model.add(tf.keras.layers.Conv2D(
        filters=32, kernel_size=(5, 5),
        strides=(1, 1), padding="same",
        data_format="channels_last",
        name="conv_1", activation="relu"))

# Pooling layer this splits image into 2x2 tiles (without overlap), finds max value, and effectively downsamples the image by a factor 4
model.add(tf.keras.layers.MaxPool2D(pool_size= (2,2), name="pool_1"))

# Note I guess the basic logic is that as the images get coarser and coarser, we can afford to use larger numbers of filters
# as we get deeper into the network
model.add(tf.keras.layers.Conv2D(
                filters=64, kernel_size=(5, 5),
                strides=(1,1), padding="same",
                name="conv_2", activation="relu"))

model.add(tf.keras.layers.MaxPool2D(
                pool_size=(2,2), name="pool_2"))

# Just a tool so we can see what output shape we would get if we fed in 16 28x28 greyscale (1 channel) images
#print(model.compute_output_shape(input_shape=(16, 28, 28, 1)))

# At this point the input to this layer has shape (N_examples, Height (after pooling), Width (after pooling), N_filters (prev conv layer))
# Collapse image/filter dimensions - so we can then use a dense fully connected layer at the end
model.add(tf.keras.layers.Flatten())
# output now has dimensions (N_examples, N_flat)

model.add(tf.keras.layers.Dense(
        units=1024, name="fc_1",
        activation='relu'))

# Now we implement a dropout step (which helps with over-fitting)
# I believe the way this works is that half the weights get set to zero randomly each time
# This prevents the network from being overly reliant on one set of weights, making it hopefully more robust
model.add(tf.keras.layers.Dropout(rate=0.5))

# Finally we have the output layer
# 10 outputs since we have 10 numbers as labels in the MNIST set
# So each output is the probability of a given label, with softmax activation (so that the probabilities are normalized correctly)
# Note that this MNIST dataset is not using one-hot encoding for the labels. E.g. labels would be (2), not (0, 0, 1, 0, etc)
# But, since the output is probability, it ends up looking like one-hot encoding in terms of the classifier output
model.add(tf.keras.layers.Dense(
        units = 10, name='fc_2',
        activation='softmax'))


tf.random.set_seed(1)
model.build(input_shape = (None, 28, 28, 1))

# Sparse Categorial Crossentropy since we this is a multiclass problem, and the MNIST labels here are using sparse label convention
# (i.e., the label is (2), not (0, 0, 1, 0, 0, etc) - makes sense given the labels are actually numbers!)
model.compile(
    optimizer=tf.keras.optimizers.Adam(),
    loss=tf.keras.losses.SparseCategoricalCrossentropy(),
    metrics=['accuracy'])

history = model.fit(mnist_train, epochs=NUM_EPOCHS, validation_data=mnist_valid, shuffle=True)

test_results = model.evaluate(mnist_test.batch(20))
print('Test acc.: {:.2f}\%'.format(test_results[1]*100))

# Visualize the results
import matplotlib.pyplot as plt

hist = history.history
x_arr = np.arange(len(hist['loss'])) + 1

fig = plt.figure(figsize=(12,4))

ax = fig.add_subplot(1, 2, 1)
ax.plot(x_arr, hist['loss'], '-o', label='Train loss')
ax.plot(x_arr, hist['val_loss'], '--<', label='Validation loss')
ax.legend(fontsize=15)

ax = fig.add_subplot(1, 2, 2)
ax.plot(x_arr, hist['accuracy'], '-o', label="Train acc")
ax.plot(x_arr, hist['val_accuracy'], '--<', label="Validation acc")
ax.legend(fontsize=15)

plt.show()

# Check anecdotally if it worked!
batch_test = next(iter(mnist_test.batch(12)))

preds = model(batch_test[0])
tf.print(preds.shape)
# shape = (12, 10), 12 images we choose, and probability that each image is 1 of 10 possible labels (numbers) in MNIST

preds = tf.argmax(preds, axis=1)
print(preds)

fig = plt.figure(figsize=(12, 4))
for i in range(12):
    ax = fig.add_subplot(2, 6, i+1)
    ax.set_xticks([]); ax.set_yticks([])
    img = batch_test[0][i, :, :, 0]
    ax.imshow(img, cmap='gray_r')
    ax.text(0.9, 0.1, '{}'.format(preds[i]), size=15, color='blue', horizontalalignment='center',verticalalignment='center', transform=ax.transAxes)

plt.show()
