# This time we use the tensorflow_datasets library to get a dataset
# Note for several days the auto-download would fail
# On the third try it did work though :)
# Edit: however, my computer fails to process the downloaded data with an io error: too many open files
# Maybe the dataset was expanded since the release of the textbook?
# Anyway, abondon ship! (again)
import tensorflow as tf
import tensorflow_datasets as tfds

# Have a look at which datasets are available
# (Note this seems to be 8x more than in the textbook example of this line!)
#print(len(tfds.list_builders()))
#print(tfds.list_builders()[:5])


# Choose a dataset of celebrity photos
celeba_bldr = tfds.builder('celeb_a')

##### I added this line myself ##########
# The auto downloader for the celeb_a wasn't working
# So I downloaded the source files manually and placed them in the following place
# I then had to look at the tfds.builder.download_and_prepare() method, and change the file path

#print (celeba_bldr._data_dir)
#quit()
#celeba_bldr._data_dir = "/Users/peter/tensorflow_datasets/celeb_a"
# Another edit. Looks like doing it this way hasn't worked - I'm still missing some files.
# Comment out the above line to make it go back to try and do the auto download/prep
# Final (hopefully) edit : in the end the auto download worked

# Have a look at the properties
#print(celeba_bldr.info.features)
#print(celeba_bldr.info.features['image'])
#print(celeba_bldr.info.features['attributes'].keys)
#print(celeba_bldr.info.citation)


# Download the dataset. This apparently always goes into a common tensforflow_datasets directory, that is created when you do this
# for the first time. Note this method retrieves the pre-downloaded dataset if available
celeba_bldr.download_and_prepare()
# Indeed to /Users/peter/tensorflow_datasets/downloads/
# Edit: this doesn't work at the time of writing, see my higher up comments about manual download and tweaking the path
# Later edit: it did work on a third day of trying

datasets = celeba_bldr.as_dataset(shuffle_files=False)
#print(datasets.keys())


ds_train = datasets["train"]

# Convert dataset into a tuple of (features, labels), rather than as a dict (which it is now)
# In order to pass it into some deep learning classifier
# Choose the 'Male' category as the class label, as an example
ds_train = ds_train.map(lambda item: (item['image'], tf.cast(item['attributes']['Male'], tf.int32)))


# Visualize 18 examples
ds_train = ds_train.batch(18)

images, labels = next(iter(ds_train))
#print(images.shape, labels)

import matplotlib as plt
fig = plt.figure(figsize(12, 8))
for i, (image, label) in enumerate(zip(images, labels)):
    ax = fig.add_subplot(3, 6, i+1)
    ax.set_xticks([]); ax.set_yticks([])
    ax.imshow(image)
    ax.set_title('{}'.format(label), size=15)
plt.show()
