# Read in 6 cat/dog images, downloaded manually off the book's github repo (since no link was provided in the book..)
import pathlib
imgdir_path = pathlib.Path("cat_dog_images")
file_list = sorted([str(path) for path in imgdir_path.glob('*.jpeg')])

# Display the images using the Tensforflow IO module to read in the data
import tensorflow as tf
import os

'''
import matplotlib.pyplot as plt

fig = plt.figure(figsize=(10, 5))

for i, file in enumerate(file_list):
    img_raw = tf.io.read_file(file)
    img = tf.image.decode_image(img_raw)

    print('Image shape: ', img.shape)
    ax = fig.add_subplot(2, 3, i+1)
    ax.set_xticks([])
    ax.set_yticks([])

    ax.imshow(img)
    ax.set_title(os.path.basename(file), size=15)
    
plt.tight_layout()
plt.show()'''


# Get class labels from image file names
labels = [1 if "dog" in os.path.basename(file) else 0
          for file in file_list]

#print(labels)


# Create a tensorflow dataset
# Note this doesn't load the image data, just includes the file names and associated class labels
ds_files_labels = tf.data.Dataset.from_tensor_slices((file_list, labels))

#for item in ds_files_labels:
#    print(item[0].numpy(), item[1].numpy())

# Helper function to load and preprocess images to have a common size, aspect ratio
def load_and_preprocess(path, label):
    image = tf.io.read_file(path)
    image = tf.image.decode_jpeg(image, channels=3)
    image = tf.image.resize(image, [img_height, img_width])
    image /= 255.
    return image, label

img_width, img_height = 120, 80

# This dataset does not contain the image data, as well as label data
ds_images_labels = ds_files_labels.map(load_and_preprocess)

import matplotlib.pyplot as plt
fig = plt.figure(figsize=(10, 6))

for i, example in enumerate(ds_images_labels):
    ax = fig.add_subplot(2, 3, i+1)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.imshow(example[0])
    ax.set_title('{}'.format(example[1].numpy()), size=15)

plt.tight_layout()
plt.show()
