import tensorflow as tf
import tensorflow_datasets as tfds
import numpy as np

# Load the o.g. iris dataset
iris, iris_info = tfds.load('iris', with_info=True)
#print(iris_info)

tf.random.set_seed(1)

# This dataset has not been pre-partitioned into test/train
# It contains 150 examples, 50 of each class label (flower)
ds_orig = iris['train']

# This shuffle is used here so we can create test/training sets (i.e. jumble the class labels before split)
# This means the dataset cannot be shuffled again afterwards without mixing the train/test sets
# This is why the reshuffle_each_iteration argument is set to False. Not sure if it does anything here, but good practice
ds_orig = ds_orig.shuffle(150, reshuffle_each_iteration=False)

ds_train_orig = ds_orig.take(100) # First 100 entries
ds_test = ds_orig.skip(100) # Final 50 entries

ds_train_orig = ds_train_orig.map(lambda x: (x['features'], x['label']))
ds_test = ds_test.map(lambda x: (x['features'], x['label']))

#for example in ds_test:
#    print (example[0], example[1])
#quit()

# Build a multi-layer neural network
# Note it is referred to as "perceptron" as the outputs are 0 or 1
iris_model = tf.keras.Sequential([
        tf.keras.layers.Dense(16, activation='sigmoid', name='fc1', input_shape=(4,)),
        tf.keras.layers.Dense(3, name="fc2", activation='softmax')
        ])

# Sequential means one layer feeds into the next
# Dense means that every input is connected to every node in the layer. (i.e. weight matrix has shape inputs x nodes)

# name='fc1' - think this is just for labelling purposes when we call iris_model.summary()

# activation = activation function (softmax is described pg 465 - it returns properly normalised probabilities for 
# >2 class classification problems (sigmoid activation doesn't return correctly normalised probabilities in this case, apparently)

# input_shape specified at the start means tensforflow knows all the array shapes beforehand
# It specifies the number of input features (4 for the iris dataset we are using)

# Number of parameters is set by n_nodes x (n_inputs+1) > +1 is for bias unit
#print(iris_model.summary())

#######################################################

# optimizer (how to compute gradients of loss function)
# loss = loss_function
# Neither of these are explained yet

# metrics is not used in training, just for us to validate afterwards how well it worked
iris_model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])

# Here we do 100 epochs, with 2 examples (batch) in each epoch
# There are 100 training examples, meaning we duplicate each once
num_epochs = 100
training_size = 100
batch_size = 2
steps_per_epoch = np.ceil(training_size / batch_size)

ds_train = ds_train_orig.shuffle(buffer_size=training_size)
ds_train = ds_train.repeat() # Double dataset with one repetition
ds_train = ds_train.batch(batch_size=batch_size) # Note this increases the rank of the ds_train tensor by one (which is needed)

ds_train = ds_train.prefetch(buffer_size=1000) # This one is less clear - not explained

history = iris_model.fit(ds_train, epochs=num_epochs, steps_per_epoch=steps_per_epoch, verbose=0)

hist = history.history

# Visualize loss/accuracy across training epochs
import matplotlib.pyplot as plt

fig = plt.figure(figsize=(12, 5))

ax = fig.add_subplot(1, 2, 1)
ax.plot(hist['loss'], lw=3)
ax.set_title('Training loss', size=15)
ax.set_xlabel('Epoch', size=15)
ax.tick_params(axis='both', which='major', labelsize=15)

ax = fig.add_subplot(1, 2, 2)
ax.plot(hist['accuracy'], lw=3)
ax.set_title('Training accuracy', size=15)
ax.set_xlabel('Epoch', size=15)
ax.tick_params(axis='both', which='major', labelsize=15)

plt.show()

# The batch call here is a dummy (there are 50 in the test set) to raise the rank of ds_test by one
results = iris_model.evaluate(ds_test.batch(50), verbose=0)

print('Test loss {:.4f} Test Acc.: {:.4f}'.format(*results))




# Saving a model and loading it
iris_model.save('iris-classifier.h5', overwrite=True, include_optimizer=True, save_format='h5') # hdf5

iris_model_new = tf.keras.models.load_model('iris-classifier.h5')
results = iris_model_new.evaluate(ds_test.batch(50), verbose=0)
print('After saving and loading the same model')
print('Test loss {:.4f} Test Acc.: {:.4f}'.format(*results))
