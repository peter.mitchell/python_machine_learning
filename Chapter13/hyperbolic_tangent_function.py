# Apparently some loss function optimizers don't work well when the activation function gets stuck around 0
# Since the standard sigmoid is bounded between 0, 1, this can happen
# Hyperbolic tangent (tanh) is almost the same as a sigmoid, but is bounded over -1, +1

import matplotlib.pyplot as plt
import numpy as np

def logistic(z):
    return 1./ (1.0 + np.exp(-z))

def tanh(z):
    e_p = np.exp(z)
    e_m = np.exp(-z)

    return (e_p - e_m) / (e_p + e_m)

z = np.arange(-5, 5, 0.005)

log_act = logistic(z)
tanh_act = tanh(z)

plt.ylim([-1.5, 1.5])
plt.xlabel('net input $z$')
plt.ylabel('net input $z$')

plt.axhline(1, color='black', linestyle=':')
plt.axhline(0.5, color='black', linestyle=':')
plt.axhline(0.0, color='black', linestyle=':')
plt.axhline(-0.5, color='black', linestyle=':')
plt.axhline(-1, color='black', linestyle=':')

plt.plot(z, log_act, linewidth=3, label='logistic')
plt.plot(z, tanh_act, linewidth=3, label='tanh')

plt.legend(loc='lower right')
plt.tight_layout()
plt.show()
