import tensorflow as tf
import numpy as np
# Note this line is in the txtbook - but it makes many of the examples not work when printing - so commented out for now
#np.set_printoptions(precision=np.int32)

# Tensor object instance creation from numpy array or python list
a = np.array([1, 2, 3], dtype=np.int32)
b = [4, 5, 6]

t_a = tf.convert_to_tensor(a)
t_b = tf.convert_to_tensor(b)

#print(t_a)
#print(t_b)

# Direct tensort creation using numpy-like syntax
t_ones = tf.ones((2,3))
#print(t_ones.shape)

# Extract raw data back into a numpy array
ones_np =  t_ones.numpy() 
#print (ones_np )

const_tensor = tf.constant([1.2, 5, np.pi], dtype=tf.float32)
#print(const_tensor)


############## Tensor shape/type manipulation ##################

t_a_new = tf.cast(t_a, tf.int64)
#print (t_a_new.dtype)

# Matrix transpose
t = tf.random.uniform(shape=(3,5))
t_tr = tf.transpose(t)
#print (t.shape, '--> ', t_tr.shape)

# Reshape
t = tf.zeros((30,))
t_reshape = tf.reshape(t, shape=(5,6))
#print (t_reshape.shape)

# Removing defunct dimensions (i.e. with only one index element)
t = tf.zeros((1, 2, 1, 4, 1))

t_sqz = tf.squeeze(t, axis=(2,4))
#print(t.shape, " --> ", t_sqz.shape)
# Note I don't know why the first dimension was retained here - this is also the case in txtbook!

############ Basic math ################

t1 = tf.random.uniform(shape=(5,2), minval = -1.0, maxval = 1.0)

t2 = tf.random.normal(shape=(5,2), mean = 0.0, stddev = 1.0)

# Basic element-wise product (not matrix multiplication) - retains shape of intut arrays
t3 = tf.multiply(t1, t2).numpy()
#print(t3)

# Mean along an axis
t4 = tf.math.reduce_mean(t1, axis=0)
#print (t4)

# Matrix-matrix product (matrix multiplication), where t2 is transposed before
t5 = tf.linalg.matmul(t1, t2, transpose_b=True)
#print(t5)

# Same but transposing t1 (yield 2x2 array this time)
t6 = tf.linalg.matmul(t1, t2, transpose_a=True)
#print(t6.numpy())

# Computing the "L^p" norm of a tensor along an axis
# i.e. sqrt(sum(square(t1))) along an axis
norm_t1 = tf.norm(t1, ord=2, axis=1).numpy()
#print(norm_t1)

############# Tensor splitting/stacking/joining ####################

tf.random.set_seed(1)
t = tf.random.uniform((6,))
#print(t.numpy())

# Split into multiple tensors 
t_splits = tf.split(t, num_or_size_splits=3)
#print( [item.numpy() for item in t_splits] )

t = tf.random.uniform((5,))
t_splits = tf.split(t, num_or_size_splits=[3,2])
#print(t_splits)

# Tensor join
A = tf.ones((3,))
B = tf.zeros((2,))
C = tf.concat([A,B], axis=0)
#print (C.numpy())

# Stacking
A = tf.ones((3,))
B = tf.zeros((3,))
S = tf.stack([A,B], axis=1)
#print(S.numpy())
