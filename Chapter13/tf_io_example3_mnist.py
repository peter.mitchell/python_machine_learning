# Another IO example, again using tensorflow_datasets, now with the MNIST dataset
# Load the data in a different way to example 2 however

import tensorflow_datasets as tfds

# This load method combines the steps from example 2: builder/download_and_prepare/as_dataset
mnist, mnist_info = tfds.load('mnist', with_info=True, shuffle_files=False)

#print(mnist_info)
print(mnist.keys())

# Take the train partition, transform from dictionary into tuple, visualize 10 examples

ds_train = mnist['train']
ds_train = ds_train.map(lambda item: (item['image'], item['label']))

ds_train = ds_train.batch(10)

# Note I think the point of this is that when batch goes into an iterator, the next iteration always gets the next item
# without having to specify an index
batch = next(iter(ds_train))

print(batch[0].shape, batch[1])

import matplotlib.pyplot as plt
fig = plt.figure(figsize=(15,6))

for i, (image, label) in enumerate(zip(batch[0], batch[1])):
    ax = fig.add_subplot(2, 5, i+1)
    ax.set_xticks([]); ax.set_yticks([])
    ax.imshow(image[:, :, 0], cmap="gray_r") # Not totally sure what that 3rd dimension is, perhaps RGB color or something?
    ax.set_title('{}'.format(label), size=15)
plt.show()
