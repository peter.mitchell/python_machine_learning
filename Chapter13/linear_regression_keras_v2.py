# Train a simple linear regression model to fit a straight line to some data
# This uses the Keras model class, and uses Keras to compute loss function gradients
# But uses from-scracth code with a train function and explicit iteration to train the model
# v2 automates this using the keras compile/fit functions
import tensorflow as tf
import numpy as np

X_train = np.arange(10).reshape((10,1))
y_train = np.array([1.0, 1.3, 3.1, 2.0, 5.0, 6.3, 6.6, 7.4, 8.0, 9.0])

# Standardize the data
X_train_norm = (X_train - np.mean(X_train))/np.std(X_train)

ds_train_orig = tf.data.Dataset.from_tensor_slices((tf.cast(X_train_norm, tf.float32),
                                                    tf.cast(y_train, tf.float32)))

# Build a custom model class that inherits from the tf.keras.Model class
class MyModel(tf.keras.Model):
    def __init__(self):
        # ?? some oo class inheritence voodoo - maybe init the parent class?
        super(MyModel, self).__init__()
        self.w = tf.Variable(0.0, name="weight")
        self.b = tf.Variable(0.0, name="bias")

    def call(self, x):
        return self.w * x + self.b

def loss_fn(y_true, y_pred):
    return tf.reduce_mean(tf.square(y_true - y_pred))

# In this version we use keras functions to train/update the model

tf.random.set_seed(1)

model = MyModel()

# Note v1 was also using stochastic gradient descent I think
# mse is mean squared error - not sure what "mae" is
model.compile(optimizer="sgd", loss=loss_fn, metrics=['mae', 'mse'])

num_epochs = 200
batch_size = 1

model.fit(X_train_norm, y_train, epochs=num_epochs, batch_size = batch_size, verbose=1)

print('Final parameters: ', model.w.numpy(), model.b.numpy())

X_test = np.linspace(0, 9, num = 100).reshape(-1, 1)

# Note test set has to be standardized the same way as the training set
X_test_norm = (X_test - np.mean(X_train)) / np.std(X_train)

y_pred = model(tf.cast(X_test_norm, dtype=tf.float32))

import matplotlib.pyplot as plt

fig = plt.figure()

plt.plot(X_train_norm, y_train, 'o', markersize=10)
plt.plot(X_test_norm, y_pred, '--', lw=3)
plt.legend(['Training examples', 'Linear Reg.'], fontsize=15)

plt.xlabel('x', size=15)
plt.ylabel('y', size=15)

plt.show()
