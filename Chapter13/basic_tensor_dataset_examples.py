import tensorflow as tf
import numpy as np

# "Dataset" is a tensor flow object used to store various tensor(s) and perform common operations on them (e.g., shuffle)
# This is typically going to be things like X, y in machine learning (and possible "test", "valid", "train", etc)

# Creating a tf dataset from a list (could also be numpy array, tf tensor, etc)
a = [1.2, 3.4, 7.5, 4.1, 5.0, 1.0]

ds = tf.data.Dataset.from_tensor_slices(a)
#print(ds)

# Iterate over elements in the dataset
#for item in ds:
#    print(item)

# Extracting batches from the dataset. Each batch will have size 3 in this case
ds_batch = ds.batch(3)

# I think the one means i starts at 1 instead of 0
#for i, elem in enumerate(ds_batch, 1):
#    print('batch {}:'.format(i), elem.numpy())

# .batch() has an argument "drop_remainder=True/False", if enabled the last batch will be dropped if it doesn't contain
# the desired number of elements



# Two tensors into one dataset
tf.random.set_seed(1)
t_x = tf.random.uniform([4,3], dtype=tf.float32)
t_y = tf.range(4)

ds_x = tf.data.Dataset.from_tensor_slices(t_x)
ds_y = tf.data.Dataset.from_tensor_slices(t_y)

ds_joint = tf.data.Dataset.zip((ds_x, ds_y))

#for example in ds_joint:
#    print(' x:', example[0].numpy(),
#          ' y:', example[1].numpy())

# Alternative method (skip making two x/y datasets step) - same output
ds_joint = tf.data.Dataset.from_tensor_slices((t_x, t_y))


# Apply transformations that affect all tensors within the dataset

# In this first example, only change x
ds_trans = ds_joint.map(lambda x, y: (x*2-1.0, y))

# In this example, shuffle x, y at the same time
tf.random.set_seed(1)

ds = ds_joint.shuffle(buffer_size=len(t_x))
# Note buffer size is an mandatory argument. Larger values I suppose cost memory. 
# Too small means arrays won't be randomised properly.
# buffer = len(array) should always work well


# Extracting batches from a joint dataset
ds = ds_joint.batch(batch_size=3, drop_remainder=False)

batch_x, batch_y = next(iter(ds))
#print("Batch-x:\n", batch_x.numpy())
#print("Batch-y:\n", batch_y.numpy())
# The batch_size = 3 thing means we effectively get one batch, with 3 examples in it (hence y has three values, and X has 3x3 values)
# However, what is drop_remainder=False doing here, we don't seem to get a remainder, and setting it True doesn't seem to change anything!
# Edit: I think this might be because of how we access stuff within ds, i.e. there should be another batch but I can't get the values
# using next(iter(ds))

ds = ds_joint.batch(3).repeat(count=2)
#for i, (batch_x, batch_y) in enumerate(ds):
#    print(i, batch_x.shape, batch_y.shape)
# Ok this time I got what I expected, I guess drop_remainder is set to False by default
# So we got one batch of 3 examples, and one batch of 1 example
# These are then repeated, such that we get batch_1, batch_2, batch_1, batch_2 when iterating

# If we invert the batch/repeat method calls:
ds = ds_joint.repeat(count=2).batch(3)
#for i, (batch_x, batch_y) in enumerate(ds):
#    print(i, batch_x.shape, batch_y.shape)
# We get different output, since now the 4 example dataset became an 8 example dataset
# Meaning we get two batches of 3, and then one batch of 2


# Importance of order of shuffle / batch / repeat
# Example 1
tf.random.set_seed(1)

ds = ds_joint.shuffle(4).batch(2).repeat(3)
#for i, (batch_x, batch_y) in enumerate(ds):
#    print(i, batch_x.shape, batch_y.shape)
# 4-example sample is shuffled, split into two batchs of two examples each, and repeated to give 6 two-example samples

# Example 2
tf.random.set_seed(1)
ds = ds_joint.batch(2).shuffle(4).repeat(3)
#for i, (batch_x, batch_y) in enumerate(ds):
#    print(i, batch_x.shape, batch_y.shape)
# 4-example is split into two batches, then shuffled, then repeated - this gives different values for the output compared to Ex1
