# Same as first example, but this time generalising some of the steps
# e.g. defining a helper function that can restrict parts of sequences to look at
# i.e. maybe the sentiment is expressed in the final paragraph of movie reviews

import tensorflow as tf
import tensorflow_datasets as tfds
import numpy as np
import pandas as pd
print()

df = pd.read_csv("../Chapter8/movie_data.csv", encoding="utf-8")

target = df.pop("sentiment")
ds_raw = tf.data.Dataset.from_tensor_slices((df.values, target.values))

#for example in ds_raw.take(3):
#    tf.print(example[0].numpy()[0][:50], example[1])

tf.random.set_seed(1)
ds_raw = ds_raw.shuffle(50000, reshuffle_each_iteration=False)

ds_raw_test = ds_raw.take(25000)
ds_raw_train_valid = ds_raw.skip(25000)
ds_raw_train = ds_raw_train_valid.take(20000)
ds_raw_valid = ds_raw_train_valid.skip(20000)


### First we encode words into numeric values ###
from collections import Counter




#### New in example 2 ###

# Max seq length sets the maximum sequence length - which is used if we use simple RNN (that doesn't handle long sequences well)
def preprocess_datasets(ds_raw_train, ds_raw_valid, ds_raw_test, max_seq_len=None, batch_size=32):

    # Note since the edition of my book likes like this tokenizer (and the "text" module in general) get deprecated
    tokenizer = tfds.deprecated.text.Tokenizer()
    token_counts = Counter()
    
    # Build the vocabulary, and count the number of times each word occurs
    for example in ds_raw_train:
        tokens = tokenizer.tokenize(example[0].numpy()[0]) # Note the indexing here is just extracting the list of (all the) word strings in the review
        if max_seq_len is not None: # So we restrict vocab also when this option is on
            tokens = tokens[-max_seq_len:]
        token_counts.update(tokens)

    print('Vocab-size:', len(token_counts))

    # Now encode the unique tokens (words) into unique integers
    encoder = tfds.deprecated.text.TokenTextEncoder(token_counts)

    #example_str = "this is an example"
    #print(encoder.encode(example_str))

    # We now want to encode all the data using the encoder using the .map() method, but this is problematic apparently
    # because the encoder doesn't work on tensor (i.e. optimized) data directly (despite coming from tfds lol)
    # So we have this odd looking double function thing here using also the tf.py_function (like a decorator) to turn
    # the function into a (pre-compiled) tensorflow operator
    def encode(text_tensor, label):
        text = text_tensor.numpy()[0]
        encoded_text = encoder.encode(text)
        if max_seq_len is not None:
            encoded_text = encoded_text[-max_seq_len:]
        return encoded_text, label

    def encode_map_fn(text, label):
        return tf.py_function(encode, inp=[text, label], Tout=(tf.int64, tf.int64))

    ds_train = ds_raw_train.map(encode_map_fn)
    ds_valid = ds_raw_valid.map(encode_map_fn)
    ds_test = ds_raw_test.map(encode_map_fn)

    # Now we introduce the concept of padded batching, where the max review length in the batch sets the sequence size
    # and smaller reviews are padded with zeros (note zero has a significance in the encoded data as a padded value)

    # 32 here is the batch size, the shapes I thing I believe just tells it to use the longest review to set the sequence size (?)
    train_data = ds_train.padded_batch(32, padded_shapes=([-1],[]))
    valid_data = ds_valid.padded_batch(32, padded_shapes=([-1],[]))
    test_data = ds_test.padded_batch(32, padded_shapes=([-1],[]))

    return (train_data, valid_data, test_data, len(token_counts))


from tensorflow.keras.layers import Embedding
from tensorflow.keras.layers import Bidirectional
from tensorflow.keras.layers import SimpleRNN
from tensorflow.keras.layers import LSTM
from tensorflow.keras.layers import GRU

def build_rnn_model(embedding_dim, vocab_size, recurrent_type="SimpleRNN",
                    n_recurrent_units=64, n_recurrent_layers=1, bidirectional=True):

    tf.random.set_seed(1)

    # buid the model
    model = tf.keras.Sequential()

    model.add(Embedding(
            input_dim = vocab_size,
            output_dim= embedding_dim,
            name='embed-layer'))

    for i in range(n_recurrent_layers):
        return_sequences = (i < n_recurrent_layers-1) # True apart from for final recurrent layer

        if recurrent_type == "SimpleRNN":
            recurrent_layer = SimpleRNN(
                units=n_recurrent_units,
                return_sequences=return_sequences,
                name="simprnn-layer-{}".format(i))
        elif recurrent_type == "LSTM":
            recurrent_layer = LSTM(
                units=n_recurrent_units,
                return_sequences=return_sequences,
                name="lstm-layer-{}".format(i))
        # This is another type of fancy RNN layer that can handle long sequences
        elif recurrent_type =="GRU":
            recurrent_layer = GRU(
                units=n_recurrent_units,
                return_sequences=return_sequences,
                name="gru-layer-{}".format(i))

        if bidirectional:
            recurrent_layer = Bidirectional(
                recurrent_layer, name='bdir-'+recurrent_layer.name)

        model.add(recurrent_layer)


    model.add(tf.keras.layers.Dense(64, activation='relu'))
    model.add(tf.keras.layers.Dense(1, activation='sigmoid'))

    return model



batch_size = 32
embedding_dim = 20
max_seq_length=100

train_data, valid_data, test_data, n = preprocess_datasets(
    ds_raw_train, ds_raw_valid, ds_raw_test, max_seq_len=max_seq_length,
    batch_size=batch_size)

vocab_size = n+2

# This example is cheaper than the first example, but (is supposed to be) less accurate
# The main point though is that for SimpleRNN to perform well, you need to truncate the sequence size to be not too large
# (100 words in this case via max_seq_length)

# Note also that this is much cheaper to train than the previous example (that used LSTM with no truncation)
rnn_model = build_rnn_model(
    embedding_dim, vocab_size,
    recurrent_type="SimpleRNN",
    n_recurrent_units=64,
    n_recurrent_layers=1,
    bidirectional=True)



rnn_model.compile(
    optimizer=tf.keras.optimizers.Adam(1e-3),
    loss=tf.keras.losses.BinaryCrossentropy(from_logits=False),
    metrics=['accuracy'])

print("Training the model")
history = rnn_model.fit(
    train_data,
    validation_data=valid_data,
    epochs=10)


test_results = rnn_model.evaluate(test_data)
print("Test Acc.: {:.2f}%".format(test_results[1]*100))
