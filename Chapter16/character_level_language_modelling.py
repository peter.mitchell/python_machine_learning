# Basically James' poetry example thing, where you feed in (e.g.) 10 chars and get a prediction for the next one
# including random sampling - such that you can use a "temperature" parameter
# Note I did not test the code after training, since it was pretty damn slow on my laptop (no GPU I guess!)
import numpy as np

# Read in a novel as a string
with open('1268-0.txt', 'r') as fp:
    text = fp.read()

start_indx = text.find('THE MYSTERIOUS ISLAND')
end_indx = text.find("End of the Project Gutenberg")
text = text[start_indx:end_indx]
print("Total Length:", len(text))


# Create a dictionary mapping each character to an integer
char_set = set(text) # Get unique characters in the text
chars_sorted = sorted(char_set)
char2int = {ch:i for i,ch in enumerate(chars_sorted)} # Assign each unique character a unique integer index

# Encode text (characters) into integers
text_encoded = np.array(
    [char2int[ch] for ch in text],
    dtype=np.int32)

# Convert into a tf dataset
import tensorflow as tf
ds_text_encoded = tf.data.Dataset.from_tensor_slices(text_encoded)


# Divide data into "chunks", where the input is 40 characeters, and the output is 40 characters incremented by one
seq_length = 40
chunk_size = seq_length+1

ds_chunks = ds_text_encoded.batch(chunk_size, drop_remainder=True)

# define function for splitting X,Y (input, output)
def split_input_target(chunk):
    input_seq = chunk[:-1]
    target_seq = chunk[1:]
    return input_seq, target_seq

# Turn each chunk into a tuple of (input, output)
ds_sequences = ds_chunks.map(split_input_target)


# Divide data into batches (as usual) (still not really clear why this is done! - I guess its due to the vectorized/pre-compiled
# nature of tensorflow - you can't afford to use batches that are too large because you run out of memory
# But using no batches (so serial mode) is less efficient I guess
BATCH_SIZE = 64
BUFFER_SIZE = 10000

ds = ds_sequences.shuffle(BUFFER_SIZE).batch(BATCH_SIZE)


### Now build an RNN model ###

def build_model(vocab_size, embedding_dim, rnn_units):
    model = tf.keras.Sequential([
        tf.keras.layers.Embedding(vocab_size, embedding_dim),
        tf.keras.layers.LSTM(
            rnn_units,
            return_sequences=True),
        tf.keras.layers.Dense(vocab_size)])

    return model

char_array = np.array(chars_sorted)
charset_size = len(char_array)
embedding_dim = 256
rnn_units = 512

tf.random.set_seed(1)
model = build_model(
    vocab_size=charset_size,
    embedding_dim=embedding_dim,
    rnn_units=rnn_units)

#print(model.summary())

# Apparently we want logits so that we can obtain the probability of different characters
# Not sure I fully understand why this is the way that achieves this
# Think it is because we are going to run the softmax function ourself after feeding in some init_text
# EDIT: logits are useful because we can multiply by a constant temperature parameter to change "temperature" - see later comments
model.compile(optimizer='adam',
              loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=False))

model.fit(ds, epochs=20)


# Now apply the model to generate some new (and randomized text)
tf.random.set_seed(1)

# The way this works is illustrated here
'''logits= [[1.0, 1.0, 1.0]]
# Softmax will correctly normalise the probability of each element to be 1/3 here
print('Probabilities:', tf.math.softmax(logits).numpy()[0])

# Example then of how to generate random samples using logits (softmax is done internally I think)
samples = tf.random.categorical(
    logits=logits, num_samples=10)
tf.print(samples.numpy())'''


# Given a starting string of characters, produce some text using the trained model
# scale_factor is the "temperature" parameter James was talking about for poetry
def sample(model, starting_str, 
           len_generated_text=500,
           max_input_length=10,
           scale_factor=1.0):
    # Encode
    encoded_input = [char2int[s] for s in starting_str]
    # Split into tuple of input, output (displaced by one)
    encoded_input = tf.reshape(encoded_input, (1,-1))

    generated_str = starting_str
    
    model.reset_states()
    for i in range(len_generated_text):
        logits = model(encoded_input)
        logits = tf.sqeeze(logits, 0) # Get rid of empty first axis (batch dimension I think, which is redundant here)
        
        # Get a new character
        # Recall logit is related to logarithm of probability, so multiplying by a constant will affect the PDF
        # after converted into linear probabilitiy and then normalized (this is dones within tf.random.categorical)
        scaled_logits = logits * scale_factor
        new_char_indx = tf.random.categorical(scaled_logits, num_samples=1)
        
        # Remove redundant empty dimensions
        new_char_indx = tf.squeeze(new_char_indx)[-1].numpy()
        
        # Convert integer back into string (characeter)
        generated_str += str(char_array[new_char_indx])
                           
        # Update the encoded input to get ready for the next iteration through the loop
        # And also reshape to have suitable dims for the model evaluation on next step
        new_char_indx = tf.expand_dims([new_char_indx], 0)
        encoded_input = tf.concat(
            [encoded_input, new_char_indx],
            axis=1)

        # Truncate the input size
        encoded_input = encoded_input[:, -max_input_length:]

    return generated_text


# Note I think for this to work the input sequence (starting_str) cannot be shorter than 10 characters??
tf.random.set_seed(1)
print ("")
print ("alpha=1 case")
print(sample(model, starting_str='The island'))


tf.random.set_seed(1)
print ("")
print ("alpha=2 case (low temperature boring version)")
print(sample(model, staring_str='The island', scale_factor=2.0))


tf.random.set_seed(1)
print ("")
print ("alpha=0.5 case (high temperature exciting version")
print(sample(model, staring_str='The island', scale_factor=2.0))
