import tensorflow as tf
tf.random.set_seed(1)

# SimpleRNN uses output-output recurrence. I.e., the output layer receives input from the hidden layer, and from the previous output layer

# Return sequences - choose whether output is for all the elements in the sequence (True), or just the final element (False)
# Presumably there is some activation function used by default, but I don't what it is atm
# Edit - tanh
rnn_layer = tf.keras.layers.SimpleRNN(units=2, use_bias=True, return_sequences=True)

# Input dimensions are batch size, sequence size, no. features (e.g., vocabulary size)
rnn_layer.build(input_shape=(None,None,5))

###### Take a look at the parameters #########
w_xh, w_oo, b_h = rnn_layer.weights

# Input-hidden
print('W_xh shape:', w_xh.shape)
# Prev output - current output
print('W_oo shape:', w_oo.shape)
# Bias-hidden
print('b_h shape:', b_h.shape)


###### Manually check the steps involved #########

# Dummy input
x_seq = tf.convert_to_tensor([[1.0]*5, [2.0]*5, [3.0]*5], dtype=tf.float32)

output = rnn_layer(tf.reshape(x_seq, shape=(1,3,5)))

# Manually compute the same thing

out_man = []

for t in range(len(x_seq)):
    xt = tf.reshape(x_seq[t], (1,5)) # have to reshape it so that we have the "batch" dimension out front, even after indexing

    print("time step {} =>".format(t))
    print("   Input: ", xt.numpy())

    # Hidden layer
    ht = tf.matmul(xt, w_xh) + b_h
    print('   Hidden:', ht.numpy())

    # Prev out > Current out
    if t > 0:
        prev_o = out_man[t-1]
    else:
        prev_o = tf.zeros(shape=(ht.shape))

    # (Current) Output layer
    ot = ht + tf.matmul(prev_o, w_oo)
    ot = tf.math.tanh(ot) # tanh activation
    
    out_man.append(ot)
    print(    'Output (manual) :', ot.numpy())
    print(    'SimpleRNN output:'.format(t), output[0][t].numpy())
    print()
