# Many -> One RNN using the IMDB data introduced in chapter 8
# This is the main example using a "LSTM" layer (page 582) - long short term memory

import tensorflow as tf
import tensorflow_datasets as tfds
import numpy as np
import pandas as pd
print()

df = pd.read_csv("../Chapter8/movie_data.csv", encoding="utf-8")

target = df.pop("sentiment")
ds_raw = tf.data.Dataset.from_tensor_slices((df.values, target.values))

#for example in ds_raw.take(3):
#    tf.print(example[0].numpy()[0][:50], example[1])

tf.random.set_seed(1)
ds_raw = ds_raw.shuffle(50000, reshuffle_each_iteration=False)

ds_raw_test = ds_raw.take(25000)
ds_raw_train_valid = ds_raw.skip(25000)
ds_raw_train = ds_raw_train_valid.take(20000)
ds_raw_valid = ds_raw_train_valid.skip(20000)


### First we encode words into numeric values ###
from collections import Counter

# Note since the edition of my book likes like this tokenizer (and the "text" module in general) get deprecated
tokenizer = tfds.deprecated.text.Tokenizer()
token_counts = Counter()

# Build the vocabulary, and count the number of times each word occurs
for example in ds_raw_train:
    tokens = tokenizer.tokenize(example[0].numpy()[0]) # Note the indexing here is just extracting the list of (all the) word strings in the review
    token_counts.update(tokens)

print('Vocab-size:', len(token_counts))

# Now encode the unique tokens (words) into unique integers
encoder = tfds.deprecated.text.TokenTextEncoder(token_counts)

#example_str = "this is an example"
#print(encoder.encode(example_str))

# We now want to encode all the data using the encoder using the .map() method, but this is problematic apparently
# because the encoder doesn't work on tensor (i.e. optimized) data directly (despite coming from tfds lol)
# So we have this odd looking double function thing here using also the tf.py_function (like a decorator) to turn
# the function into a (pre-compiled) tensorflow operator
def encode(text_tensor, label):
    text = text_tensor.numpy()[0]
    encoded_text = encoder.encode(text)
    return encoded_text, label

def encode_map_fn(text, label):
    return tf.py_function(encode, inp=[text, label], Tout=(tf.int64, tf.int64))

ds_train = ds_raw_train.map(encode_map_fn)
ds_valid = ds_raw_valid.map(encode_map_fn)
ds_test = ds_raw_test.map(encode_map_fn)

# Now we introduce the concept of padded batching, where the max review length in the batch sets the sequence size
# and smaller reviews are padded with zeros (note zero has a significance in the encoded data as a padded value)

# 32 here is the batch size, the shapes I thing I believe just tells it to use the longest review to set the sequence size (?)
train_data = ds_train.padded_batch(32, padded_shapes=([-1],[]))
valid_data = ds_valid.padded_batch(32, padded_shapes=([-1],[]))
test_data = ds_test.padded_batch(32, padded_shapes=([-1],[]))

### After encoding, we now use another trick - called embedding ####
# The point here is that we need that we need to convert the unique integers of each word into
# a format that will work in a Machine Learning classifier
# To do this, we convert each integer into a vector of floating point numbers
# e.g. [233] -> [0.1, -0.5, -0.22132, 0.61234]
# The key part is this mapping is done with a trainable weight matrix (hence this is a keras layer)
# and thus the classifier essentially learns itself how to best represent the input data as
# a standardized set of floats bounded within +/-1

from tensorflow.keras.layers import Embedding

# In this commented out example, I think that output_dim is the number of floats produced by each integer input (word)
# Input dim is maybe the size of the vocabulary (?)
# And input_length is the size of the reviews (generally padded to max in the batch)
'''model = tf.keras.Sequential()

model.add(Embedding(input_dim=100,
                    output_dim=6,
                    input_length=20,
                    name='embed-layer'))'''


#### Now we build the classifier, using embedding as the first layer #####

# How many output floats are produced for each word fed into the embedding layer
embedding_dim = 20

# +2 because 0 indicates padded value, and max(token_counts)+1 is reserved for unknown words (which will be encountered in valid/test)
vocab_size = len(token_counts)+2

tf.random.set_seed(1)

bi_lstm_model = tf.keras.Sequential([
        tf.keras.layers.Embedding(
            input_dim = vocab_size,
            output_dim= embedding_dim,
            name='embed-layer'),
        
        # Bidirectional wraps around another layer - it means we go through each word in the sequence (review)
        # And then reverse direction, and go back to the first word again
        tf.keras.layers.Bidirectional(
            # LSTM is fancy RNN layer than efficiently retains useful information from much further back in the sequence (review)
            # It is needed when you have long sequences (as we do here)
            tf.keras.layers.LSTM(64, name='lstm-layer'),
            name='bidir-lstm'),
        
        tf.keras.layers.Dense(64, activation="relu"),

        tf.keras.layers.Dense(1, activation="sigmoid")
        ])


print(bi_lstm_model.summary())

bi_lstm_model.compile(
    optimizer=tf.keras.optimizers.Adam(1e-3),
    loss=tf.keras.losses.BinaryCrossentropy(from_logits=False),
    metrics=['accuracy'])

print("Training the model")
history = bi_lstm_model.fit(
    train_data,
    validation_data=valid_data,
    epochs=10)


test_results = bi_lstm_model.evaluate(test_data)
print("Test Acc.: {:.2f}%".format(test_results[1]*100))
