import pandas as pd
import numpy as np

np.random.seed(123)

variables = ["X", "Y", "Z"]
labels = ["ID_0", "ID_1", "ID_2", "ID_3", "ID_4"]

X = np.random.random_sample([5,3])*10

df = pd.DataFrame(X, columns=variables, index=labels)

# First build a pairwise distance matrix using scipy
from scipy.spatial.distance import pdist, squareform

# This is a 1d array. Elements are the top-side off-diagonal elements of the 2d symmetric pairwise distance matrix
# (Obviously the diagonal elements are all zero)
pdist_flat = pdist(df, metric="euclidean")

# This just transforms that data into a full symmetric 2d distance matrix (note this is not used later)
# Just create it for illustration purposes
row_dist = pd.DataFrame(squareform(pdist_flat), columns=labels, index=labels)
#print(row_dist)

######### Now use a hierarchical clustering algorithm ############
from scipy.cluster.hierarchy import linkage

# method="complete" means that for each pair of clusters, we find the pair of two elements (one from each cluster)
# where the distance between them is maximal
# Of all these maximal pair distances, we merge the two clusters where this distance is minimal
# This is done incrementally, with one merge (I think) performed per step
# This is repeated until eventually all elements have been merged into a single cluster
# Initial state is each element is a separate cluster
row_clusters = linkage( pdist_flat, method="complete")

# The output of linkage is a 2d array.
# Each row is a "cluster" - note all clusters in the hierarchy are here - one of them simply contains all the elements
# First two columns are the element index of the two most distant elements within that cluster
# Third column is that maximal intra-cluster distance
# Col 4 is the elements within the cluster (note therefore that the index of all cluster members is not given)

print( pd.DataFrame(row_clusters,
                   columns=["row label 1", "row label 2", "distance", "No of items in clust."],
                   index = ["cluster %d" % (i+1) for i in range(row_clusters.shape[0])]) )

# Visualize as a dendrogram
from scipy.cluster.hierarchy import dendrogram
import matplotlib.pyplot as plt

row_dendr = dendrogram( row_clusters, labels=labels)
plt.tight_layout()
plt.ylabel("Euclidean distance")
plt.show()


# Visualize as a dendrogram with a heatmap
# The idea is the coords of the elements are printed as a heatmap, next to the dengrogram
# So we can see visually if the elements that are grouped are actually clustered in terms of the colours
# Seems a little unecessary but ok

fig = plt.figure(figsize=(8,8), facecolor="white")

axd = fig.add_axes([ 0.09, 0.1, 0.2, 0.6])

row_dendr= dendrogram(row_clusters, orientation="left")

# ["leaves"] item gives the order with each element appears in the dendrogram, from left-to-right
# (what sets that order in turn I am not fully sure!)
#print (row_dendr["leaves"])

df_rowclust = df.iloc[row_dendr['leaves'][::-1]]
# This is a 2d array of the leaf elements, sorted in the order of the dengrogram
# Row is element, columns are x,y,z coords

axm = fig.add_axes([0.23, 0.1, 0.6, 0.6])
cax = axm.matshow(df_rowclust, interpolation="nearest", cmap="hot_r")

axd.set_xticks([])
axd.set_yticks([])

for i in axd.spines.values():
    i.set_visible(False)

fig.colorbar(cax)

axm.set_xticklabels([''] +list(df_rowclust.columns))
axm.set_yticklabels([''] + list(df_rowclust.index))

plt.show()
