import pandas as pd
import numpy as np

np.random.seed(123)

variables = ["X", "Y", "Z"]
labels = ["ID_0", "ID_1", "ID_2", "ID_3", "ID_4"]

X = np.random.random_sample([5,3])*10

df = pd.DataFrame(X, columns=variables, index=labels)

from sklearn.cluster import AgglomerativeClustering

# n_clusters=3 - stop when there are 3 clusters that enclose all the points
# Note this isn't very clear how this is done. Are the clusters mutually exclusive ?
# "complete" - merge clusters based on most separated pair of elements
ac = AgglomerativeClustering(n_clusters=3, affinity="euclidean", linkage="complete")

labels = ac.fit_predict(X)

print("Cluster labels: %s" % labels)
