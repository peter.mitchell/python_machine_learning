# Page 363 - a visual diagnostic of the performance of a clustering algorithm
# Applied in this case to K-means

from sklearn.datasets import make_blobs

X, y = make_blobs(n_samples=150, n_features=2, centers=3, cluster_std=0.5, shuffle=True, random_state=0)

from sklearn.cluster import KMeans

# n_clusters=3 is the optimal case. But try changing to 2 or 4 to see how the silhouette plot indicates a poor choice of n_clusters
km = KMeans(n_clusters=3, init="k-means++", n_init=10, max_iter=300, tol=1e-4, random_state=0)

y_km = km.fit_predict(X)

import numpy as np
from sklearn.metrics import silhouette_samples

cluster_labels = np.unique(y_km)
n_clusters = cluster_labels.shape[0]

# For each point in a cluster, compare average distance within cluster to average distance to examples in the nearest adjacent cluster
# +1 is where clusters are very cleanly separated, -1 is where clusters are completely blended
silhouette_vals = silhouette_samples(X, y_km, metric="euclidean")

# So every point has a silhouette_val, for some reason this is plotted as a "hbar" plot

# The important point is that the shape of each cluster should ideally by close on average to +1, and should look similar to each
# other
# If you don't have the correct number of clusters, one cluster silhouette will most likely look very different to another
# You can verify that here just by changing the n_clusters from 3 to 2 (or 4 or whatever) above

import matplotlib.pyplot as plt
from matplotlib import cm
y_ax_lower, y_ax_upper = 0, 0

y_ticks = []

for i, c in enumerate(cluster_labels):
    c_silhouette_vals = silhouette_vals[y_km==c]
    c_silhouette_vals.sort()

    y_ax_upper += len(c_silhouette_vals)

    color = cm.jet(float(i) / n_clusters)
    plt.barh(range(y_ax_lower, y_ax_upper), c_silhouette_vals, height=1.0, edgecolor="none", color=color)

    y_ticks.append((y_ax_lower + y_ax_upper) / 2.)
    y_ax_lower += len(c_silhouette_vals)

silhouette_avg = np.mean(silhouette_vals)
plt.axvline(silhouette_avg, color="red", linestyle="--")

plt.yticks(y_ticks, cluster_labels+1)
plt.ylabel("Cluster")
plt.xlabel("Silhouette coefficient")
plt.tight_layout()
plt.show()

