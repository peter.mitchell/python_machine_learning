from sklearn.datasets import make_moons

X, y = make_moons(n_samples=200, noise=0.05, random_state=0)

from sklearn.cluster import DBSCAN

# "eps" > epsilon is the top-hat kernel radius used to evaluate the local point density around each point
# min_samples is the number of points within eps to considered a "core point". 
# In 2d, min_samples / pi eps^2 would be the surface density threshold.

# Points are labelled as "core" (pass threshold), "border" (within eps of another core point, but fail threshold), or "noise"
# Core points are clustered via FoF with eps as the linking length
# Border points are assigned to the cluster of their associated core point 
# (not clear if there are two nearby core points in different clusters however)

db = DBSCAN(eps=0.2, min_samples=5, metric="euclidean")

y_db = db.fit_predict(X)


import matplotlib.pyplot as plt

plt.scatter(X[y_db==0, 0], X[y_db==0, 1], c="lightblue", edgecolor="black", marker="o", s=40, label="Cluster 1")

plt.scatter(X[y_db==1, 0], X[y_db==1, 1], c="red", edgecolor="black", marker="s", s=40, label="Cluster 2")

plt.legend()

plt.tight_layout()

plt.show()
