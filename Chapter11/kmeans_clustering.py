from sklearn.datasets import make_blobs

X, y = make_blobs(n_samples=150, n_features=2, centers=3, cluster_std=0.5, shuffle=True, random_state=0)

from sklearn.cluster import KMeans

# n_clusters - how many clusters :)
# n_init - how many independent sets of random centroids we use (I believe the centroid set with the lowest final error is chosen)
# max_iter - maximum number of iterations before we give up if the error doesn't drop below the tolerance
# tol = tolerance (problem depenendent I suppose)

km = KMeans(n_clusters=3, init="random", n_init=10, max_iter=300, tol=1e-4, random_state=0)

y_km = km.fit_predict(X)





import matplotlib.pyplot as plt

#plt.scatter(X[:,0], X[:,1], c="white", marker="o", edgecolor="black", s=50)

plt.scatter(X[y_km==0, 0], X[y_km==0, 1], s=50, c="lightgreen", marker="s", edgecolors="black", label="Cluster 1")

plt.scatter(X[y_km==1, 0], X[y_km==1, 1], s=50, c="orange", marker="o", edgecolors="black", label="Cluster 2")

plt.scatter(X[y_km==2, 0], X[y_km==2, 1], s=50, c="lightblue", marker="v", edgecolors="black", label="Cluster 3")

# Also show the cluster centers
plt.scatter(km.cluster_centers_[:,0], km.cluster_centers_[:,1], s=250, marker="*", c="red", edgecolors="black", label="Centroids")

plt.legend(scatterpoints=1)
plt.grid()
plt.tight_layout()
plt.show()


###### Finding the optimal number of clusters #######
# "Elbow plot" - visually see that 3 is the optimal number based on the gradient of the plot (page 362)
distortions = []

for i in range(1,11):

    # k-means++ refers to a strategy for how to place the initial random centroid positions
    # (page 358) such that the centroids are properly sampling the space

    km = KMeans(n_clusters=i, init="k-means++", n_init=10, max_iter=300, random_state=0)

    km.fit(X)

    # For each cluster, inertia gives the sum-squared error (variance), then average over the clusters I guess
    distortions.append(km.inertia_)

plt.figure()
plt.plot(range(1,11), distortions, marker="o")

plt.xlabel("Number of clusters")
plt.ylabel("Distortion")

plt.tight_layout()
plt.show()
