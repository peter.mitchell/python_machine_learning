# Decision tree regression (page 346)
import pandas as pd
import numpy as np

df = pd.read_csv('https://raw.githubusercontent.com/rasbt/'
                 'python-machine-learning-book-3rd-edition'
                 '/master/ch10/housing.data.txt',
                 header=None, sep='\s+')

df.columns = ["CRIM", "ZN", "INDUS", "CHAS", "NOX", "RM", "AGE", "DIS", "RAD", "TAX", "PTRATIO", "B", "LSTAT", "MEDV"]

X = df[["LSTAT"]].values
y = df["MEDV"].values


from sklearn.tree import DecisionTreeRegressor

tree = DecisionTreeRegressor(max_depth=3)

tree.fit(X, y)

sort_idx = X.flatten().argsort()

import matplotlib.pyplot as plt

plt.figure()

def lin_regplot(X, y, model):
    plt.scatter(X, y, c="steelblue", edgecolor="white", s=70)
    plt.plot(X, model.predict(X), color="black", lw=2)
    return None

lin_regplot(X[sort_idx], y[sort_idx], tree)

plt.xlabel("% lower status of the population [LSTAT]")
plt.ylabel("Price in $1000s [MEDV]")

plt.show()
