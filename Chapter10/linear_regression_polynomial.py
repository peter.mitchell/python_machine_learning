# Linear regression with least-squares optimization (default in scikit learn)
# Examples of fitting polynomials this time (page 342)
# Note actually best fit as an exponential decay, but nvm :p
import pandas as pd
import numpy as np

df = pd.read_csv('https://raw.githubusercontent.com/rasbt/'
                 'python-machine-learning-book-3rd-edition'
                 '/master/ch10/housing.data.txt',
                 header=None, sep='\s+')

df.columns = ["CRIM", "ZN", "INDUS", "CHAS", "NOX", "RM", "AGE", "DIS", "RAD", "TAX", "PTRATIO", "B", "LSTAT", "MEDV"]

X = df[["LSTAT"]].values
y = df["MEDV"].values

from sklearn.linear_model import LinearRegression

regr = LinearRegression()

# To apply linear regression but with a polynomial fit, we need to transform X into a new feature space
# [ones, X, X**2] for quadratic
# Note I don't know why the ones (just a column of ones) is in there, but ok

# Create quadratic and cubic features
from sklearn.preprocessing import PolynomialFeatures

quadratic = PolynomialFeatures(degree=2)
cubic = PolynomialFeatures(degree=3)

X_quad = quadratic.fit_transform(X)
X_cubic = cubic.fit_transform(X)

#print(X_quad[:10])
#print(X[:10])
#quit()

######### Linear ##########################
# Not a fit, just the grid we use to plot the best-fit line
X_fit = np.arange(X.min(), X.max(), 1)[:, None]

regr = regr.fit(X, y)
y_lin_fit = regr.predict(X_fit)

# See page 337, an accuracy metric based on residuals - 0 is disaster, 1 is perfect
from sklearn.metrics import r2_score
linear_r2 = r2_score(y, regr.predict(X))



#### Quadratic #############
regr = regr.fit(X_quad, y)
y_quad_fit = regr.predict(quadratic.fit_transform(X_fit))
quadratic_r2 = r2_score(y, regr.predict(X_quad))

#### Cubic #############
regr = regr.fit(X_cubic, y)
y_cubic_fit = regr.predict(cubic.fit_transform(X_fit))
cubic_r2 = r2_score(y, regr.predict(X_cubic))



import matplotlib.pyplot as plt

plt.figure()

plt.scatter(X, y, label="Training points", color="lightgray")

plt.plot(X_fit, y_lin_fit, label="Linear (d=1), R^2=%.2f" % linear_r2, color="blue", lw=2, linestyle=":")
plt.plot(X_fit, y_quad_fit, label="Quadratic (d=2), R^2=%.2f" % quadratic_r2, color="red", lw=2, linestyle="-")
plt.plot(X_fit, y_cubic_fit, label="Cubic (d=3), R^2=%.2f" % cubic_r2, color="green", lw=2, linestyle="--")

plt.xlabel("% lower status of the population [LSTAT]")
plt.xlabel("Price in $1000s [MEDV]")
plt.legend(loc="upper right")
plt.show()
