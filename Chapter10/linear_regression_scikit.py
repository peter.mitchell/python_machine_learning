# Linear regression with least-squares optimization (default in scikit learn)
import pandas as pd
import numpy as np

df = pd.read_csv('https://raw.githubusercontent.com/rasbt/'
                 'python-machine-learning-book-3rd-edition'
                 '/master/ch10/housing.data.txt',
                 header=None, sep='\s+')

df.columns = ["CRIM", "ZN", "INDUS", "CHAS", "NOX", "RM", "AGE", "DIS", "RAD", "TAX", "PTRATIO", "B", "LSTAT", "MEDV"]

X = df[["RM"]].values
y = df["MEDV"].values

# Default LinearRegression algorithm from scikit learn uses LeastSquares - linear algebra
# And therefore does not require standardized data (unlike gradient descent)

from sklearn.linear_model import LinearRegression

slr = LinearRegression()

slr.fit(X, y)

import matplotlib.pyplot as plt

plt.figure()

def lin_regplot(X, y, model):
    plt.scatter(X, y, c="steelblue", edgecolor="white", s=70)
    plt.plot(X, model.predict(X), color="black", lw=2)
    return None

lin_regplot(X, y, slr)

plt.xlabel("Average number of rooms [RM]")
plt.xlabel("Price in $1000s [MEDV]")

plt.show()
