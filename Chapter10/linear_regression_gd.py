# Linear regression gradient descent (page 326)
import pandas as pd
import numpy as np

df = pd.read_csv('https://raw.githubusercontent.com/rasbt/'
                 'python-machine-learning-book-3rd-edition'
                 '/master/ch10/housing.data.txt',
                 header=None, sep='\s+')

df.columns = ["CRIM", "ZN", "INDUS", "CHAS", "NOX", "RM", "AGE", "DIS", "RAD", "TAX", "PTRATIO", "B", "LSTAT", "MEDV"]


# This is the same as the Adaline gradient descent class from Chapter 2
# The only difference being we don't have a (linear) activation function transforming predicted y into +/- 1
class LinearRegressionGD(object):
    
    def __init__(self, eta=0.001, n_iter=20):
        self.eta = eta
        self.n_iter = 20

    def fit(self, X, y):
        self.w_ = np.zeros(1+X.shape[1])
        self.cost_ = []

        for i in range(self.n_iter):
            output = self.net_input(X)
            errors = (y-output)
            # Note in this context, w[0] is the y-intercept, w[1] is the slope (if only one feature column)
            self.w_[1:] += self.eta * X.T.dot(errors)
            self.w_[0] += self.eta*errors.sum()
            cost = (errors**2).sum() / 2.0
            self.cost_.append(cost)
        return self

    def net_input(self, X):
        return np.dot(X, self.w_[1:]) + self.w_[0]

    # In this case the prediction is just the net input - there is no "neural" activation function
    def predict(self, X):
        return self.net_input(X)


X = df[["RM"]].values
y = df["MEDV"].values

from sklearn.preprocessing import StandardScaler

sc_x = StandardScaler()
sc_y = StandardScaler()

X_std = sc_x.fit_transform(X)
# Standard scalar assumes input is a 2d array
y_std = sc_y.fit_transform(y[:, None]).flatten()

lr = LinearRegressionGD()

lr.fit(X_std, y_std)

import matplotlib.pyplot as plt

# Check convergence
plt.plot(range(1, lr.n_iter+1), lr.cost_)
plt.ylabel('SSE') # Summed-square error (I think)
plt.xlabel('Epoch')
plt.show()


# Then check the quality of the fit
plt.figure()

def lin_regplot(X, y, model):
    plt.scatter(X, y, c="steelblue", edgecolor="white", s=70)
    plt.plot(X, model.predict(X), color="black", lw=2)
    return None

lin_regplot(X_std, y_std, lr)

plt.xlabel("Average number of rooms [RM] (standardized)")
plt.xlabel("Price in $1000s [MEDV] (standardized)")

plt.show()
