# Linear regression using least-squares with scikit, and this time with the RANdom SAmple Consensus (RANSAC) algorithm
# also applied. This is a scheme to reject outliers before performing the final regression
# (page 332)

import pandas as pd
import numpy as np

df = pd.read_csv('https://raw.githubusercontent.com/rasbt/'
                 'python-machine-learning-book-3rd-edition'
                 '/master/ch10/housing.data.txt',
                 header=None, sep='\s+')

df.columns = ["CRIM", "ZN", "INDUS", "CHAS", "NOX", "RM", "AGE", "DIS", "RAD", "TAX", "PTRATIO", "B", "LSTAT", "MEDV"]

X = df[["RM"]].values
y = df["MEDV"].values

from sklearn.linear_model import LinearRegression, RANSACRegressor

# Idea of Ransac
# Choose at random a number of points (without replacement) from the main sample - label these "inliers"
# Fit the model to the inliers
# Evaluate the model for all the main sample, find the points where the model-data residual is less than the residual_threshold
# Relabel the inliers as the original inlier set - plus extra points that meet this residual threshold requirement
# Re-fit the model to the new inlier set
# Estimate the total error of the model for the inlier set
# If the error is less than the residual_theshold, we achieve convergence, if not try again
# Note (I think) no information is passed between iterations here

# max_trials - maxmimum number of iterations if convergence is not reached
# min_samples - the mimimum number of "inliers" selected radomly for each iteration
# loss="absolute_loss" - how to compute the model error that is compared to the convergence threshold
# residual_threshold - the convergence threshold

ransac = RANSACRegressor(LinearRegression(),
                         max_trials=100,
                         min_samples=50,
                         loss="absolute_loss",
                         residual_threshold = 5.0,
                         random_state=0)

ransac.fit(X, y)

inlier_mask = ransac.inlier_mask_
outlier_mask = np.logical_not(inlier_mask)

line_X = np.arange(3, 10, 1)
line_y_ransac = ransac.predict(line_X[:, None])

import matplotlib.pyplot as plt

plt.figure()

plt.scatter(X[inlier_mask], y[inlier_mask], c="steelblue", edgecolor="white", marker="o", label="Inliers")
plt.scatter(X[outlier_mask], y[outlier_mask], c="limegreen", edgecolor="white", marker="s", label="Outliers")

plt.plot(line_X, line_y_ransac, color="black", lw=2)

plt.xlabel("Average number of rooms [RM]")
plt.xlabel("Price in $1000s [MEDV]")

plt.show()
