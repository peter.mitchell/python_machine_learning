# Random forest regression (page 348)
import pandas as pd
import numpy as np

df = pd.read_csv('https://raw.githubusercontent.com/rasbt/'
                 'python-machine-learning-book-3rd-edition'
                 '/master/ch10/housing.data.txt',
                 header=None, sep='\s+')

df.columns = ["CRIM", "ZN", "INDUS", "CHAS", "NOX", "RM", "AGE", "DIS", "RAD", "TAX", "PTRATIO", "B", "LSTAT", "MEDV"]

X = df.iloc[:, :-1].values
y = df["MEDV"].values

from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.4, random_state=1)

from sklearn.ensemble import RandomForestRegressor

# n_estimators is the how many random-sub-samples of the training set are used (each has its own decision tree trained)
# criterion="mse" means the impurity criterion is mean squared error (since we are working with continuous variables now)

forest = RandomForestRegressor(n_estimators=1000,
                               criterion="mse",
                               random_state=1,
                               n_jobs=1)

forest.fit(X_train, y_train)

y_train_pred = forest.predict(X_train)
y_test_pred = forest.predict(X_test)


import matplotlib.pyplot as plt

plt.figure()

plt.scatter(y_train_pred, y_train_pred - y_train, c="steelblue", edgecolor="white", marker="o", s=35, alpha=0.9, label="Training data")

plt.scatter(y_test_pred, y_test_pred - y_test, c="limegreen", edgecolor="white", marker="o", s=35, alpha=0.9, label="Test data")

plt.xlabel("Predicted values")
plt.ylabel("Residuals")

plt.show()
