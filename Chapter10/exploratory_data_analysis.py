import pandas as pd

df = pd.read_csv('https://raw.githubusercontent.com/rasbt/'
                 'python-machine-learning-book-3rd-edition'
                 '/master/ch10/housing.data.txt',
                 header=None, sep='\s+')

df.columns = ["CRIM", "ZN", "INDUS", "CHAS", "NOX", "RM", "AGE", "DIS", "RAD", "TAX", "PTRATIO", "B", "LSTAT", "MEDV"]


# First make a scatter plot between pairs of (a subset of the) variables
import matplotlib.pyplot as plt
from mlxtend.plotting import scatterplotmatrix

cols = ["LSTAT", "INDUS", "NOX", "RM", "MEDV"]

scatterplotmatrix(df[cols].values, figsize=(10,8), names=cols, alpha=0.5)
plt.tight_layout()
plt.show()

# Next, make a correlation matrix visualization of the same (subset) variable pairs

from mlxtend.plotting import heatmap
import numpy as np

# Each element in the matrix is a Pearsons correlation coefficient, +1 implies perfect positive correlation,
# -1 implies perfect anti-correlation, 0 implies no correlation
cm = np.corrcoef(df[cols].values.T)

hm = heatmap(cm, row_names=cols, column_names=cols)

plt.show()
