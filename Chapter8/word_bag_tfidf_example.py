import numpy as np

from sklearn.feature_extraction.text import CountVectorizer

count = CountVectorizer()

docs = np.array(["The sun is shining",
                 "The weather is sweet",
                 "The sun is shining, the weather is sweet, and one and one is two"])

bag = count.fit_transform(docs)

print(count.vocabulary_)

print(bag.toarray())

# Weight words by how often they occur in a single document, and by how infrequently they occur between documents
# "Term Frequency-Inverse Document Frequency" (tfidf)

from sklearn.feature_extraction.text import TfidfTransformer

# Norm chooses how we normalise. L2 means the sqrt(sum(square(weight))) for a given document is equal to one.

# Smooth idf is a "+1" in the numerator/denominator of idf, in effects sets weights of words that are in all documents to zero
# (Probably helps with sparse matrix implementations or something)

tfidf = TfidfTransformer(use_idf=True, norm="l2", smooth_idf=True)

tfidf = tfidf.fit_transform(bag).toarray()
print(tfidf)
