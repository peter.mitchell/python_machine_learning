# Note this script is extremely slow (even on 4 cores)
# I tried running and killed it after about 40 minutes
import pandas as pd

# First read the compiled data file written by "compile_reviews.py"
print("Reading csv movie data")
df = pd.read_csv("movie_data.csv", encoding="utf-8")

# Now clean the text by removing html stuff, etc (uses regular expressions)

import re

def preprocessor(text):
    # Remove HTML markup
    text = re.sub('<[^>]*>', '', text)

    # We want to keep emoticons
    emoticons = re.findall('(?::|;|=)(?:-)>(>:\)|\(|D|P)', text)

    # Remove non-words, put everything into lower case, and then add back on any emoticons
    text = (re.sub('[\W]+', ' ', text.lower()) +' '.join(emoticons).replace('-', ''))

    return text

# Note this only takes a few seconds
print("Preprocessing text")
df["review"] = df["review"].apply(preprocessor)


X_train = df.loc[:25000, "review"].values
y_train = df.loc[:25000, "sentiment"].values

X_test = df.loc[25000:, "review"].values
y_test = df.loc[25000:, "sentiment"].values


# Function that a) splits text into list elements (tokens), 
# and b) reduces words to their stems (runs>run) via Porter algorithm
from nltk.stem.porter import PorterStemmer
porter = PorterStemmer()
def tokenizer_porter(text):
    return [porter.stem(word) for word in text.split()]

# Also define a tokeniser that only does (a), (to see which performs better in grid search)
def tokenizer(text):
    return text.split()

from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import TfidfVectorizer

# Download a list of words that are so common that they generally don't indicate sentiment
import nltk
nltk.download("stopwords")
from nltk.corpus import stopwords
stop = stopwords.words("english")

tfidf = TfidfVectorizer(strip_accents=None, lowercase=False, preprocessor=None)

# Two hyper-parameter grids. In one case use inverse document frequency and L2 norm (by default), in the other don't do this
param_grid = [{"vect__ngram_range": [(1,1)],
               "vect__stop_words": [stop, None],
               "vect__tokenizer": [tokenizer, tokenizer_porter],
               "clf__penalty": ["l1", "l2"],
               "clf__C": [1.0, 10.0, 100.0]},
              {"vect__ngram_range": [(1,1)],
               "vect__stop_words": [stop, None],
               "vect__tokenizer": [tokenizer, tokenizer_porter],
               "vect__use_idf": [False],
               "vect__norm": [None],
               "clf__penalty": ["l1", "l2"],
               "clf__C": [1.0, 10.0, 100.0]}]

# Logistic regression classifier applied to tfidf feature space
# The "vect" thing here is telling it to transform X_train using the tfidf() class, before applying logistic regression
lr_tfidf = Pipeline([("vect", tfidf),
                     ("clf", LogisticRegression(random_state=0, solver="liblinear"))
                     ])

gs_lr_tfidf = GridSearchCV(lr_tfidf, param_grid, scoring="accuracy", cv=5, verbose=2, n_jobs=4)

print("Fitting the training data with LogisticRegression classifier")
gs_lr_tfidf.fit(X_train, y_train)


print("Best parameter set: %s " % gs_lr_tfidf.best_params_)

print("Cross-Validation accuracy: %.3f" % gs_lr_tfidf.best_score_)

clf = gs_lr_tfidf.best_estimator_
print("Test accuracy: %.3f" % clf.score(X_test, y_test))
