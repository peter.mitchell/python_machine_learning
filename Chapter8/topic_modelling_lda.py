# First example in the book of unsupervised machine learning - topic modelling via "Latent Dirichlet Allocation" (LDA)
# (not to be confused with with Linear Discriminant Analysis, which is also LDA)
# To be fair, this feels more like we are doing PCA or Linear Discriminant Analysis, i.e. we are finding the "topics"
# that are like eigen vectors of the word bag matrix

import pandas as pd
df = pd.read_csv("movie_data.csv", encoding="utf-8")

from sklearn.feature_extraction.text import CountVectorizer


# Use scikit built-in stop words list (very common words that can be ignored)

# max_df = maximum document frequency - exclude words that occur in more than 10% of the documents
# (note this value is probably chosen to coincide with the 10 topics we will choose later)

# max_features = 5000 - only keep the 5000 most common words, to avoid having too many dimensions for the LDA to fit

count = CountVectorizer(stop_words="english", max_df=0.1, max_features=5000)

# Build the bag of words matrix
X = count.fit_transform(df["review"].values)

from sklearn.decomposition import LatentDirichletAllocation

lda = LatentDirichletAllocation(n_components=10, random_state=123, learning_method="batch")

print("Starting the LDA fit (will take a while)")
X_topics = lda.fit_transform(X)

# After fitting, lda now contents an array "lda.components_" with shape (10,5000)
# Where first axis is the topic, and the second axis is a sorted order of the word importance for that topic (ascending order)

n_top_words = 5
feature_names = count.get_feature_names()

for topic_idx, topic in enumerate(lda.components_):
    print("Topic %d:" % (topic_idx+1))
    print(" ".join([feature_names[i] for i in topic.argsort()[:-n_top_words -1:-1]]) )

