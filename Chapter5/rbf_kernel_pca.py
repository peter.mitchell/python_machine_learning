# RBF stands for Radial Basis Function (Gaussian) kernel
import numpy as np
from scipy.spatial.distance import pdist, squareform
from scipy import exp
from scipy.linalg import eigh

def rbf_kernel_pca(X, gamma, n_components):
    
    # Calculate pairwise squared Eucledian distances
    sq_dists = pdist(X, "sqeuclidean")

    # Convert pairwise distances into a square matrix
    mat_sq_dists = squareform(sq_dists)

    # Compute the symmetric kernel matrix (similarity matrix)
    K = exp(-gamma * mat_sq_dists)

    # Center the kernel matrix
    N = K.shape[0]
    one_n = np.ones((N,N)) / N
    
    K = K - one_n.dot(K) - K.dot(one_n) + one_n.dot(K).dot(one_n) # Linear algebra weirdness..

    # Obtain eigenpairs
    eigvals, eigvecs = eigh(K) # Note the "h" in "eigh" means scipy returns the pairs in ascending order
    eigvals, eigvecs = eigvals[::-1], eigvecs[:, ::-1] # Reverse the order into descending order

    # Collect the top k eigenvectors (projected examples)
    alphas = np.column_stack([eigvecs[:,i] for i in range(n_components)])

    # Collect the corresponding eigenvalues
    lambdas = [eigvals[i] for i in range(n_components)]

    return alphas, lambdas


from sklearn.datasets import make_moons

X, y= make_moons(n_samples=100, random_state=123)

alphas, lambdas = rbf_kernel_pca(X, gamma=15, n_components=2)
# Where alpha is the eigenvector of the similarity matrix Kappa, i.e. the projected principle components

#### To project other data into the new (in general lower dimensional (but not here) feature space),
# such as test data, we need to combine data from the training set (X) combined with eigenvectors/values
# In this example, the "new" datapoint is a copy of a value in X, so that we can test it works (i.e. test if x_proj==
# But the new datapoint could be in principle any point we like
x_new = X[25]
x_proj = alphas[25]

def project_x(x_new, X, gamma, alphas, lambdas):
    pair_dist = np.array([np.sum((x_new-row)**2) for row in X])
    k = np.exp(-gamma*pair_dist)
    return k.dot(alphas/lambdas)

x_reproj = project_x(x_new, X, gamma=15, alphas=alphas, lambdas=lambdas)

#from sklearn.linear_model import LogisticRegression
#lr = LogisticRegression(multi_class="ovr", random_state=1, solver="lbfgs")
#lr = lr.fit(X_train_lda, y_train)

import matplotlib.pyplot as plt


plt.scatter(alphas[y==0, 0], alphas[y==0, 1], color="red", marker="^", alpha=0.5)
plt.scatter(alphas[y==1, 0], alphas[y==1, 1], color="blue", marker="o", alpha=0.5)

plt.scatter(x_proj[0], x_proj[1], color="black", label="Original projection of point X[25]", marker="^", s=100)
plt.scatter(x_reproj[0], x_reproj[1], color="green", label="Remapped point X[25]", marker="x", s=500)

#from plot_decision_regions import *
#plot_decision_regions(X_train_lda, y_train, classifier=lr)

plt.xlabel("PC 1")
plt.ylabel("PC 2")

plt.tight_layout()
plt.show()
