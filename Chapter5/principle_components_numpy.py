import numpy as np
import pandas as pd

df_wine = pd.read_csv('https://archive.ics.uci.edu/'
                      'ml/machine-learning-databases/'
                      'wine/wine.data',header=None)
df_wine.columns = ['Class label', 'Alchohol', 'Malic acide', 'Ash', 'Alcalinity of ash', 'Magnesium', 'Total phenols',
                   'Flavanoids', 'Nonflavavonid phenols', 'Proanthocyanins', 'Color intensity', 'Hue',
                   'OD280/OD315 of diluted wines', 'Proline']


# Stratify=y means try to shuffle contents such that there are fractionally equal numbers of y=0,1,2 etc in the test and train sets
from sklearn.model_selection import train_test_split
X, y = df_wine.iloc[:,1:].values, df_wine.iloc[:,0].values
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=0, stratify=y)

# Standardize the features
from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
sc.fit(X_train)
X_train_std = sc.transform(X_train)
X_test_std = sc.transform(X_test)

# First we build the covariance matrix, where each element sigma_jk = 1/(n-1) Sum_i^n (x_j^i - mu_j) (x_k^i - mu_k)
# where mu_j is the mean value of x_j (where j is the feature or column index)
# Note that if data is standardized, mu_j and mu_k are zero
# i.e. sigma_jk is (I think) +1 if x_j and x_k are perfectly positively correlated
# sigma_jk is -1 if x_j and x_k are perfectly negatively correlated
# sigma_jk is 0 if x_j and x_k are completely uncorrelated

# I think .T is needed because linear algebra has different conventions to numpy
cov_mat = np.cov(X_train_std.T)

# Now use linear algebra voodoo to rotate the system along orthogonal eigen vectors that maximize the variance in a given (rotated) dimension
eigen_vals, eigen_vecs = np.linalg.eig(cov_mat)

print('\nEigenvalues \n%s' % eigen_vals)

# Now we choose which dimensions in the new rotated feature space we want to keep
# Sort eigen pairs by decreasing order (i.e. dimensions with max variance appear first)

# Make a list of (eigenvalue, eigenvector) tuples
eigen_pairs = [(np.abs(eigen_vals[i]), eigen_vecs[:,i]) for i in range(len(eigen_vals))]

# Sort the pairs from high to low
eigen_pairs.sort(key=lambda k: k[0], reverse=True) # Fancy native python here lol!

# Extract the two most important eigenvectors (max variance) and cast them in a form suitable for array multiplication
w = np.hstack((eigen_pairs[0][1][:, np.newaxis],
               eigen_pairs[1][1][:, np.newaxis]))

print("Matrix W:\n", w)

# Finally we compute the value of first two principle components
X_train_pca = X_train_std.dot(w)

import matplotlib.pyplot as plt

colors = ["r", "b", "g"]
markers = ["s", "x", "o"]

for l, c, m in zip(np.unique(y_train), colors, markers):
    plt.scatter(X_train_pca[y_train==l, 0],
                X_train_pca[y_train==l, 1],
                c=c, label=l, marker=m)


plt.xlabel("PC 1")
plt.ylabel("PC 2")

plt.legend(loc="lower left")
plt.tight_layout()
plt.show()
