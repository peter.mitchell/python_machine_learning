import numpy as np
import pandas as pd

df_wine = pd.read_csv('https://archive.ics.uci.edu/'
                      'ml/machine-learning-databases/'
                      'wine/wine.data',header=None)
df_wine.columns = ['Class label', 'Alchohol', 'Malic acide', 'Ash', 'Alcalinity of ash', 'Magnesium', 'Total phenols',
                   'Flavanoids', 'Nonflavavonid phenols', 'Proanthocyanins', 'Color intensity', 'Hue',
                   'OD280/OD315 of diluted wines', 'Proline']


# Stratify=y means try to shuffle contents such that there are fractionally equal numbers of y=0,1,2 etc in the test and train sets
from sklearn.model_selection import train_test_split
X, y = df_wine.iloc[:,1:].values, df_wine.iloc[:,0].values
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=0, stratify=y)

# Standardize the features
from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
sc.fit(X_train)
X_train_std = sc.transform(X_train)
X_test_std = sc.transform(X_test)

from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA

lda = LDA(n_components=2)
X_train_lda = lda.fit_transform(X_train_std, y_train)
X_test_lda = lda.transform(X_test_std)

from sklearn.linear_model import LogisticRegression

lr = LogisticRegression(multi_class="ovr", random_state=1, solver="lbfgs")
lr = lr.fit(X_train_lda, y_train)

import matplotlib.pyplot as plt
from plot_decision_regions import *

plot_decision_regions(X_train_lda, y_train, classifier=lr)

plt.xlabel("LD 1")
plt.ylabel("LD 2")

plt.legend(loc="upper left")
plt.tight_layout()
plt.show()
