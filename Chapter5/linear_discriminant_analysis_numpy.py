import numpy as np
import pandas as pd

df_wine = pd.read_csv('https://archive.ics.uci.edu/'
                      'ml/machine-learning-databases/'
                      'wine/wine.data',header=None)
df_wine.columns = ['Class label', 'Alchohol', 'Malic acide', 'Ash', 'Alcalinity of ash', 'Magnesium', 'Total phenols',
                   'Flavanoids', 'Nonflavavonid phenols', 'Proanthocyanins', 'Color intensity', 'Hue',
                   'OD280/OD315 of diluted wines', 'Proline']


# Stratify=y means try to shuffle contents such that there are fractionally equal numbers of y=0,1,2 etc in the test and train sets
from sklearn.model_selection import train_test_split
X, y = df_wine.iloc[:,1:].values, df_wine.iloc[:,0].values
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=0, stratify=y)

# Standardize the features
from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
sc.fit(X_train)
X_train_std = sc.transform(X_train)
X_test_std = sc.transform(X_test)

####### Build the two scatter matrices (between-class and within-class) ################

# First build the mean vectors
mean_vecs = []
for label in range(1,4): # 3 unique class labels 1,2,3
    mean_vec = np.mean( X_train_std[y_train==label], axis=0 ) # For elements belonging to a class, sum over rows for each column
    mean_vecs.append( mean_vec )

# Next compute the within-class scatter matrix (S_W)
d = 13 # Number of features
S_W = np.zeros((d,d))

for label, mv in zip(range(1,4), mean_vecs):
    # Covariance matrix for each class subset
    # +1 means perfect positive correlation, -1 means perfect anti-correlation (over a given feature pair)
    class_scatter = np.cov(X_train_std[y_train==label].T)
    
    # When we sum, we give a larger (absolute) score if all the classes are strongly correlated in the same way (either positive or negative)
    # and a lower variance score if all classes are poorly correlated with themselves, or ..
    # if some are positive and others are negative correlated

    # Remember that if there is no correlation between features, we assign all the variance to the diagonal elements
    # If there is a perfect correlation between features, we assign all the variance to the off-diagonal elements

    # What this means is a bit fuzzy to me, since we take the inverse of S_W in the next step...
    S_W += class_scatter

# A reminder: matrix inverse is defined such that A x A^-1 = I, where I is the identity matrix (ones on diagonal, zero elsewhere)
# The inverse is related to original by a multiplicative constant (the determinant), and for a 2x2 matrix the elements would be
# A = ( a b ) -> A^-1 = determinant x (d -b)
#     ( c d )                         (-c a)
# So diagonals were swapped, and off-diagonals flip in sign. Plus everything is rescaled by the determinant. 
# For 3x3 matrix and larger, the rule is that each element in the inverse is given by determinants for subsets of the matrix
# For diagonal elements in inverse, they depend on the determinant of the other diagonal elements in the original (plus associated off-diagonals)
# For off-diagonal elements in inverse, they depend on the determinant of themselves in the original (plus associated elements)
# In practice, I don't have great intuition for how this works out

# Now we compute the inter-class scatter matrix S_B
mean_overall = np.mean(X_train_std, axis=0)
d = 13 # No of features
S_B = np.zeros((d,d))

for i, mean_vec in enumerate(mean_vecs):
    n = X_train_std[y_train==i+1, :].shape[0] # Number of rows in class i
    mean_vec = mean_vec.reshape(d,1) # Make column vector
    mean_overall = mean_overall.reshape(d,1)
    
    S_B += n * (mean_vec-mean_overall).dot( (mean_vec-mean_overall).T)
    # So instead of measuring the standard deviation between two features, we are measuring the (scaled) offset in the means
    # If both features in the pair are strongly offset from the average (over all classes), give a large absolute score
    # If both features are bang on the average (we give a tiny score)

    # Note you might think well what happens if two classes are offset in the same direction (surely these can't be separated)
    # But if that happens then there must (by definition) be other classes that are strongly opposite direction for this feature pair
    # So its still going to be a good direction to split some classes from some of the others

    # S_B is easier to understand, since we don't take the inverse
    # Larger S_B means the two features are 


# I think the idea is that the best case for splitting the classes is between feature pairs that have
# a) miminal variance within each class (which we get somehow from S_W inverse)
# b) maximum variance between the classes (S_B)

# Now we find the eigen values/vectors of the total scatter matrix
eigen_vals, eigen_vecs = np.linalg.eig( np.linalg.inv(S_W).dot(S_B) )

# Make a list of (eigenvalue, eigenvector) tuples
eigen_pairs = [(np.abs(eigen_vals[i]), eigen_vecs[:,i]) for i in range(len(eigen_vals))]

# Sort the pairs from high to low
eigen_pairs.sort(key=lambda k: k[0], reverse=True) # Fancy native python here lol!

# Extract the two most important eigenvectors (max variance) and cast them in a form suitable for array multiplication
w = np.hstack((eigen_pairs[0][1][:, np.newaxis],
               eigen_pairs[1][1][:, np.newaxis]))

print("Matrix W:\n", w)

# Finally we compute the value of first two principle components
X_train_pca = X_train_std.dot(w)

import matplotlib.pyplot as plt

colors = ["r", "b", "g"]
markers = ["s", "x", "o"]

for l, c, m in zip(np.unique(y_train), colors, markers):
    plt.scatter(X_train_pca[y_train==l, 0],
                X_train_pca[y_train==l, 1],
                c=c, label=l, marker=m)


plt.xlabel("LD 1")
plt.ylabel("LD 2")

plt.legend(loc="upper left")
plt.tight_layout()
plt.show()
