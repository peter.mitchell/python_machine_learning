import numpy as np
import pandas as pd

df_wine = pd.read_csv('https://archive.ics.uci.edu/'
                      'ml/machine-learning-databases/'
                      'wine/wine.data',header=None)
df_wine.columns = ['Class label', 'Alchohol', 'Malic acide', 'Ash', 'Alcalinity of ash', 'Magnesium', 'Total phenols',
                   'Flavanoids', 'Nonflavavonid phenols', 'Proanthocyanins', 'Color intensity', 'Hue',
                   'OD280/OD315 of diluted wines', 'Proline']


# Stratify=y means try to shuffle contents such that there are fractionally equal numbers of y=0,1,2 etc in the test and train sets
from sklearn.model_selection import train_test_split
X, y = df_wine.iloc[:,1:].values, df_wine.iloc[:,0].values
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=0, stratify=y)

# Standardize the features
from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
sc.fit(X_train)
X_train_std = sc.transform(X_train)
X_test_std = sc.transform(X_test)

from sklearn.linear_model import LogisticRegression
from sklearn.decomposition import PCA

# arg is how many principle components to retain
pca = PCA(n_components=2)

lr = LogisticRegression(multi_class="ovr", random_state=1, solver="lbfgs")

X_train_pca = pca.fit_transform(X_train_std) # note we fit and transform in one step
X_test_pca = pca.transform(X_test_std)

lr.fit(X_train_pca, y_train)

import matplotlib.pyplot as plt
from plot_decision_regions import *

plot_decision_regions(X_train_pca, y_train, classifier=lr)

plt.xlabel("PC 1")
plt.ylabel("PC 2")

plt.legend(loc="lower left")
plt.tight_layout()
plt.show()
