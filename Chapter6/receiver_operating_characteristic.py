# aka, make a completeness versus purity plot
# This also provides "auc" area-under-curve, which is a useful performance metric for the chosen classifier (+1 good, 0 rubbish, -1 antichrist)

import pandas as pd
import numpy as np

# Read in the breast cancer dataset
df = pd.read_csv('https://archive.ics.uci.edu/ml/machine-learning-databases/breast-cancer-wisconsin/wdbc.data', header=None)

# Firt turn classes "M" (malignant) and "B" (benign) into integer values
from sklearn.preprocessing import LabelEncoder

X = df.loc[:,2:].values
y = df.loc[:,1].values

le = LabelEncoder()
y = le.fit_transform(y)

# Partition into train/test samples
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.2, stratify=y, random_state=1)

####### Create a pipeline to combine different data transform/fitting steps ##############
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import make_pipeline
from sklearn.decomposition import PCA

pipe_lr = make_pipeline(StandardScaler(),
                        PCA(n_components=2),
                        LogisticRegression(penalty="l2", random_state=1, solver="lbfgs", C=100.))

# To make a tougher challenge (so that we acutally get a significant number of false positive, false negatives)
X_train2 = X_train[:, [4, 14]]

from sklearn.model_selection import StratifiedKFold

cv = list(StratifiedKFold(n_splits=3).split(X_train, y_train))

import matplotlib.pyplot as plt

fig = plt.figure(figsize=(7,5))

mean_tpr = 0.0
mean_fpr = np.linspace(0, 1, 100)
all_tpr = []

from sklearn.metrics import roc_curve, auc
from scipy import interp

for i, (train, test) in enumerate(cv):
    # Recall that logistic regression returns "probabilities"
    # notice we are using the predict_proba method!
    probas = pipe_lr.fit(X_train2[train], y_train[train]).predict_proba(X_train2[test])

    # probas here tells us the probability than each example in the test set belongs to class 0 (first column in probas) or class 1

    # pos label - which class label value should be regarded as the positive value (note this is assuming binary class labels)
    fpr, tpr, thresholds = roc_curve(y_train[test], probas[:,1], pos_label=1)
    # fpr - false positive rate (1-purity)
    # tpr - true positive rate (completeness)

    # Note fpr and tpr are not one number here, they are computed as a function of the thresholds
    # In this case, fpr[i] gives the false positive rate if we were to use thresholds[i] as the probability threshold to class as positive

    ### Note - I have no idea (not explained in textbook or in official scikit documentation) how thresholds is computed
    # Why did we decide to use 56 thresholds here? No idea...

    # Compute average over k-folds (accouting for different threshold grids in each case - hence interpolation)
    mean_tpr += interp(mean_fpr, fpr, tpr)
    mean_tpr[0] = 0.0

    # auc stands for "area under curve", where a value of 1 indicates perfect model performance, 0 indicates random performance
    #, and -1 is that you perfectly predict everything incorrectly!
    # As such auc is the output of this exercise, representing how well the classifier performs
    roc_auc = auc(fpr, tpr)
    
    plt.plot(fpr, tpr, label="ROC fold %d (area= %0.2f)" % (i+1, roc_auc))

plt.plot([0, 1], [0,1], linestyle='--', color=(0.6, 0.6, 0.6), label="Random guessing")

mean_tpr /= len(cv)
mean_tpr[-1] = 1.0

mean_auc = auc(mean_fpr, mean_tpr)
plt.plot([0, 0, 1], [0,1,1], linestyle=':', color="black", label="Perfect performance")

plt.xlim([-0.05, 1.05])
plt.ylim([-0.05, 1.05])

plt.xlabel("False positive rate (1-purity)")
plt.ylabel("True positive rate (completeness)")
plt.legend(loc="lower right")
plt.show()
