import pandas as pd
import numpy as np

# Read in the breast cancer dataset
df = pd.read_csv('https://archive.ics.uci.edu/ml/machine-learning-databases/breast-cancer-wisconsin/wdbc.data', header=None)

# Firt turn classes "M" (malignant) and "B" (benign) into integer values
from sklearn.preprocessing import LabelEncoder

X = df.loc[:,2:].values
y = df.loc[:,1].values

le = LabelEncoder()
y = le.fit_transform(y)

# Partition into train/test samples
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.2, stratify=y, random_state=1)

####### Create a pipeline to combine different data transform/fitting steps ##############
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC
from sklearn.pipeline import make_pipeline

pipe_svc = make_pipeline(StandardScaler(),
                         SVC(random_state=1))

# Set up (hyper-)parameter space to brute force evaluate via grid search
# CV stands for cross-validation (by default via k-folds) - used to assess the accuracy
from sklearn.model_selection import GridSearchCV

# Try linear and Gassian kernel (RBF) support vector machines
param_range = [0.0001, 0.001, 0.01, 0.1, 1.0, 10., 100., 1000.]
param_grid = [{'svc__C': param_range, 'svc__kernel': ['linear']},
              {'svc__C': param_range, 'svc__gamma': param_range, 'svc__kernel': ['rbf']}]

# cv=2 is no of cross-validation k-folds
gs = GridSearchCV(estimator=pipe_svc, param_grid=param_grid, scoring="accuracy",
                  cv = 2)


### In this approach, we split training set into 5 folds (cv=5)
# Then we shuffle through, picking 4 folds, leaving the last as the "test fold"
# For each set of 4 folds, we divide them into 2 (cv=2), as "training" and "validation"
# For each set of 4 folds, we use these sub-folds to find the best hyper parameters
from sklearn.model_selection import cross_val_score

scores = cross_val_score(gs, X_train, y_train, scoring="accuracy", cv=5)

# By having 4 sets of hyper parameters, we can now estimate the accuracy and (importantly) its associated error
# i.e. we calculate the accuracy for each of the 4 hyper parameter sets, using the excluded "test fold"
# Then we just take the mean and standard

accuracy = np.mean(scores)
uncertainty = np.std(scores)

print('CV accuracy: %.3f +/- %.3f' % (np.mean(scores), np.std(scores)))
