import pandas as pd
import numpy as np

# Read in the breast cancer dataset
df = pd.read_csv('https://archive.ics.uci.edu/ml/machine-learning-databases/breast-cancer-wisconsin/wdbc.data', header=None)

# Firt turn classes "M" (malignant) and "B" (benign) into integer values
from sklearn.preprocessing import LabelEncoder

X = df.loc[:,2:].values
y = df.loc[:,1].values

le = LabelEncoder()
y = le.fit_transform(y)

# Partition into train/test samples
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.2, stratify=y, random_state=1)


####### Create a pipeline to combine different data transform/fitting steps ##############

from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import make_pipeline

pipe_lr = make_pipeline(StandardScaler(),
                        PCA(n_components=2),
                        LogisticRegression(random_state=1, solver='lbfgs'))

pipe_lr.fit(X_train, y_train)

y_pred = pipe_lr.predict(X_test)

######## Example of how to to k-fold cross-validation #####################
# (note this has no purpose currently because we aren't varying hyper parameters here)

from sklearn.model_selection import StratifiedKFold

# Split training sets into n_splits subsets
kfold = StratifiedKFold(n_splits=10).split(X_train, y_train)

scores = []
# For each fold "k", we have n_splits-9 train subsets, and 1 test subset 
# (note these are all subsets of the main training sample)
for k, (train, test) in enumerate(kfold):
    pipe_lr.fit(X_train[train], y_train[train])
    score = pipe_lr.score(X_train[test], y_train[test])
    scores.append(score)
    
    print('Fold: %2d, Class dist.: %s, Acc: %.3f' % (k+1, np.bincount(y_train[train]), score))

print('\nCV accuracy: %.3f +/- %.3f' % (np.mean(scores), np.std(scores)))

######### A more compact version of the above ####################
from sklearn.model_selection import cross_val_score

# cv is n_splits here, how many times we want to cross-validate the model
# n_jobs is a multi-processing arg
scores = cross_val_score(estimator=pipe_lr, X=X_train, y=y_train, cv=10, n_jobs=1)

print('')
print('Compact version')
print('CV accuracy scores: %s' % scores)
print('CV accuracy: %.3f +/- %.3f' % (np.mean(scores), np.std(scores)))
