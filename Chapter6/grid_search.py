import pandas as pd
import numpy as np

# Read in the breast cancer dataset
df = pd.read_csv('https://archive.ics.uci.edu/ml/machine-learning-databases/breast-cancer-wisconsin/wdbc.data', header=None)

# Firt turn classes "M" (malignant) and "B" (benign) into integer values
from sklearn.preprocessing import LabelEncoder

X = df.loc[:,2:].values
y = df.loc[:,1].values

le = LabelEncoder()
y = le.fit_transform(y)

# Partition into train/test samples
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.2, stratify=y, random_state=1)

####### Create a pipeline to combine different data transform/fitting steps ##############
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC
from sklearn.pipeline import make_pipeline

pipe_svc = make_pipeline(StandardScaler(),
                         SVC(random_state=1))

# Set up (hyper-)parameter space to brute force evaluate via grid search
# CV stands for cross-validation (by default via k-folds) - used to assess the accuracy
from sklearn.model_selection import GridSearchCV

# Try linear and Gassian kernel (RBF) support vector machines
param_range = [0.0001, 0.001, 0.01, 0.1, 1.0, 10., 100., 1000.]
param_grid = [{'svc__C': param_range, 'svc__kernel': ['linear']},
              {'svc__C': param_range, 'svc__gamma': param_range, 'svc__kernel': ['rbf']}]

# cv=10 is no of cross-validation k-folds
# refit tells gs to fit again to the entire training set (no valdiation sample) after determining the optimal parameters
gs = GridSearchCV(estimator=pipe_svc, param_grid=param_grid, scoring="accuracy",
                  cv = 10, refit=True, n_jobs=1)
gs = gs.fit(X_train, y_train)

print(gs.best_score_)
print(gs.best_params_)

clf = gs.best_estimator_
clf.fit(X_train, y_train) # This is unecessary due to "refit=True" argument, but is done for illustrative purposes

print('Test accuracy: %.3f' % clf.score(X_test, y_test))
