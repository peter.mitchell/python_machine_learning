import pandas as pd
import numpy as np

# Read in the breast cancer dataset
df = pd.read_csv('https://archive.ics.uci.edu/ml/machine-learning-databases/breast-cancer-wisconsin/wdbc.data', header=None)

# Firt turn classes "M" (malignant) and "B" (benign) into integer values
from sklearn.preprocessing import LabelEncoder

X = df.loc[:,2:].values
y = df.loc[:,1].values

le = LabelEncoder()
y = le.fit_transform(y)

# Partition into train/test samples
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.2, stratify=y, random_state=1)

####### Create a pipeline to combine different data transform/fitting steps ##############

####### Create a pipeline to combine different data transform/fitting steps ##############
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import make_pipeline

pipe_lr = make_pipeline(StandardScaler(),
                        LogisticRegression(penalty="l2",random_state=1, solver='lbfgs',max_iter=10000))

# Evaluate accuracy from training and validation sets using the learning_curve class
# This by default uses k-folds cross-validation, with the no of folds set by "cv"
from sklearn.model_selection import learning_curve

# train_sizes specifies the fractional subsets of the training data used
# Note I'm not sure how the validation data (k-folds) relates to this (or not)
train_sizes, train_scores, test_scores = learning_curve(estimator=pipe_lr, X=X_train, y=y_train, train_sizes=np.linspace(0.1, 1.0, 10),
                                                        cv=10, n_jobs=1)

train_mean = np.mean(train_scores,axis=1)
train_std = np.std(train_scores,axis=1)

test_mean = np.mean(test_scores,axis=1)
test_std = np.std(test_scores,axis=1)

import matplotlib.pyplot as plt

plt.plot(train_sizes, train_mean, color="blue", marker="o", markersize=5, label="Training accuracy")
plt.fill_between(train_sizes, train_mean + train_std, train_mean-train_std, alpha=0.15, color="blue")

plt.plot(train_sizes, test_mean, color="green", marker="s", markersize=5, label="Testing accuracy")
plt.fill_between(train_sizes, test_mean + test_std, test_mean-test_std, alpha=0.15, color="green")

plt.grid()
plt.xlabel("Number of training examples")
plt.ylabel("Accuracy")
plt.legend(loc="lower right")
plt.ylim((0.8, 1.03))
plt.show()
