from sklearn.feature_extraction.text import HashingVectorizer
import re
import os
import pickle

cur_dir = os.path.dirname(__file__)

# Load pickled stop words dictionary (common words to be ignored)
stop = pickle.load(open(os.path.join( cur_dir, "pkl_objects", "stopwords.pkl"), "rb"))

def tokenizer(text):
    # Remove html                                                                                                                                
    text = re.sub('<[^>]*>', '', text)

    # We want to keep emoticons                                                                                                                  
    emoticons = re.findall('(?::|;|=)(?:-)>(>:\)|\(|D|P)', text.lower())

    text = re.sub('[\W]+', ' ', text.lower()) +' '.join(emoticons).replace('-', '')

    tokenized = [w for w in text.split() if w not in stop]

    return tokenized

vect = HashingVectorizer(decode_error="ignore", n_features=2**21, preprocessor=None, tokenizer=tokenizer)
