from flask import Flask, render_template, request
from wtforms import Form, TextAreaField, validators
import pickle
import sqlite3
import os
import numpy as np

# import HashingVectorizer from local dir
from vectorizer import vect

# Tell flash it can find the templates folder in the current directory
app = Flask(__name__)

######## Preparing the Classifier #########

cur_dir = os.path.dirname(__file__)
clf = pickle.load(open(os.path.join(cur_dir, "pkl_objects", "classifier.pkl"), "rb"))

db = os.path.join(cur_dir, "reviews.sqlite")

def classify(document):
    label = {0: "negative", 1: "positive"}
    X = vect.transform([document])
    y = clf.predict(X)[0]
    
    proba = np.max(clf.predict_proba(X))
    return label[y], proba

# The idea here is that we update the classifier as we feed in new reviews typed into the webserver
def train(document, y):
    X = vect.transform([document])
    clf.partial_fit(X, [y])

# Save a new review into the sqlite database
def sqlite_entry(path, document, y):
    conn = sqlite3.connect(path)
    c = conn.cursor()
    c.execute("INSERT INTO review_db (review, sentiment, date)"\
                  "Values (?, ?, DATETIME('now'))", (document, y))
    conn.commit()
    conn.close()

########## Flask ########################

class ReviewForm(Form):
    moviereview = TextAreaField('', [validators.DataRequired(), validators.length(min=15)])

# Specify the URL that triggers the index function below (again a dummy in this case)
@app.route('/')

def index():
    form = ReviewForm(request.form)
    return render_template("reviewform.html", form=form)

@app.route('/results', methods=['POST'])
def results():
    form = ReviewForm(request.form)
    if request.method == "POST" and form.validate():
        review = request.form['moviereview']
        y, proba = classify(review)
        print("hmm", proba, round(proba*100, 2))
        return render_template('results.html',
                               content=review,
                               prediction=y,
                               probability=round(proba*100, 2))
    # If user screwed up, make them do it again
    return render_template('reviewform.html', form=form)

@app.route('/thanks', methods=['POST'])
def feedback():
    feedback = request.form["feedback_button"]
    review = request.form['review']
    prediction = request.form["prediction"]
    
    inv_label = {"negative": 0, "positive": 1}
    y = inv_label[prediction]
    if feedback == "Incorrect":
        y = int(not(y))

    train(review, y)
    sqlite_entry(db, review, y)
    
    return render_template('thanks.html')



if __name__ == "__main__":
    app.run(debug=True)
