from flask import Flask, render_template

# Tell flash it can find the templates folder in the current directory
app = Flask(__name__)
# Specify the URL that triggers the index function below (again a dummy in this case)
@app.route('/')

def index():
    # Render the dummy html template
    return render_template("first_app.html")

if __name__ == "__main__":
    app.run()
