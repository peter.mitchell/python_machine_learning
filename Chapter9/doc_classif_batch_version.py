# Note this is a verbatum copy of the same file from chapter 8, but now with the classifier/stopwords saved as pickle files at the end

import numpy as np
import re
from nltk.corpus import stopwords

stop = stopwords.words("english")

# This time we combine the preprocessing, no stop-words, and tokenizer steps into one function
def tokenizer(text):
    # Remove html
    text = re.sub('<[^>]*>', '', text)

    # We want to keep emoticons                                                                                                        
    emoticons = re.findall('(?::|;|=)(?:-)>(>:\)|\(|D|P)', text.lower())

    text = re.sub('[\W]+', ' ', text.lower()) +' '.join(emoticons).replace('-', '')

    tokenized = [w for w in text.split() if w not in stop]

    return tokenized

# Read in and return just one document at a time
# Note this is an iterator function (you can see this as it "yields" an output, and does not return anything)
# You call it using the next() function: next(stream_docs(file_path))
# Each time you call it in this way, it moves one line further into the file (note each line is a "document" in this case)
def stream_docs(path):
    with open(path, "r", encoding="utf-8") as csv:
        next(csv) # skip header
        for line in csv:
            text, label = line[:-3], int(line[-2])
            yield text, label

def get_minibatch(doc_stream, size):
    docs, y = [], []
    try:
        for _ in range(size):
            text, label = next(doc_stream)
            docs.append(text)
            y.append(label)
    except StopIteration:
        return None, None
    return docs, y


doc_stream = stream_docs(path="movie_data.csv")

# Example of how to step through the documents manually, and print sentiment score
#print(next(doc_stream)[1])
#print(next(doc_stream)[1])
#print(next(doc_stream)[1])

# Read in 5 reviews and separate into review text, and sentiment score lists
#docs, y = get_minibatch(doc_stream, 5)
#print(y)


from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.linear_model import SGDClassifier

# Build up the transforming function (transforms X_train into bag of words, counts) using hash indexing
#
# Why Hash indexing? The key part is that the feature space (i.e. vocab)
# is defined at the very start before we even look at any reviews at all (i.e. 2**21 words (about 2,000,000)).
# This means that we have the same vocab for each batch, without having to retain any information from previous batches
# (apart from coefficients in the SGD classifier (which in effect will be related to a cumulative counts)). 
# This is what allows one SGD instance to run on different batches.
# If we use CountVectorizer, the feature space (vocab) would be different for each batch, and SGD would not be able to continue
# from one batch to the next, as the coefficients from previous batch wouldn't map onto the new batch
#
# Note that we cannot however use inverse-document frequency weighting here, since we aren't preserving direct
# information about the number of times a given word appears between documents between batches
# (the SGD coefficients will contain some complicated jumble of the word counts, but won't be easy to disentangle)
#
# Note that the "tokenizer" we define earlier also does the desired preprocessing

vect = HashingVectorizer(decode_error = "ignore",
                         n_features = 2**21,
                         preprocessor=None,
                         tokenizer=tokenizer)

# Use Stochastic Gradient Descent classifier, since this works by stepping through training examples (rows) one by one
# And updates the fit as it goes
# This is needed for this "batch" approach being implemented here
# Note that when we compiled "movie_data.csv" in "compile_reviews.py", we randomized the order there
clf = SGDClassifier(loss="log", random_state=1)

import pyprind
pbar = pyprind.ProgBar(45)

classes = np.array([0,1])

for _ in range(45):
    X_train, y_train = get_minibatch(doc_stream, size=1000)
    
    if not X_train:
        break

    # Transform X_train into word count feature space
    # Note because of Hash Indexing (where Vocab was pre-defined), the vocab (feature columns) don't depend on what is in this batch
    X_train = vect.transform(X_train)

    # Update the fit parameters based on this batch
    clf.partial_fit(X_train, y_train, classes=classes)

    pbar.update()

# Use the remaining 5000 reviews as the test set
X_test, y_test = get_minibatch(doc_stream, size=5000)
X_test = vect.transform(X_test)


print("Accuracy %.3f" % clf.score(X_test, y_test))

# An extra bonus of SGD is that we can update our fit using the test dataset once we are doing estimating the accuracy
clf = clf.partial_fit(X_test, y_test)

# New for chapter 9, save as pickle files

print("Finished training the classifier, now saving pickle files to disk")

import pickle
import os

dest = os.path.join('movieclassifier', 'pkl_objects')

if not os.path.exists(dest):
    os.makedirs(dest)

pickle.dump(stop, open(os.path.join(dest, "stopwords.pkl"), "wb"), protocol=4)

pickle.dump(clf, open(os.path.join(dest, "classifier.pkl"), "wb"), protocol=4)
