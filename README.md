Worked examples (and some of my own experiementation) taken from the textbook "Python Machine Learning" (third edition): https://sebastianraschka.com/books/

This book works through the basics, covering (among other things) data preprocessing, dimensionality reduction (PCA, LDA), classification algorithms (logistic regresion, decision trees, SVM, kernel SVM), regularization,  model cross-validation and ensemble learning, linear regression  on non-labelled data, clustering analysis, artificial neural networks, convolutional neural nets, recurrent neural nets, generative adverserial neural nets, and a little bit of reinforcement learning for good measure.

Examples all use a mix of from-scratch algorithms, scikit-learn, or tensorflow+keras
