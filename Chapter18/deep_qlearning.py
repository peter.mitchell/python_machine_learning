# This is a standalone script to solve the cartpole problem using q-learning with a deep neural network
# Cartpole is where a pole tries to balance against falling over
# We use it as an example here because it is a continuous input space - meaning you can't record all
# possible action-value values in a big dictionary - as we did with the discrete gridworld example

# Note not fully tested - the things is damn slow!

import gym
import tensorflow as tf
import random
import matplotlib.pyplot as plt
from collections import namedtuple
from collections import deque
import numpy as np

Transition = namedtuple('Transition', ('state', 'action', 'reward', 'next_state', 'done'))

class DQNAgent:
    def __init__(self, env, 
                 learning_rate = 1e-3,
                 discount_factor = 0.95,
                 epsilon_greedy = 1.0,
                 epsilon_min = 0.01,
                 epsilon_decay = 0.995,
                 max_memory_size = 2000):
        self.enf = env # Environment object instance
        self.state_size = env.observation_space.shape[0]
        self.action_size = env.action_space.n

        # The approach here is that we cannot store all the action-value q vals in a dictionary
        # But we can save a finite number of them to short term "memory"
        # This is a python list-like object that be fixed to a max length - discard elements when new ones are added
        self.memory = deque(maxlen=max_memory_size)
        
        self.lr = learning_rate # How quickly trainable parameters are updated
        self.gamma = discount_factor # How much future success is traded off against current success
        # See page 683 - it is the gamma that appears in the value function

        self.epsilon = epsilon_greedy # How likely you are to ignore the policy and do something completely at random instead
        # This is just the initial value, it will decrease as we go - like simulated annealing temperature parameter

        self.epsilon_min = epsilon_min # Man value that epsilon can decrease to
        self.epsilon_decay = epsilon_decay # How quickly epsilon value decreases

        self._build_nn_model()

    def _build_nn_model(self, n_layers=3):
        # This NN takes in the input featurized states (i.e. states that have been parametrised in some way
        # I'm not totally sure but I think the action is somehow also specified as part of the inputs
        # And outputs the expected action-values (i.e. expectation value of reward given a state/action pair)
        self.model = tf.keras.Sequential()

        ## Hiden layers ##
        for n in range(n_layers):
            self.model.add(tf.keras.layers.Dense(units=32, activation='relu'))
            self.model.add(tf.keras.layers.Dense(units=32, activation='relu'))
            # Not sure why there is doubling up

        ## Last layer
        self.model.add(tf.keras.layers.Dense(units=self.action_size))

        ## Build & compile model
        self.model.build(input_shape=(None, self.state_size))
        self.model.compile(
            loss='mse', 
            optimizer=tf.keras.optimizers.Adam(lr=self.lr)
            )

    # Add an interaction to memory (recall transition is the five value tuple - state,action,reward,next_state,done - I think)
    def remember(self, transition):
        self.memory.append(transition)

    def choose_action(self, state):

        # Do a fully random movement if greedy choice threshold fails
        if np.random.rand() <= self.epsilon:
            return random.randrange(self.action_size)

        # Otherwise deterministically follow the NN recommendation (greedy choice)
        q_values = self.model.predict(state)[0]

        return np.argmax(q_values)
           
    # For this example, we learn from a random subset of the stored transitions (past evalutions) that are in "memory"
    # This is done I think because a NN wants there to be an independent (non-ordered) set of examples to learn from
    # Which doesn't match the structure of the problem - so we mimic the desired structure by storing ~ 1000 past
    # transitions in memory - and iterate over those. As we do so the memory cache is slowly changing (slow enough to be stable I guess)
    # as new transitions are added, and the eldest are forgotten
    def _learn(self, batch_samples):
        batch_states, batch_targets = [], []

        for transition in batch_samples:
            s, a, r, next_s, done = transition

            if done:
                target = r
            else:
                # Off policy learning - target is set by best action - irrespective of what the agent actually decides to do
                target = (r + self.gamma * np.amax(self.model.predict(next_s)[0]))

            target_all = self.model.predict(s)[0]
            target_all[a] = target
            # page 712 and associated diagram for explanation of these two lines
            # the ideas is that instead of creating a scalar target value, we instead create a vector target (vector across actions I think)
            # but set all the non-chosen actions in such a way that the loss is zero
            # and so the NN only updates weight parameters relating to the chosen action - as I understand it
            
            batch_states.append(s.flatten())
            batch_targets.append(target_all)
            
            self._adjust_epsilon

        return self.model.fit(x = np.array(batch_states), y=np.array(batch_targets), epochs=1, verbose=0)

    def _adjust_epsilon(self):
        if self.epsilon > self.epsilon_min:
            self.epsilon *= self.epsilon_decay
    
    def replay(self, batch_size):
        samples = random.sample(self.memory, batch_size)
        history = self._learn(samples)
        return history.history['loss'][0]

def plot_learning_history(history):
    fig = plt.figure(1, figsize=(14,5))
    ax = fig.add_subplot(1, 1, 1)
    episodes = np.arange(len(history[0]))+1
    plt.plot(episodes, history[0], lw=4, marker='o', markersizer=10)

    ax.tick_params(axis='both', which='major', labelsize=15)
    plt.xlabel('Episodes', size=20)
    plt.ylabel('# Total Rewards', size=20)
    plt.show()

## General settings ##
EPISODES = 200
batch_size = 32
init_replay_memory_size = 500

if __name__ == '__main__':
    env = gym.make('CartPole-v1')
    agent = DQNAgent(env)
    state = env.reset()
    state = np.reshape(state, [1, agent.state_size])

    ## Filling up the replay memory (enough that the NN can do some meaningful learning when we get started properly
    for i in range(init_replay_memory_size):
        action = agent.choose_action(state)
        next_state, reward, done, _  = env.step(action)
        next_state = np.reshape(next_state, [1, agent.state_size])
        agent.remember(Transition(state, action, reward, next_state, done))

        if done:
            state = env.reset()
            state = np.reshape(state, [1, agent.state_size])
        else:
            state = next_state

    total_rewards, losses = [], []

    for e in range(EPISODES):
        state = env.reset()
        if e % 10 == 0:
            env.render()

        state = np.reshape(state, [1, agent.state_size])
        for i in range(500):
            action = agent.choose_action(state)
            next_state, reward, done, _ = env.step(action)
            next_state = np.reshape(next_state, [1, agent.state_size])

            agent.remember(Transition(state, action, reward, next_state, done))

            state = next_state
            if e % 10 == 0:
                env.render()
            if done:
                total_rewards.append(i)
                print('Episode: %d/%d, Total reward: %d' % (e, EPISODES, i))
                break

            # This is where the training occurs
            loss = agent.replay(batch_size)
            losses.append(loss)
    
    plot_learning_history(total_rewards)
