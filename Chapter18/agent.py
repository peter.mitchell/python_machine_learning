# This implements a RL solution for solving the gridworld problem using "Q-learning"

from collections import defaultdict
import numpy as np

class Agent(object):
    def __init__(self, env, 
                 learning_rate = 0.01,
                 discount_factor = 0.9,
                 epsilon_greedy = 0.9,
                 epsilon_min = 0.1,
                 epsilon_decay = 0.95):
        self.env = env # Environment object instance
        self.lr = learning_rate # How quickly trainable parameters are updated
        self.gamma = discount_factor # How much future success is traded off against current success
        # See page 683 - it is the gamma that appears in the value function

        self.epsilon = epsilon_greedy # How likely you are to ignore the policy and do something completely at random instead
        # This is just the initial value, it will decrease as we go - like simulated annealing temperature parameter

        self.epsilon_min = epsilon_min # Man value that epsilon can decrease to
        self.epsilon_decay = epsilon_decay # How quickly epsilon value decreases

        ## Define the q_table ##
        # Q is the action-value function (page 683)
        # It gives the expected return, given the current state/action
        #
        # nA is the number of possible actions (4 - up/down/left/right)
        # default dict is a python dict that returns a default value if you index with a key that is not in the dict
        self.q_table = defaultdict(lambda: np.zeros(self.env.nA))

    def choose_action(self, state):

        # Do a fully random movement if greedy choice threshold fails
        if np.random.uniform() < self.epsilon:
            action = np.random.choice(self.env.nA)
            
        # Otherwise enact the current "greedy" policy given the state (grid position)
        # The "greedy" part is that this is deterministic (always do the best possible move)
        # unless there is a tie between two (or more) best-possible options
        # in which case we choose a random option from the multiple best-possible options
        else:
            q_vals = self.q_table[state]
            perm_actions = np.random.permutation(self.env.nA) # e.g. [0,2,1,3], or [3,0,1,2]
            # The random purmutation is done so that if there is a tie for the best choice (highest q of the 4)
            # This way we randomly choose from the best choices
            q_vals = [q_vals[a] for a in perm_actions] 
            perm_q_argmax = np.argmax(q_vals)
            action = perm_actions[perm_q_argmax]

        return action

    def _learn(self, transition):
        s, a, r, next_s, done = transition
        q_val = self.q_table[s][a]
        if done:
            q_target = r
        else:
            # This is the "off-policy" Q-learning part. You look at all the possible actions one could take from the next state
            # And choose the best action (irrespective of what the agent will do in practice) to update the Q value for this state
            # Recall q is the expected return given the current state and action
            q_target = r+self.gamma*np.max(self.q_table[next_s])

            # (Note that I think r (= R_{t+1} on 694) is always zero unless we are about to hit one of the termination grid tiles), in which
            # case done would be True I think? (EDIT: confirmed yes r=0 unless you hit one of the final tiles)
            # If this is true then is gamma is meaningless ?
            # Probably not, but I think it may be degenerate with the learning rate "alpha" (or self.lr) in this example


        ## Update the q_table
        self.q_table[s][a] += self.lr * (q_target - q_val) # Page 694 - this follows from the Bellman Eqn - page 685
        # So what happens here is that if the next state is more or less favourable (than the current state+action combo),
        # we update this state/action to reflect that
        # So in practice the agent learns backwards - first it finds the states that win/lose, then as we go into
        # further episodes (iterations) it will back-propagate the win/loss information to the earlier moves that preceed them

        ## Adjust the epsilon
        self._adjust_epsilon()

    def _adjust_epsilon(self):
        if self.epsilon > self.epsilon_min:
            self.epsilon *= self.epsilon_decay
        
