# DCGAN - Deep Convolutional Generative Adversarial Network
# note seems to crash on gp_tape line - might be a typo, but might also be a deeper problem (outdated code?)

import tensorflow as tf
import tensorflow_datasets as tfds
import numpy as np

## define a function for the generator
def make_dcgan_generator(z_size=20,
                         output_size=(28,28,1),
                         n_filters=128,
                         n_blocks=2):

    # I think n_blocks is the number of transpose convolutions that are applied in the network
    # Each time one is applied, the size of each image doubles
    # "size factor" is then the total size shift - which we use to set the input size correctly such that it matches
    # the desired output image size
    size_factor = 2**n_blocks

    # "//" is a python 3 operation. In python 3 "/" division always returns float by default
    # "//" was then added to added to give old python 2 functionality i.e. return integer if two integers are fed in
    # i.e. 2/3 in python yields 0.666.., 2//3 yields 0, 2.//3. yields 0.666... 
    hidden_size = (output_size[0] // size_factor, output_size[1] // size_factor)

    ###### First steps are to build the input 
    model = tf.keras.Sequential([
            # One way to specify ahead of time the expected input dims
            tf.keras.layers.Input(shape=(z_size,)),
            
            # Transform the input z distn into a big 1d output array
            tf.keras.layers.Dense(units=n_filters * np.prod(hidden_size), use_bias=False),
            
            # Batch normalization normalizes all the images in each batch 
            # Note that it comes with 2 trainable parameter vectors that rescale and shift each batch after being normed 
            # There is one of these vectors per convolution channel (which here is taken as the final axis of the input array,
            # which is actually in this case then the big 1d array, since it has been reshaped into (batch x image_x x image_y x channel) yet
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.LeakyReLU(),
            tf.keras.layers.Reshape(
                (hidden_size[0], hidden_size[1], n_filters)),

            # Transpose convolution operations are later going to be used to increase the image size (and are
            # therefore I believe related to the n_blocks argument mentioned above
            # but this one just does a convolution that preserves the input shape
            #
            # Note: I am a bit confused - why this happens, as you would expect filter to produce a different image from all the input images
            # I guess there is some implicit aggregation happening to preserve the shape
            #
            # I also don't know why this layer is a Transpose layer?? (think it doesn't matter if strides are 1 ?)
            #
            # use_bias is set to False since it is effectively replaced by the BatchNormalization() call before activation
            tf.keras.layers.Conv2DTranspose(
                filters=n_filters, kernel_size=(5,5),
                strides=(1,1), padding="same", use_bias=False),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.LeakyReLU()
            ])

    nf = n_filters
    for i in range(n_blocks):
        # Decrease filter numbers as we go - (since image size is increasing each step also)
        nf = nf // 2
        # In this case stries=2 is going to increase the size of the image (this is Transpose part of the Convolution operatioN)
        model.add(
            tf.keras.layers.Conv2DTranspose(
                filters = nf, kernel_size=(5,5),
                strides=(2,2), padding="same",
                use_bias=False))
        model.add(tf.keras.layers.BatchNormalization())
        model.add(tf.keras.layers.LeakyReLU())
    
    # Produce the final output image
    model.add(
        tf.keras.layers.Conv2DTranspose(
            filters=output_size[2], kernel_size=(5,5),
            strides=(1,1), padding='same', use_bias=False,
            activation='tanh'))

    return model
            
## define a function for the discrimator
def make_dcgan_discriminator(
    input_size = (28,28,1),
    n_filters = 64,
    n_blocks=2):
    
    model = tf.keras.Sequential([
            tf.keras.layers.Input(shape=input_size),
            tf.keras.layers.Conv2D(
                filters=n_filters, kernel_size=5,
                strides=(1,1), padding='same'),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.LeakyReLU()
            ])

    nf = n_filters
    for i in range(n_blocks):
        # Increase filter numbers as we go - (since in this case image size decreases at each step)
        nf = nf * 2
        model.add(
            # Note standard Conv2D layer here - so strides > 1 decreases image size
            tf.keras.layers.Conv2D(
                filters = nf, kernel_size=(5,5),
                strides=(2,2), padding="same",
                use_bias=False))
        model.add(tf.keras.layers.BatchNormalization())
        model.add(tf.keras.layers.LeakyReLU())
        model.add(tf.keras.layers.Dropout(0.3))

    model.add(
        tf.keras.layers.Conv2D(
            filters=1, kernel_size=(7,7),
            padding="valid"))
    model.add(tf.keras.layers.Reshape((1,)))

            
    return model

mnist_bldr = tfds.builder('mnist')
mnist_bldr.download_and_prepare()
mnist = mnist_bldr.as_dataset(shuffle_files=False)

# This has to be tweaked from gan_mnist.py to keep the (greyscale so redundant but hey) channel dimension
def preprocess(ex, mode="uniform"):
    image = ex['image']
    image = tf.image.convert_image_dtype(image, tf.float32)

    #image = tf.reshape(image, [-1]) # Grey-scale mnist doesn't have the required colour dimension
    image = image*2 - 1.0
    # For each real image from the dataset, we also add a fake input (not an image but rather the input for the generator)
    if mode=='uniform':
        input_z = tf.random.uniform(
            shape=(z_size,), minval=-1.0, maxval=1.0)
    elif mode=="normal":
        input_z = tf.random.normal(shape=(z_size,))
    return input_z, image


num_epochs = 100
batch_size=128
image_size=(28, 28)
z_size = 20
mode_z = 'uniform'
lambda_gp = 10.0 # Gradient penalty parameter that will be used for the loss function

tf.random.set_seed(1)
np.random.seed(1)

## Setup the dataset
mnist_trainset = mnist['train']
mnist_trainset = mnist_trainset.map(preprocess)

mnist_trainset = mnist_trainset.shuffle(10000)
mnist_trainset = mnist_trainset.batch(batch_size, drop_remainder=True)

## Setup the model
gen_model = make_dcgan_generator()
gen_model.build(input_shape=(None, z_size))

disc_model = make_dcgan_discriminator()
disc_model.build(input_shape=(None, np.prod(image_size)))




import time

g_optimizer = tf.keras.optimizers.Adam()
d_optimizer = tf.keras.optimizers.Adam()

if mode_z == "uniform":
    fixed_z = tf.random.uniform(
        shape=(batch_size, z_size),
        minval=-1, maxval=1)
elif mode_z == "normal":
    fixed_z = tf.random.normal(
        shape=(batch_size, z_size))
else:
    print("noope")
    quit()

def create_samples(g_model, input_z):
    g_output = g_model(input_z, training=False)
    images = tf.reshape(g_output, (batch_size, *image_size))
    return (images+1)/2.0

all_losses = []
epoch_sampels = []

start_time = time.time()


for epoch in range(1, num_epochs+1):
    
    epoch_losses = []

    for i,(input_z, input_real) in enumerate(mnist_trainset):
        
        ## Note the gradient tape structure is more complex this time - I have the feeling things are not being fully explained!
        with tf.GradientTape() as d_tape, tf.GradientTape() as g_tape:
            g_output = gen_model(input_z, training=True)

            d_critics_real = disc_model(g_output, training=True)

            d_critics_fake = disc_model(g_output, training=True)

            ## Compute generator's loss
            g_loss = -tf.math.reduce_mean(d_critics_fake)
            
            ## Compute discriminator's loss
            d_loss_real = -tf.math.reduce_mean(d_critics_real)
            d_loss_fake = tf.math.reduce_mean(d_critics_fake)
            d_loss = d_loss_real + d_loss_fake

            ## Gradient-penalty
            # See 662 for an explanation
            with tf.GradientTape as gp_tape:
                alpha = tf.random.uniform(
                    shape = [d_critics_real.shape[0], 1, 1, 1],
                    minval = 0.0, maxval=1.0)
                interpolated = (alpha*input_real + (1-alpha)*g_output)
                gp_tape.watch(interpolated)
                d_critics_intp = disc_model(interpolated)

            grads_intp = gp_tape.gradient(d_critics_intp, [interpolated,])[0]
            grads_intp_l2 = tf.sqrt( tf.reduce_sum( tf.square( grads_intp), axis=[1,2,3]))

            grad_penalty = tf.reduce_mean(tf.square(grads_intp_l2 - 1.0))

            d_loss = d_loss + lambda_gp * grad_penalty

        ## Optimization: compute the gradients and apply them

        d_grads = d_tape.gradient(d_loss, disc_model.trainable_variables)
        
        d_optimizer.apply_gradients(
            grads_and_vars=zip(d_grads, disc_model.trainable_variables))

        ## Compute the gradients of g_loss
        g_grads = g_tape.gradient(g_loss, gen_model.trainable_variables)
            
        ## Optimization: apply the gradients
        g_optimizer.apply_gradients(
            grads_and_vars=zip(g_grads, gen_model.trainable_variables))

        ## book-keeping
        epoch_losses.append(
            (g_loss.numpy(), d_loss.numpy(),
             d_loss_real.numpy(), d_loss_fake.numpy()))

        all_losses.append(epoch_losses)
        print('Epoch {:03d} | ET {:.2f} min | Avg Losses >>'
              'G/D {:6.2f}/{:6.2f} [D-Real: {:6.2f} D-Fake: {:6.2f}]'
              .format(
                epoch, (time.time()-start_time)/60,
                *list(np.mean(all_losses[-1], axis=0))))

        # Create a mock image from the generator at this epoch (for display purposes)
        epoch_samples.append(create_samples(gen_model, fixed_z).numpy())


# Page 666
### There is then code to plot the anecdotal fake image examples across epochs
