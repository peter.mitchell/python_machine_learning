##### Note its with this example that I finally understand what they mean by epoch, and a bit more about batches
# 1 epoch in this example simple means going through every example (split into batches) exactly once
# I think (needs to be verified) that I finally understand what the batches are doing also
# The weights are being updated once per batch (not once per example within the batch) - so this is a tradeoff I suppose
# If batch size is too large - we will need many epochs to achieve convergence
# If batch size is too small - individual weight updates won't be very meaningful (one image probability does not help a lot)

import tensorflow as tf
import tensorflow_datasets as tfds
import numpy as np

## define a function for the generator
def make_generator_network(num_hidden_layers=1,
                           num_hidden_units=100,
                           num_output_units=784):
    
    model = tf.keras.Sequential()
    for i in range(num_hidden_layers):
        model.add(
            tf.keras.layers.Dense(
                units=num_hidden_units, use_bias=False))
        model.add(tf.keras.layers.LeakyReLU())
        # I don't know why the LeakyReLU is added as a separate layer??
        # But I imagine this is equivalent to adding LeakyReLU as the activation to the Dense layer
        # i.e. there are no extra trainable parameters associated with ReLU layer

    model.add(
        tf.keras.layers.Dense(
            units=num_output_units, activation="tanh"))
    
    return model
            
## define a function for the discrimator
def make_discriminator_network(
    num_hidden_layers=1,
    num_hidden_units=100,
    num_output_units=1):
    
    model = tf.keras.Sequential()
    for i in range(num_hidden_layers):
        model.add(
            tf.keras.layers.Dense(units=num_hidden_units))
        model.add(tf.keras.layers.LeakyReLU())
        model.add(tf.keras.layers.Dropout(rate=0.5))

    model.add(
        tf.keras.layers.Dense(
            units=num_output_units, activation=None))
    return model



do_example = True
if do_example:

    image_size = (28, 28)
    z_size = 20
    mode_z = 'uniform' # Choose the z distribution that is the input for the Image Generator
    gen_hidden_layers = 1
    gen_hidden_size = 100
    disc_hidden_layers = 1
    disc_hidden_size = 100

    tf.random.set_seed(1)

    gen_model = make_generator_network(
        num_hidden_layers=gen_hidden_layers,
        num_hidden_units=gen_hidden_size,
        num_output_units=np.prod(image_size))

    gen_model.build(input_shape=(None, z_size))
    #gen_model.summary()

    disc_model = make_discriminator_network(
        num_hidden_layers=disc_hidden_layers,
        num_hidden_units=disc_hidden_size)

    disc_model.build(input_shape=(None, np.prod(image_size)))



#### Build the training set (of real and fake images) ####

mnist_bldr = tfds.builder('mnist')
mnist_bldr.download_and_prepare()
mnist = mnist_bldr.as_dataset(shuffle_files=False)

def preprocess(ex, mode="uniform"):
    image = ex['image']
    image = tf.image.convert_image_dtype(image, tf.float32)
    image = tf.reshape(image, [-1]) # Grey-scale mnist doesn't have the required colour dimension
    image = image*2 - 1.0
    # For each real image from the dataset, we also add a fake input (not an image but rather the input for the generator)
    if mode=='uniform':
        input_z = tf.random.uniform(
            shape=(z_size,), minval=-1.0, maxval=1.0)
    elif mode=="normal":
        input_z = tf.random.normal(shape=(z_size,))
    return input_z, image

###### Now let's get a feeling for this data goes is passed through the networks #######
if do_example:
    mnist_trainset = mnist['train']
    mnist_trainset = mnist_trainset.map(preprocess)

    mnist_trainset = mnist_trainset.batch(32, drop_remainder=True)


    input_z, input_real = next(iter(mnist_trainset))

    print('input-z -- shape:   ', input_z.shape) # (32,20) - 32 is batch size, 20 is the number of elements used to represent z distn.
    print('input-real -- shape:', input_real.shape) # (32, 784) - 784 is number of pixels in image

    g_output = gen_model(input_z)
    print('Output of G -- shape:', g_output.shape) # (32, 784) # i.e. generator creates a fake image

    d_logits_real = disc_model(input_real)
    d_logits_fake = disc_model(g_output)

    print('Disc. (real) -- shape:', d_logits_real.shape) # (32, 1) probabilitiy that each real image in the batch is real
    print('Disc. (fake) -- shape:', d_logits_fake.shape) # (32, 1) probabilitiy that each fake image in the batch is real


    ####### Model training ##########

    loss_fn = tf.keras.losses.BinaryCrossentropy(from_logits=True)
    
    ## 1) Loss for the generator   
    g_labels_real = tf.ones_like(d_logits_fake)
    g_loss = loss_fn(y_true=g_labels_real, y_pred=d_logits_fake)
    
    # For the generator you want to get as close as possible to the discriminator (d_logits_fake) to think the fake
    # images are real
    print("Generator Loss: {:.4f}".format(g_loss))
    
    ## 2) Loss for the discriminant (two terms this time)
    d_labels_real = tf.ones_like(d_logits_real)
    d_labels_fake = tf.zeros_like(d_logits_fake)

    d_loss_real = loss_fn(y_true=d_labels_real, y_pred = d_logits_real)
    d_loss_fake = loss_fn(y_true=d_labels_fake, y_pred = d_logits_fake)

    print('Discriminator losses: Real {:.4f} Fake {:.4f}'.format(d_loss_real.numpy(), d_loss_fake.numpy()))

####### Real thing version ############
import time
num_epochs = 100
batch_size=64
image_size=(28, 28)
z_size = 20
mode_z = 'uniform'
gen_hidden_layers = 1
gen_hidden_size = 100
disc_hidden_layers=1
disc_hidden_size=100

tf.random.set_seed(1)
np.random.seed(1)

if mode_z == "uniform":
    fixed_z = tf.random.uniform(
        shape=(batch_size, z_size),
        minval=-1, maxval=1)
elif mode_z == "normal":
    fixed_z = tf.random.normal(
        shape=(batch_size, z_size))
else:
    print("noope")
    quit()

def create_samples(g_model, input_z):
    g_output = g_model(input_z, training=False)
    images = tf.reshape(g_output, (batch_size, *image_size))
    return (images+1)/2.0

## Setup the dataset
mnist_trainset = mnist['train']
mnist_trainset = mnist_trainset.map(
    lambda ex: preprocess(ex, mode=mode_z))

mnist_trainset = mnist_trainset.shuffle(10000)
mnist_trainset = mnist_trainset.batch(batch_size, drop_remainder=True)

## Setup the model
#with tf.device(device_name): # No idea what this line is for - or where device_name comes from!
# Edit - this is to tell which device to use (GPU/CPU if we were running this on google cloud services (as recommended by book)
# I'm not doing this so nvm
gen_model = make_generator_network(
    num_hidden_layers=gen_hidden_layers,
    num_hidden_units=gen_hidden_size,
    num_output_units=np.prod(image_size))

gen_model.build(input_shape=(None, z_size))

disc_model = make_discriminator_network(
    num_hidden_layers=disc_hidden_layers,
    num_hidden_units=disc_hidden_size)

disc_model.build(input_shape=(None, np.prod(image_size)))

## Loss function and optimizers
loss_fn = tf.keras.losses.BinaryCrossentropy(from_logits=True)

g_optimizer = tf.keras.optimizers.Adam()
d_optimizer = tf.keras.optimizers.Adam()


all_losses = []
all_d_vals = []
epoch_samples = []

start_time = time.time()

for epoch in range(1, num_epochs+1):
    
    epoch_losses, epoch_d_vals = [], []

    for i,(input_z, input_real) in enumerate(mnist_trainset):
        
        ## Compute generator's loss
        with tf.GradientTape() as g_tape:
            g_output = gen_model(input_z)
            d_logits_fake = disc_model(g_output, training=True)
            
            labels_real = tf.ones_like(d_logits_fake)
            g_loss = loss_fn(y_true=labels_real, y_pred = d_logits_fake)

        ## Compute the gradients of g_loss
        g_grads = g_tape.gradient(g_loss, gen_model.trainable_variables)
            
        ## Optimization: apply the gradients
        g_optimizer.apply_gradients(
            grads_and_vars=zip(g_grads, gen_model.trainable_variables))

        ## Compute discriminator's loss
        with tf.GradientTape() as d_tape:
            d_logits_real = disc_model(input_real, training=True)
            d_labels_real = np.ones_like(d_logits_real)
            d_loss_real = loss_fn(y_true=d_labels_real, y_pred=d_logits_real)
            
            d_logits_fake = disc_model(g_output, training=True)
            d_labels_fake = np.zeros_like(d_logits_fake)
            d_loss_fake = loss_fn(y_true=d_labels_fake, y_pred=d_logits_fake)
            
            # This is I think one of the reasons we are working with logits (log probabilities) - can do linear addition
            d_loss = d_loss_real + d_loss_fake
            
        ## Compute the gradients of d_loss
        d_grads = d_tape.gradient(d_loss, disc_model.trainable_variables)

        ## Opimization: apply the gradients
        d_optimizer.apply_gradients(grads_and_vars=zip(d_grads, disc_model.trainable_variables))

        ## book-keeping
        epoch_losses.append(
            (g_loss.numpy(), d_loss.numpy(),
             d_loss_real.numpy(), d_loss_fake.numpy()))

        # Convert logits into probability - note sure why reduce_mean is needed? (there is one logit value per image in a batch)
        # Edit - I think it's averaging over the examples in the batch
        d_probs_real = tf.reduce_mean(tf.sigmoid(d_logits_real))
        
        d_probs_fake = tf.reduce_mean(tf.sigmoid(d_logits_fake))

        epoch_d_vals.append((d_probs_real.numpy(), d_probs_fake.numpy()))

        all_losses.append(epoch_losses)
        all_d_vals.append(epoch_d_vals)
        
        print('Epoch {:03d} | ET {:.2f} min | Avg Losses >>'
              'G/D {:.4f}/{:.4f} [D-Real: {:.4f} D-Fake: {:.4f}]'
              .format(
                epoch, (time.time()-start_time)/60,
                *list(np.mean(all_losses[-1], axis=0))))

        # Create a mock image from the generator at this epoch (for display purposes)
        epoch_samples.append(create_samples(gen_model, fixed_z).numpy())


# Page 644
### There is then code to plot the losses across iterations (batches), as well as the discriminant probability (for both fake/real images)

### And then the recorded fake images are shown as f(epoch)
