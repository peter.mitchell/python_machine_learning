# Edit. This example did not work for me. Adaboost complains about an argument "SAMME.R" that textbook does not mention
# Changing like it recommends to "SAMME" seems to (weirdly) result in class instance just being a string... go figure..

# Adaboost - adaptive boosting (Ensemble technique, using iterations over weak classifiers, focussing on mis-classifications)
# "Adaptive" refers to assigning the training examples weights that are adjusted adaptively according to
# the performance of each iteration of the weak classifier

import pandas as pd

df_wine = pd.read_csv('https://archive.ics.uci.edu/'
                      'ml/machine-learning-databases/'
                      'wine/wine.data',header=None)
df_wine.columns = ['Class label', 'Alchohol', 'Malic acide', 'Ash', 'Alcalinity of ash', 'Magnesium', 'Total phenols',
                   'Flavanoids', 'Nonflavavonid phenols', 'Proanthocyanins', 'Color intensity', 'Hue',
                   'OD280/OD315 of diluted wines', 'Proline']


# drop 1 class
df_wine = df_wine[df_wine["Class label"] != 1]

y = df_wine["Class label"].values
X = df_wine[["Alchohol", "OD280/OD315 of diluted wines"]].values

from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split

le = LabelEncoder()
y = le.fit_transform(y)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1, stratify=y)


from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier

# Use pruned, depth=1 decision tree as a "weak" learner (very high bias typically)
tree = DecisionTreeClassifier(criterion="entropy", random_state=1, max_depth=1)

# n_estimators is the number of iterations (or individual decision trees that are used)
# learning_rate is not explained in the book, but I would guess controls the alpha coefficient (page 253)
# (i.e. how you much you punish mis-classification)
ada = AdaBoostClassifier(base_estimator="tree", n_estimators=500, learning_rate=0.1, random_state=1)

# First compute performance of depth=1 tree on its own (for reference)
tree = tree.fit(X_train, y_train)
y_train_pred = tree.predict(X_train)
y_test_pred = tree.predict(X_test)

from sklearn.metrics import accuracy_score
tree_train = accuracy_score(y_train, y_train_pred)
tree_test = accuracy_score(y_test, y_test_pred)

print('Decision tree train/test accuracies %.3f/%.3f' % (tree_train, tree_test))


# Now use adaboost
ada = ada.fit(X_train, y_train)

y_train_pred = ada_predict(X_train)
y_test_pred = ada_predict(X_test)

ada_train = accuracy_score(y_train, y_train_pred)
ada_test = accuracy_score(y_test, y_test_pred)
print('AdaBoost train/test accuracies %.3f/%.3f' % (tree_train, tree_test))
