# Tune the hyper parameters of the majority vote classifier

from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import LabelEncoder

iris = datasets.load_iris()

X,y = iris.data[50:, [1,2]], iris.target[50:]

le = LabelEncoder()
y = le.fit_transform(y)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.5, random_state=1, stratify=y)

from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier

from sklearn.pipeline import Pipeline
import numpy as np

clf1 = LogisticRegression( penalty="l2", C = 0.001, solver="lbfgs", random_state=1)

clf2 = DecisionTreeClassifier(max_depth=1, criterion="entropy", random_state=0)

clf3 = KNeighborsClassifier(n_neighbors=1, p =2, metric="minkowski")

# LogisticRegression and KNN require standardized data
pipe1 = Pipeline([['sc', StandardScaler()], ['clf', clf1]])

pipe3 = Pipeline([['sc', StandardScaler()], ['clf', clf3]])

# Now use majority vote classifier to improve performance
from majority_vote_classifier import MajorityVoteClassifier

mv_clf = MajorityVoteClassifier(classifiers=[pipe1, clf2, pipe3])

# We can see the parameters of each classifier inside mv_clf as follows
#for thing in mv_clf.get_params():
#    print( thing, mv_clf.get_params()[thing])

# As an example, pick one parameter from two of the classifiers and do a cross-validation grid search

from sklearn.model_selection import GridSearchCV

params = {'decisiontreeclassifier__max_depth': [1, 2], 'pipeline-1__clf__C': [0.001, 0.1, 100.]}

# Score models based on "roc_auc", i.e. purity/completeness balance diagnostic
# 10 kfold cross valdiation to estimate the performance
# not sure what iid does
grid = GridSearchCV(estimator=mv_clf, param_grid=params, cv=10, scoring="roc_auc")
grid.fit(X_train, y_train)

for r, _ in enumerate(grid.cv_results_["mean_test_score"]):
    print("%0.3f +/- %0.2f %r" % (grid.cv_results_["mean_test_score"][r],
                                  grid.cv_results_["std_test_score"][r] / 2.0,
                                  grid.cv_results_["params"][r]))

print("Best parameters: %s" % grid.best_params_)

print("Accuracy: %.2f" % grid.best_score_)
