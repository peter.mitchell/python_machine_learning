# Bootstrap aggregation (Bagging)
import pandas as pd

df_wine = pd.read_csv('https://archive.ics.uci.edu/'
                      'ml/machine-learning-databases/'
                      'wine/wine.data',header=None)
df_wine.columns = ['Class label', 'Alchohol', 'Malic acide', 'Ash', 'Alcalinity of ash', 'Magnesium', 'Total phenols',
                   'Flavanoids', 'Nonflavavonid phenols', 'Proanthocyanins', 'Color intensity', 'Hue',
                   'OD280/OD315 of diluted wines', 'Proline']

# drop 1 class
df_wine = df_wine[df_wine["Class label"] != 1]

y = df_wine["Class label"].values
X = df_wine[["Alchohol", "OD280/OD315 of diluted wines"]].values

from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split

le = LabelEncoder()
y = le.fit_transform(y)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1, stratify=y)


## Bootstrapping works best to reduce the variance (overfitting) of a classifier
# But it does not help with improving the bias (underfitting)
# As such, bagging is typically performed with an "unpruned decision tree"
# With no max depth specified (unpruned), a decision tree will perfectly reproduce the training set (so low bias) 
# but will be over-fit (high variance)
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import BaggingClassifier

tree = DecisionTreeClassifier(criterion="entropy", random_state=1, max_depth=None)

# Not sure what all these arguments do
bag = BaggingClassifier(base_estimator=tree, n_estimators=500, max_samples=1.0, max_features=1.0, bootstrap=True, bootstrap_features=False, n_jobs=1, random_state=1)


from sklearn.metrics import accuracy_score

# First check performance on a single unpruned decision tree for reference
tree = tree.fit(X_train, y_train)
y_train_pred = tree.predict(X_train)
y_test_pred = tree.predict(X_test)
tree_train = accuracy_score(y_train, y_train_pred)
tree_test = accuracy_score(y_test, y_test_pred)

print("Decision tree train/test accuracies %0.3f/%0.3f" % (tree_train, tree_test))

# Now do the same for the bagging classifier
bag = bag.fit(X_train, y_train)
y_train_pred = bag.predict(X_train)
y_test_pred = bag.predict(X_test)
bag_train = accuracy_score(y_train, y_train_pred)
bag_test = accuracy_score(y_test, y_test_pred)

print("Bagging train/test accuracies %0.3f/%0.3f" % (bag_train, bag_test))

# I couldn't be bothered plotting the decision regions, look at the book (page 249)
