from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import LabelEncoder

iris = datasets.load_iris()

X,y = iris.data[50:, [1,2]], iris.target[50:]

le = LabelEncoder()
y = le.fit_transform(y)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.5, random_state=1, stratify=y)

from sklearn.model_selection import cross_val_score

from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier

from sklearn.pipeline import Pipeline
import numpy as np

clf1 = LogisticRegression( penalty="l2", C = 0.001, solver="lbfgs", random_state=1)

clf2 = DecisionTreeClassifier(max_depth=1, criterion="entropy", random_state=0)

clf3 = KNeighborsClassifier(n_neighbors=1, p =2, metric="minkowski")

# LogisticRegression and KNN require standardized data
pipe1 = Pipeline([['sc', StandardScaler()], ['clf', clf1]])

pipe3 = Pipeline([['sc', StandardScaler()], ['clf', clf3]])

clf_labels = ["Logistic regression", "Decision Tree", "KNN"]

print('10-fold cross-validation:\n')
for clf, label in zip([pipe1, clf2, pipe3], clf_labels):
    # Scoring is area under receiver operating characteristic curve
    scores = cross_val_score(estimator=clf, X=X_train, y=y_train, cv=10, scoring="roc_auc")
    
    print("ROC AUC: %0.2f (+/- %0.2f) [%s]" % (scores.mean(), scores.std(), label))

# Now use majority vote classifier to improve performance
from majority_vote_classifier import MajorityVoteClassifier

mv_clf = MajorityVoteClassifier(classifiers=[pipe1, clf2, pipe3])

clf_labels += ["Majority voting"]

all_clf = [pipe1, clf2, pipe3, mv_clf]

print('')
print('And now with majority voting added')
print('10-fold cross-validation:\n')
for clf, label in zip(all_clf, clf_labels):
    # Scoring is area under receiver operating characteristic curve                                                                    
    scores = cross_val_score(estimator=clf, X=X_train, y=y_train, cv=10, scoring="roc_auc")

    print("ROC AUC: %0.2f (+/- %0.2f) [%s]" % (scores.mean(), scores.std(), label))


# Visualize the ROC curves
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve
from sklearn.metrics import auc

colors= ["black", "orange", "blue", "green"]
linestyles = [":", "--", "-.", "-"]

for clf, label, clr, ls in zip(all_clf, clf_labels, colors, linestyles):

    # Assumes the label of the "positive" class is 1

    # Note we refit since we are not doing cross-validation, so may as well use all the training data
    y_pred = clf.fit(X_train, y_train).predict_proba(X_test)[:,1]

    # Recall fpr, tpr are f(thresholds), where I'm not sure how the thresholds array is determined..
    fpr, tpr, thresholds = roc_curve(y_true=y_test, y_score=y_pred)

    # Integrate the area under the curve
    roc_auc = auc(x=fpr, y=tpr)

    plt.plot(fpr, tpr, color=clr, linestyle=ls, label="%s (auc = %0.2f)" % (label, roc_auc))

plt.legend(loc="lower right")
plt.plot([0,1],[0,1], linestyle='--', color="gray", linewidth=2)

plt.xlim([-0.1, 1.1])
plt.ylim([-0.1, 1.1])

plt.grid(alpha=0.5)
plt.xlabel("False positive rate (FPR)")
plt.ylabel("True positive rate (TPR)")
plt.show()
