# This figure shows how the weights (and number of features) change as a function of the regularization coefficient C
# Using L1 regularization (which tends to set feature weights to zero as C decreases - (underfitting)).
import numpy as np
import pandas as pd

df_wine = pd.read_csv('https://archive.ics.uci.edu/'
                      'ml/machine-learning-databases/'
                      'wine/wine.data',header=None)
df_wine.columns = ['Class label', 'Alchohol', 'Malic acide', 'Ash', 'Alcalinity of ash', 'Magnesium', 'Total phenols',
                   'Flavanoids', 'Nonflavavonid phenols', 'Proanthocyanins', 'Color intensity', 'Hue',
                   'OD280/OD315 of diluted wines', 'Proline']


# Stratify=y means try to shuffle contents such that there are fractionally equal numbers of y=0,1,2 etc in the test and train sets
from sklearn.model_selection import train_test_split
X, y = df_wine.iloc[:,1:].values, df_wine.iloc[:,0].values
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=0, stratify=y)

# Standardize the features
from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
sc.fit(X_train)
X_train_std = sc.transform(X_train)
X_test_std = sc.transform(X_test)

from sklearn.linear_model import LogisticRegression

weights, params = [], []

for c in np.arange(-4.0, 6.0):

    # OVR I believe stands for One versus Rest
    # penalty="l1" asks for L1 regularization to be used
    # C is inverse of lambda, lambda sets how much we try to minimise the weights via the regularization term
    # If lambda is too large (C is too small), we underfit. Conversely if lambda is too small (C is too large) we overfit
    lr = LogisticRegression(penalty = "l1", solver='liblinear', C=10.**c, multi_class='ovr', random_state=0)
    
    lr.fit(X_train_std, y_train)
    
    weights.append(lr.coef_[1])
    params.append(10**c)


import matplotlib.pyplot as plt

fig = plt.figure()
ax = plt.subplot(111)

colors = ["blue", "green", "red", "cyan", "magenta", "yellow", "black", "pink", "lightgreen", "lightblue",
          "gray", "indigo", "orange"]


weights = np.array(weights)

for column, color in zip(range(weights.shape[1]), colors):
    
    plt.plot( params, weights[:,column], label=df_wine.columns[column+1], color=color)

plt.axhline(0, color="black", linestyle='--', linewidth=3)

plt.xlim([10**(-5), 10**5])

plt.xlabel("C")
plt.ylabel("weight coefficient")
plt.xscale("log")

plt.legend(loc="upper left")
ax.legend(loc="upper center", bbox_to_anchor=(1.38, 1.03), ncol=1, fancybox=True)

plt.show()
