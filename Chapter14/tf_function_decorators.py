import tensorflow as tf

# Can use a function decorator to turn a python function into a static graph (i.e., optimized)
# Recall that for a static graph the inputs need to be pre-specified
# This is done when the decorated function is called I believe

# The trick is that when the function (now a graph) is executed many times with the same input shape/dtype
# TF will keep track of this and use the pre-compiled graph again

@tf.function
def compute_z(a,b,c):
    r1 = tf.subtract(a, b)
    r2 = tf.multiply(2, r1)
    z = tf.add(r2, c)
    return z

tf.print('Scalar inputs:', compute_z(1, 2, 3))
tf.print('Rank 1 inputs:', compute_z([1], [2], [3]))
tf.print('Rank 2 inputs', compute_z([[1]], [[2]], [[3]]))



# It is also possible to limit a function in terms of the inputs (will return an error if wrong input is supplied)
# In this case only rank 1 tensor with type tf.int32 are allowed
@tf.function(input_signature=(tf.TensorSpec(shape=[None], dtype=tf.int32),
                              tf.TensorSpec(shape=[None], dtype=tf.int32),
                              tf.TensorSpec(shape=[None], dtype=tf.int32),))
def compute_z(a, b, c):
    r1 = tf.subtract(a, b)
    r2 = tf.multiply(2, r1)
    z = tf.add(r2, c)
    return z

# Will crash
#tf.print('Scalar inputs:', compute_z(1, 2, 3))

tf.print('Rank 1 inputs:', compute_z([1], [2], [3]))

# Will crash
#tf.print('Rank 2 inputs', compute_z([[1]], [[2]], [[3]]))
