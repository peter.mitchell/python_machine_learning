# Another approach to using keras where you just subclass the main Keras model class
import tensorflow as tf

class MyModel(tf.keras.Model):
    def __init__(self):
        super(MyModel, self).__init__()
        self.hidden_1 = tf.keras.layers.Dense(units=4, activation="relu")
        self.hidden_2 = tf.keras.layers.Dense(units=4, activation="relu")
        self.hidden_3 = tf.keras.layers.Dense(units=4, activation="relu")
        self.output_layer = tf.keras.layers.Dense(units=1, activation="sigmoid")

    def call(self, inputs):
        h = self.hidden_1(inputs)
        h = self.hidden_2(h)
        h = self.hidden_3(h)
        return self.output_layer(h)

tf.random.set_seed(1)

model = MyModel()
model.build(input_shape=(None,2))

print(model.summary())

model.compile(optimizer=tf.keras.optimizers.SGD(),
              loss = tf.keras.losses.BinaryCrossentropy(),
              metrics=[tf.keras.metrics.BinaryAccuracy()])

# See previous script for dataset examples
#hist = model.fit(x_train, y_train, validation_data = (x_valid, y_valid), epochs=200, batch_size=2, verbose=0)
