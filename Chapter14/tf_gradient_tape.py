# Tape is referring to recording things as we do operations, see that gradients can be computed after the fact
import tensorflow as tf

# Tensorflow variables are trainable by default, meaning that gradient-related terms are by default saved by the tape
w = tf.Variable(1.0)
b = tf.Variable(0.5)
print(w.trainable, b.trainable)

x = tf.convert_to_tensor([1.4])
y = tf.convert_to_tensor([2.1])

with tf.GradientTape() as tape:
    z = tf.add(tf.multiply(w, x), b)
    loss = tf.reduce_sum(tf.square(y-z))

# Pretty cool actually!
dloss_dw = tape.gradient(loss, w)

tf.print('dL/dw:', dloss_dw)


# We can check this is correct by doing the alegbra/differntiation ourselves (product rule in this case)
tf.print( 2*x*(w*x +b -y))


#######
# With the way this was setup, x/y were not trainable variables, and so by default gradient tape doesn't store the needed terms
# if you want to compute d Loss / d x (or d y)
# Can get around this (without setting to trainable using .watch method

with tf.GradientTape() as tape:
    tape.wach(x)
    z = tf.add(tf.multiply(w, x), b)
    loss = tf.reduce_sum(tf.square(y -z))

dloss_dx = tape.gradient(loss, x)


#######
# Gradient tape is by default one-use only, it discards the stored data after you request a gradient
# This behaviour can be changed as follows

with tf.GradientTape(persistent=True) as tape:
    z = tf.add(tf.multiply(w, x), b)
    loss = tf.reduce_sum(tf.square(y-z))

dloss_dw = tape.gradient(loss, w)
dloss_db = tape.gradient(loss, b)


### Finally, the optimizers included in Keras have an apply gradients method to update the weights

optimizer = kf.Keras.Optimizer.SGD()
optimizer.apply_gradients(zip([dloss_dw, dloss_db], [w, b]))

