import tensorflow as tf

# "computational graph" refers to the network of lin alg calculations - with the order/dependencies all specified
# This is why the module is called "tensor flow" - the library is optimised to do pre-specified matrix operations
# in a certain order, and to compute the requested gradient-related terms on the fly such that the gradient
# of the final loss function can be computed

# First the old tensorflow 1.0 style static computational graph
g = tf.Graph()

with g.as_default():
    a = tf.constant(1, name='a')
    b = tf.constant(1, name='b')
    c = tf.constant(1, name='c')

    z = 2*(a-b) + c # Can still rely on pythons internal logic to decide the albebraic order of operations

# Then in 1.0 to execute the graph you have to create a "session"
with tf.compat.v1.Session(graph=g) as sess:
    print('Result: z=', sess.run(z))

######################
# Next the up-to-date tensorflow 2.something dynamic computational graph
a = tf.constant(1, name='a')
b = tf.constant(1, name='b')
c = tf.constant(1, name='c')
z = 2*(a-b) + c

tf.print('Result: z=', z)

############################################
# Feeding data into a model
# In this scenario we want to write down the model/graph before specifying the parameters/inputs

# v1.0 style
g = tf.Graph()
with g.as_default():
    # Have to define placeholder variables with pre-specific dtype (and shape?, not sure about that)
    a = tf.compat.v1.placeholder(shape=None, dtype=tf.int32, name='tf_a')
    b = tf.compat.v1.placeholder(shape=None, dtype=tf.int32, name='tf_b')
    c = tf.compat.v1.placeholder(shape=None, dtype=tf.int32, name='tf_c')

    z = 2*(a-b)+c

with tf.compat.v1.Session(graph=g) as sess:
    feed_dict = {a:1, b:2, c:3}
    print('Result: z=', sess.run(z, feed_dict=feed_dict))

# v2.0 style
# dtype does not have to be pre-specified
def compute_z(a, b, c):
    r1 = tf.subtract(a, b) # Use tf math operations so that the function can handle arbitrary inputs (including tf tensor objects)
    r2 = tf.multiply(2, r1)
    z = tf.add(r2, c)
    return z

# Can handle multiple input types
tf.print('Scalar inputs:', compute_z(1, 2, 3))
tf.print('Rank 1 inputs:', compute_z([1], [2], [3]))
tf.print('Rank 2 inputs', compute_z([[1]], [[2]], [[3]]))
