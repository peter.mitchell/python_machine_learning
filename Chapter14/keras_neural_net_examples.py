# Building on Chapter 13, look more closely at how NNets can be constructed in tf/keras

import tensorflow as tf

########### Example 1 ########################

# Sequential means simple neural net structure where one layer feeds sequentially into the next
model = tf.keras.Sequential()

# Dense means all inputs are connnected to all outputs
# Units is number of nodes, this also sets the number of outputs (per example) from the layer

# 'relu' stands for Rectified linear unit activation (pg 468) - it is max(0, z)
# This is actually still technically a non-linear function, with behaviour that gradient is always 0, 1
# Meaning it solves problems with sigmoid like activations with gradient terms dropping to almost zero 
# (which then leads to very slow training convergence)

model.add(tf.keras.layers.Dense(units=16, activation='relu'))
model.add(tf.keras.layers.Dense(units=32, activation='relu'))

# None is number of training examples (dummy value)
# 4 is number of input features (so first weight matrix here would be 4x16 elements)
model.build(input_shape=(None, 4))

'''print(model.summary())

# Note in this case we didn't hand-set the name of the layers, so they get auto-assigned instead
for v in model.variables:
    print('{:20s}'.format(v.name), v.trainable, v.shape)'''


########### Example 2 #########################

model = tf.keras.Sequential()

model.add(tf.keras.layers.Dense(
        units = 16,
        activation = tf.keras.activations.relu,
        kernel_initializer = tf.keras.initializers.glorot_uniform(), # 'kernel' is weight matrix - choose how we init weights
        bias_initializer = tf.keras.initializers.Constant(2.0)
        ))

model.add(tf.keras.layers.Dense(
        units = 32,
        activation = tf.keras.activations.sigmoid,
        kernel_regularizer=tf.keras.regularizers.l1 # Use L1 regularization - which punishes non-zero weights (not sure where coeff is set though!)
        ))

# Note I'm not very clear on the difference between build and compile from the two examples in this script!

model.compile(optimizer=tf.keras.optimizers.SGD(learning_rate=0.001), # Stochastic Gradient Descent
              loss = tf.keras.losses.BinaryCrossentropy(), # Some loss function - maybe it was defined earlier in the book?
              metrics=[tf.keras.metrics.Accuracy(), # Not used in training, but so we can evaluate training afterwards
                       tf.keras.metrics.Precision(),
                       tf.keras.metrics.Recall(),])

########## Example 3 #####################################

# Now let's try to solver the XOR problem (dataset where there are two classes that live in opposite quadrants)
# i.e. classic test of non-linear classifiers

import numpy as np

tf.random.set_seed(1)
np.random.seed(1)

x = np.random.uniform(low=-1, high=1, size=(200,2))
y = np.ones(len(x))
y[x[:,0] * x[:,1] < 0] = 0

x_train = x[:100, :]
y_train = y[:100]

x_valid = x[100:, :]
y_valid = y[100:]

# First verify that a linear classifier (logistic regression) fails
model = tf.keras.Sequential()

# I.e. fit points with y = w1*x_0 + w2*x1 + b # where _0 and _1 are the two dimensions (features) of the input data
model.add(tf.keras.layers.Dense(units=1, input_shape=(2,), activation = 'sigmoid'))

model.compile(optimizer=tf.keras.optimizers.SGD(),
              loss = tf.keras.losses.BinaryCrossentropy(),
              metrics = [tf.keras.metrics.BinaryAccuracy()])

print( "Training simple logistic regression model to XOR")
hist = model.fit(x_train, y_train, validation_data = (x_valid, y_valid), epochs=200, batch_size=2, verbose=0)
print( "Done, plotting..")

# Visualize the results
from mlxtend.plotting import plot_decision_regions
import matplotlib.pyplot as plt

history = hist.history

fig = plt.figure(figsize=(16,4))

ax = fig.add_subplot(1, 3, 1)
plt.plot(history['loss'], lw=4)
plt.plot(history['val_loss'], lw=4)
plt.legend(['Train loss', 'Validation loss'], fontsize=15)
ax.set_xlabel('Epochs', size=15)

ax = fig.add_subplot(1, 3, 2)
ax.plot(history['binary_accuracy'], lw=4)
ax.plot(history['val_binary_accuracy'], lw=4)
plt.legend(['Train Acc.', 'Validation Acc.'], fontsize=15)
ax.set_xlabel('Epochs', size=15)

ax = fig.add_subplot(1, 3, 3)
plot_decision_regions(X=x_valid, y=y_valid.astype(np.integer),clf=model)

ax.set_xlabel(r'$x_1$', size=15)
ax.xaxis.set_label_coords(1, -0.025)

ax.set_ylabel(r'$x_2$', size=15)
ax.yaxis.set_label_coords(-0.025, 1)

plt.show()

#### Next train a complex neural net - which will create a non-linear decision boundary
tf.random.set_seed(1)

model = tf.keras.Sequential()
model.add(tf.keras.layers.Dense(units=4, input_shape=(2,), activation='relu'))
model.add(tf.keras.layers.Dense(units=4, activation='relu'))
model.add(tf.keras.layers.Dense(units=4, activation='relu'))
model.add(tf.keras.layers.Dense(units=1, activation='sigmoid'))

#print(model.summary())

model.compile(optimizer=tf.keras.optimizers.SGD(),
              loss = tf.keras.losses.BinaryCrossentropy(),
              metrics=[tf.keras.metrics.BinaryAccuracy()])

print( "Training the multi-layer neural net")
hist = model.fit(x_train, y_train, validation_data=(x_valid, y_valid), epochs=200, batch_size=2, verbose=0)

history = hist.history

fig = plt.figure(figsize=(16,4))

ax = fig.add_subplot(1, 3, 1)
plt.plot(history['loss'], lw=4)
plt.plot(history['val_loss'], lw=4)
plt.legend(['Train loss', 'Validation loss'], fontsize=15)
ax.set_xlabel('Epochs', size=15)

ax = fig.add_subplot(1, 3, 2)
ax.plot(history['binary_accuracy'], lw=4)
ax.plot(history['val_binary_accuracy'], lw=4)
plt.legend(['Train Acc.', 'Validation Acc.'], fontsize=15)
ax.set_xlabel('Epochs', size=15)

ax = fig.add_subplot(1, 3, 3)
plot_decision_regions(X=x_valid, y=y_valid.astype(np.integer),clf=model)

ax.set_xlabel(r'$x_1$', size=15)
ax.xaxis.set_label_coords(1, -0.025)

ax.set_ylabel(r'$x_2$', size=15)
ax.yaxis.set_label_coords(-0.025, 1)

plt.show()
