# Introduce proper dataset preparation going into a tensorflow "estimator"
# Example uses the MPG (miles per gallon) dataset - car quality
# An "estimator" is a high-level wrapper around all of the NeuralNet classifier stuff (including keras)

import pandas as pd
import tensorflow as tf
import numpy as np

# Downloads and stores
dataset_path = tf.keras.utils.get_file("auto-mpg.data",
                                       ("http://archive.ics.uci.edu/ml/machine-learning"
                                        "-databases/auto-mpg/auto-mpg.data"))

column_names = ['MPG', 'Cylinders', 'Displacement', 'Horsepower', 'Weight', 'Acceleration', 'ModelYear', 'Origin']

df = pd.read_csv(dataset_path, names=column_names, na_values='?', comment='\t', sep=' ', skipinitialspace=True)

## drop NaN rows
df = df.dropna()
df = df.reset_index(drop=True)

# Train/test split
import sklearn
import sklearn.model_selection

df_train, df_test = sklearn.model_selection.train_test_split(df, train_size=0.8)
train_stats = df_train.describe().transpose() # Some pandas thing?

# Some features are continuous floats, some are not. Here are the former
numeric_column_names = ['Cylinders', 'Displacement', 'Horsepower', 'Weight', 'Acceleration']

df_train_norm, df_test_norm = df_train.copy(), df_test.copy()

# Normalise the continuous features
for col_name in numeric_column_names:
    mean = train_stats.loc[col_name, 'mean']
    std = train_stats.loc[col_name, 'std']
    df_train_norm.loc[:, col_name] = (df_train_norm.loc[:, col_name] - mean)/std
    df_test_norm.loc[:, col_name] = (df_test_norm.loc[:, col_name] - mean)/std

##### Now we want to change the dataset into the required format for tensorflow estimators #####
# The step is to define the set of features that we are going to use

# This is a list of tensorflow "feature_column" instances
# There are several types of these, first is the numeric_column (i.e. floating/continuous values)
numeric_features = []
for col_name in numeric_column_names:
    numeric_features.append( tf.feature_column.numeric_column(key=col_name))
                              
# For dates, we bin them into a few bins (bucketing) to compress the data
feature_year = tf.feature_column.numeric_column(key='ModelYear')
bucketized_features = []
bucketized_features.append(tf.feature_column.bucketized_column(
        source_column = feature_year,
        boundaries=[73, 76, 79]))

# For country of origin (string), we use a vocabulary list
# Note there are a few ways to do this, see page 505
feature_origin = tf.feature_column.categorical_column_with_vocabulary_list(
    key='Origin',
    vocabulary_list=[1,2,3])

# Some estimators (see pg 505) require categorical features to converted into a "Dense" column
# A few ways again to do this
# Here we convert into an "indicator column", which is like one-hot encoding (class one is (1, 0, 0), class two is (0, 1, 0) etc)
categorical_indicator_features = []
categorical_indicator_features.append(tf.feature_column.indicator_column(feature_origin))

all_feature_columns = (numeric_features + bucketized_features + categorical_indicator_features)

# Now we create a function to convert the pandas dataset into the needed input format (plus shuffle/batch/repeat operations)

# Note "train" here means we feed in the training set. Function doesn't do the training itself
def train_input_fn(df_train, batch_size=8):
    df = df_train.copy()
    train_x, train_y = df, df.pop('MPG')
    dataset = tf.data.Dataset.from_tensor_slices( (dict(train_x), train_y))

    return dataset.shuffle(1000).repeat().batch(batch_size)

'''ds = train_input_fn(df_train_norm)
batch = next(iter(ds))
#print('Keys:', batch[0].keys())
#print('Batch Model Years:', batch[0]['ModelYear'])'''

# Same but for test dset
def eval_input_fn(df_test, batch_size=8):
    
    df = df_test.copy()
    test_x, test_y = df, df.pop('MPG')
    dataset = tf.data.Dataset.from_tensor_slices( (dict(test_x), test_y))

    return dataset.batch(batch_size)


# And finally, we introduce a tf estimator

regressor = tf.estimator.DNNRegressor(
    feature_columns = all_feature_columns,
    hidden_units=[32, 10], # This implicitly specifies no. of hidden layers, and explicitly the number of units in each
    model_dir = 'models/autompg-dnnregressor/') # Controls where outputs (including checkpoints) are written

# ALL CAPS WAARGGHH (??, think its to separate arguments, from arg names below)
EPOCHS = 1000
BATCH_SIZE = 8
total_steps = EPOCHS * int(np.ceil(len(df_train) / BATCH_SIZE))

regressor.train(input_fn=lambda:train_input_fn( df_train_norm, batch_size=BATCH_SIZE), steps=total_steps)

# Note we could then reload the checkpointed (or final) model as follows
reloaded_regressor = tf.estimator.DNNRegressor(
    feature_columns=all_feature_columns,
    hidden_units=[32, 10],
    warm_start_from = 'models/autompg-dnnregressor/',
    model_dir = 'models/autompg-dnnregressor/')

eval_results = reloaded_regressor.evaluate(
    input_fn = lambda: eval_input_fn(df_test_norm, batch_size=8))

print('Average Loss {:.4f}'.format(eval_results['average_loss']))

pred_res = regressor.predict(
    input_fn = lambda: eval_input_fn( df_test_norm, batch_size=8))

print(next(iter(pred_res)))
