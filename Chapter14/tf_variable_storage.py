import tensorflow as tf

# Introduction to tensorflows "variable" class, and some of its features
a = tf.Variable(initial_value=3.14, name='var_a')
#print(a)

b = tf.Variable(initial_value=[1,2,3], name='var_b')
#print(b)

c = tf.Variable(initial_value=[True, False], dtype=tf.bool)
d = tf.Variable(initial_value=['abc'], dtype=tf.string)

# By default, tf variables have an attribute "trainable" set to true
# This means the gradient of the final loss function w.r.t this variable is automatically computed
# It also informs keras module about which are the trainable parameters obviously 
w = tf.Variable([1, 2, 3], trainable=False)
#print(w.trainable)


# Variable values can be re-assigned (provided shape/dtype retained)
#print( w.assign([3,1,4], read_value=True) ) # Read value determines if the call returns the values

# += version
w.assign_add([2, -1, 2], read_value=False) # False means in place


# Random variable creation
# For weights the commonly used sampling scheme is Glorot Normal distribution
# This (I believe) is a normal distribution but normalised for 2d weight matrices such that different layers
# are weighted appropriately given the number of inputs/outputs

tf.random.set_seed(1)

init = tf.keras.initializers.GlorotNormal()

tf.print(init(shape=(3,)))

v = tf.Variable(init(shape=(2,3,)))
tf.print(v)



# Defining variables inside a module

class MyModule(tf.Module):
    def __init__(self):
        init = tf.keras.initializers.GlorotNormal()
        self.w1 = tf.Variable(init(shape=(2,3)), trainable=True)
        self.w2 = tf.Variable(init(shape=(1,2)), trainable=False)

m = MyModule()
print("All module variables:", [v.shape for v in m.variables])
print("Trainable variable:", [v.shape for v in m.trainable_variables])
